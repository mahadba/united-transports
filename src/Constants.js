
// Dev URL 

/*

export const LOGIN_URL =  'http://localhost:8080/ut/validateLoginDetails';
export const GET_CUSTOMER_DETAILS =  'http://localhost:8080/ut/getCustomerDetails';
export const POST_CUSTOMER_DETAILS  = 'http://localhost:8080/ut/addOrsaveCustomerDetails';
export const GET_LOCATION_DETAILS =  'http://localhost:8080/ut/getLocationDetails';
export const GET_LOCATION_DETAILS_BOOKING =  'http://localhost:8080/ut/getLocationDetailsForBooking';
export const GET_LOC_DET_BY_CUST_ID  = 'http://localhost:8080/ut/getLocationDetailsByCustId/';
export const GET_SAL_DET_BY_CUST_ID  = 'http://localhost:8080/ut/getSalaryDetByLocId';
export const SAVE_lOC_DETAILS  = 'http://localhost:8080/ut/saveCustomerLocationDetails';
export const SAVE_CONTACT_DETAILS  = 'http://localhost:8080/ut/saveContactDetails';
export const GET_CONTACT_DETAILS  = 'http://localhost:8080/ut/getContactDetails';
export const DELETE_CONTACT_DETAILS  = 'http://localhost:8080/ut/deleteContactDetails';

export const GET_ACCESS_DETAILS  = 'http://localhost:8080/ut/getAccessDetails';
export const SAVE_ACCESS_DETAILS  = 'http://localhost:8080/ut/saveAccessDetails';

export const UPDATE_ACCESS_DETAILS  = 'http://localhost:8080/ut/updateAccessDetails';
export const DELETE_ACCESS_DETAILS  = 'http://localhost:8080/ut/deleteAccessDetails';
export const OPEN_NEW_BOOKING  = 'http://localhost:8080/ut/openNewBooking';

export const EXPIRED_VEHICLE_DETAILS  = 'http://localhost:8080/ut/expiredVehicleDetails';
export const ABOUT_TO_EXPIRE_DETAILS  = 'http://localhost:8080/ut/aboutToExpireDetails';
export const GET_VEHICLES_DETAILS = 'http://localhost:8080/ut/getAllVehicleDetails';
export const EXPIRED_VEHICLE_DETAILS  = 'http://localhost:8080/ut/expiredVehicleDetails';
export const ABOUT_TO_EXPIRE_DETAILS  = 'http://localhost:8080/ut/aboutToExpireDetails';
export const GET_VEHICLES_DETAILS = 'http://localhost:8080/ut/getAllVehicleDetails';

export const OPEN_NEW_BOOKING  = 'http://localhost:8080/ut/openNewBooking';
export const GET_BOOKING_DETAILS = 'http://localhost:8080/ut/getBookingDetails';
export const GET_BOOKING_DETAILS_BY_ID = 'http://localhost:8080/ut/getBookingDetailsById/';
export const UPDATE_BOOKING_DETAILS = 'http://localhost:8080/ut/updateBookingDetails';
export const APPROVE_OPEN_BOOKING = 'http://localhost:8080/ut/approveOpenBooking';
export const CLOSE_OPEN_AP_BOOKING = 'http://localhost:8080/ut/closeApprovedBooking';
export const APPROVE_CLOSE_BOOKING = 'http://localhost:8080/ut/closeBooking';
export const DELETE_BOOKING_DETAILS = 'http://localhost:8080/ut/deleteBooking';

export const GET_DRIVER_DETAILS = 'http://localhost:8080/ut/getAllDriverDetails';
export const POST_DRIVER_DETAILS = 'http://localhost:8080/ut/saveOrUpdateDriverDetails';
export const DRIVER_SALARY_DETAILS = 'http://localhost:8080/ut/getDriverSalaryDetails/';

export const GET_HALTING_DETAILS = 'http://localhost:8080/ut/getHaltingDetails';
export const GET_DRIVER_HALTING_DETAILS = 'http://localhost:8080/ut/getDriverHaltingDetails/';
export const GET_HALTING_DETAILS_ID = 'http://localhost:8080/ut/getHaltingDetailsById/';
export const ADD_HALTING_DETAILS = 'http://localhost:8080/ut/addDriverHaltingCharges';
export const UPDATE_HALTING_DETAILS = 'http://localhost:8080/ut/updateDriverHaltingCharges';
export const DELETE_HALTING_DETAILS = 'http://localhost:8080/ut/deleteDriverHaltingCharges';
export const PAY_SALARY_DETAILS = 'http://localhost:8080/ut/paySalaryDetails';

export const ADD_VEHICLE_DETAILS = 'http://localhost:8080/ut/addOrsaveVehicleDetails';


*/


// Prod URL

export const LOGIN_URL =  'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/validateLoginDetails';

export const GET_CUSTOMER_DETAILS =  'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getCustomerDetails';
export const GET_ACTIVE_CUSTOMER_DETAILS =  'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getActiveCustomerDetails';
export const POST_CUSTOMER_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/addOrsaveCustomerDetails';


export const GET_LOCATION_DETAILS =  'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getLocationDetails';
export const GET_LOC_DET_BY_CUST_ID  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getLocationDetailsByCustId/';
export const GET_SAL_DET_BY_CUST_ID  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getSalaryDetByLocId';
export const SAVE_lOC_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/saveCustomerLocationDetails';
export const SAVE_CONTACT_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/saveContactDetails';
export const GET_CONTACT_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getContactDetails';
export const DELETE_CONTACT_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/deleteContactDetails';

export const GET_ACCESS_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getAccessDetails';
export const SAVE_ACCESS_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/saveAccessDetails';
export const UPDATE_ACCESS_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/updateAccessDetails';
export const DELETE_ACCESS_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/deleteAccessDetails';


export const GET_DRIVER_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getAllDriverDetails';
export const POST_DRIVER_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/saveOrUpdateDriverDetails'; 
export const DRIVER_SALARY_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getDriverSalaryDetails/';

export const GET_OTHEREXPENSE_DETAILS = "http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getAllVehicleExpense"
export const SAVE_OTHEREXPENSE_DETAILS = "http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/saveVehicleExpense"
export const UPDATE_OTHEREXPENSE_DETAILS = "http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/updateVehicleExpense"
export const DELETE_OTHEREXPENSE_DETAILS = "http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/deleteVehicleExpense";



export const OPEN_NEW_BOOKING  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/openNewBooking';
export const GET_BOOKING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getBookingDetails';
export const GET_BOOKING_DETAILS_BY_ID = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getBookingDetailsById/';
export const THIRD_PARTY_BOOKED_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getThirdPartyBookedDetails';

export const UPDATE_BOOKING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/updateBookingDetails';
export const APPROVE_OPEN_BOOKING = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/approveOpenBooking';
export const CLOSE_OPEN_AP_BOOKING = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/closeApprovedBooking';
export const APPROVE_CLOSE_BOOKING = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/closeBooking';
export const DELETE_BOOKING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/deleteBooking';

export const EXPIRED_VEHICLE_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/expiredVehicleDetails';
export const ABOUT_TO_EXPIRE_DETAILS  = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/aboutToExpireDetails';

export const GET_TRIP_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getTripDetail';


export const ADD_VEHICLE_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/addOrsaveVehicleDetails';
export const GET_VEHICLES_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getAllVehicleDetails';

export const GET_HALTING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getHaltingDetails';
export const GET_DRIVER_HALTING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getDriverHaltingDetails/';
export const GET_HALTING_DETAILS_ID = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getHaltingDetailsById/';
export const ADD_HALTING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/addDriverHaltingCharges';
export const UPDATE_HALTING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/updateDriverHaltingCharges';
export const DELETE_HALTING_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/deleteDriverHaltingCharges';
export const PAY_SALARY_DETAILS = 'http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/paySalaryDetails';

 export const vehicleInsurance = [
            {
                Header: "Vehicle No",
                accessor: "vehicleNumber"
            },
            {
                Header: "Vehicle Model",
                accessor: "vehicleModel"
            },
            {
                Header: "Vehicle Type",
                accessor: "vehicleType"
            }
			,
            {
                Header: "Insurance Validity",
                accessor: "insuranceValiditiy"
            }
            ,
			 {
                Header: "Insurance Name",
                accessor: "insuranceName"
            },
            {
                Header: "Company",
                accessor: "vehicleCompany"
            },
            {
                Header: "Status",
                accessor: "insuranceStatus"
            }
        ];
		
export const vehicleRoadTax = [
            {
                Header: "Vehicle No",
                accessor: "vehicleNumber"
            },
            {
                Header: "Vehicle Model",
                accessor: "vehicleModel"
            },
            {
                Header: "Vehicle Type",
                accessor: "vehicleType"
            }
            ,
			 {
                Header: "Road Tax Due Date",
                accessor: "roadTaxValiditiy"
            },
            {
                Header: "Company",
                accessor: "vehicleCompany"
            },
            {
                Header: "Amount",
                accessor: "roadTaxAmount"
            },
            {
                Header: "Road Tax Status",
                accessor: "roadTaxStatus"
            }
			
			
        ];
		
export const vehicleTNPermitValidity = [
            {
                Header: "Vehicle No",
                accessor: "vehicleNumber"
            },
            {
                Header: "Vehicle Model",
                accessor: "vehicleModel"
            },
            {
                Header: "Vehicle Type",
                accessor: "vehicleType"
            }
            ,
			 {
                Header: "TN Permit Validity",
                accessor: "tnPermitValiditiy"
            },
            {
                Header: "Company",
                accessor: "vehicleCompany"
            },
            {
                Header: "Status",
                accessor: "tnPermitStatus"
            }
			
        ];
		
export const vehicleNationalPermitValidity = [
            {
                Header: "Vehicle No",
                accessor: "vehicleNumber"
            },
            {
                Header: "Vehicle Model",
                accessor: "vehicleModel"
            },
            {
                Header: "Vehicle Type",
                accessor: "vehicleType"
            }
            ,
			 {
                Header: "NP Validity",
                accessor: "npValiditiy"
            },
            {
                Header: "Company",
                accessor: "vehicleCompany"
            },
            {
                Header: "Status",
                accessor: "npStatus"
            }
			
        ];
		
export const vehicleFCValidity = [
            {
                Header: "Vehicle No",
                accessor: "vehicleNumber"
            },
            {
                Header: "Vehicle Model",
                accessor: "vehicleModel"
            },
            {
                Header: "Vehicle Type",
                accessor: "vehicleType"
            }
            ,
			 {
                Header: "FC Validity",
                accessor: "fcValidity"
            },
            {
                Header: "Company",
                accessor: "vehicleCompany"
            },
            {
                Header: "Status",
                accessor: "fcStatus"
            }
			
        ];
	
	
	
		
export const allDetails = [
            {
                Header: "Vehicle No",
                accessor: "vehicleNumber"
            },
            {
                Header: "Vehicle Model",
                accessor: "vehicleModel"
            },
            {
                Header: "Vehicle Type",
                accessor: "vehicleType"
            }
            ,
			 {
                Header: "NP Validity",
                accessor: "npValiditiy"
            },
            {
                Header: "Company",
                accessor: "vehicleCompany"
            },
            {
                Header: "NP Permit Status",
                accessor: "npStatus"
            },
			 {
                Header: "TN Permit Validity",
                accessor: "tnPermitValiditiy"
            },
            {
                Header: "TN Permit Status",
                accessor: "tnPermitStatus"
            } ,
			 {
                Header: "Road Tax Due Date",
                accessor: "roadTaxValiditiy"
            },
            
            {
                Header: "Road Tax Amount",
                accessor: "roadTaxAmount"
            }, {
                Header: "Insurance Validity",
                accessor: "insuranceValiditiy"
            },
            
            {
                Header: "Insurance Status",
                accessor: "insuranceStatus"
            }
			,
            
            {
                Header: "Insurance Name",
                accessor: "insuranceName"
            }
			
        ];
		
export const vehicleDetails =[
{"vehicleNumber":"TN 04 AJ 1071","vehicleModel":"13-04-16","vehicleType":"20'","insuranceValidity":"21-10-2020","roadTaxDueDate":"30-09-2020","company":"TIMESCAN LOGISTICS","status":"","roadTaxAmount":"5","tnPermitValidity":"08-03-2021","npPermitValidity":"18-08-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 18 A 7677","vehicleModel":"29-11-11","vehicleType":"40'","insuranceValidity":"22-10-2020","roadTaxDueDate":"30-09-2020","company":"UNITED TRANSPORTS","status":"","roadTaxAmount":"6","tnPermitValidity":"18-05-2021","npPermitValidity":"06-09-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 05 AY 9111","vehicleModel":"29-1-11","vehicleType":"20'","insuranceValidity":"12-11-2020","roadTaxDueDate":"30-09-2020","company":"UNITED TRANSPORTS","status":"","roadTaxAmount":"7","tnPermitValidity":"02-08-2021","npPermitValidity":"15-11-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 04 AJ 1071","vehicleModel":"13-04-16","vehicleType":"20'","insuranceValidity":"21-10-2020","roadTaxDueDate":"30-09-2020","company":"TIMESCAN LOGISTICS","status":"","roadTaxAmount":"8","tnPermitValidity":"08-03-2021","npPermitValidity":"18-08-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 18 A 7677","vehicleModel":"29-11-11","vehicleType":"20'","insuranceValidity":"22-10-2020","roadTaxDueDate":"30-09-2020","company":"UNITED TRANSPORTS","status":"","roadTaxAmount":"9","tnPermitValidity":"18-05-2021","npPermitValidity":"06-09-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 05 AY 9111","vehicleModel":"29-1-11","vehicleType":"20'","insuranceValidity":"12-11-2020","roadTaxDueDate":"30-09-2020","company":"UNITED TRANSPORTS","status":"","roadTaxAmount":"10","tnPermitValidity":"02-08-2021","npPermitValidity":"15-11-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 04 AJ 1071","vehicleModel":"13-04-16","vehicleType":"20'","insuranceValidity":"21-10-2020","roadTaxDueDate":"30-09-2020","company":"TIMESCAN LOGISTICS","status":"","roadTaxAmount":"11","tnPermitValidity":"08-03-2021","npPermitValidity":"18-08-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 18 A 7677","vehicleModel":"29-11-11","vehicleType":"20'","insuranceValidity":"22-10-2020","roadTaxDueDate":"30-09-2020","company":"UNITED TRANSPORTS","status":"","roadTaxAmount":"12","tnPermitValidity":"18-05-2021","npPermitValidity":"06-09-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""},
{"vehicleNumber":"TN 05 AY 9111","vehicleModel":"29-1-11","vehicleType":"20'","insuranceValidity":"12-11-2020","roadTaxDueDate":"30-09-2020","company":"UNITED TRANSPORTS","status":"","roadTaxAmount":"13","tnPermitValidity":"02-08-2021","npPermitValidity":"15-11-2020","tamilNaduPermitStatus":"","nationalPermitStatus":""}];

		
export const driverDetails =[
    {"driver":"John","phone":"9876543210","alternatephone":"8795462130","addressLine1":"1, Adam Street, Bob Lan","addressLine2":"Anna Nagar, Chennai - 600009","licenseNumber":"25TN0215479962", "expiry":"14-12-2025","bank":"ABC Bank","ifscCode":"abc0001234","accNumber":"012301468897","status":"Active", "credit":"1000"},
    {"driver":"Adam","phone":"9876543211","alternatephone":"8795462130","addressLine1":"1, Adam Street, Bob Lan","addressLine2":"Anna Nagar, Chennai - 600009","licenseNumber":"25TN0215479962","expiry":"14-12-2025","bank":"ABC Bank","ifscCode":"abc0001234","accNumber":"012301468897","status":"Active","credit":"1000"},
    {"driver":"Bob","phone":"9876543212","alternatephone":"8795462130","addressLine1":"1, Adam Street, Bob Lan","addressLine2":"Anna Nagar, Chennai - 600009","licenseNumber":"25TN0215479962","expiry":"15-12-2025","bank":"ABC Bank","ifscCode":"abc0001234","accNumber":"012301468897","status":"Active","credit":"1000"},
    {"driver":"Newton","phone":"9876543213","alternatephone":"8795462130","addressLine1":"1, Adam Street, Bob Lan","addressLine2":"Anna Nagar, Chennai - 600009","licenseNumber":"25TN0215479962","expiry":"12-12-2025","bank":"ABC Bank","ifscCode":"abc0001234","accNumber":"012301468897","status":"Active","credit":"0"},
    {"driver":"Jack","phone":"9876543214","alternatephone":"8795462130","addressLine1":"1, Adam Street, Bob Lan","addressLine2":"Anna Nagar, Chennai - 600009","licenseNumber":"25TN0215479962","expiry":"18-10-2025","bank":"ABC Bank","ifscCode":"abc0001234","accNumber":"012301468897","status":"Active","credit":"1000"},
    {"driver":"Jim","phone":"9876543215","alternatephone":"8795462130","addressLine1":"1, Adam Street, Bob Lan","addressLine2":"Anna Nagar, Chennai - 600009","licenseNumber":"25TN0215479962","expiry":"12-12-2025","bank":"ABC Bank","ifscCode":"abc0001234","accNumber":"012301468897","status":"Active","credit":"1000"},
    {"driver":"Steve","phone":"9876543216","alternatephone":"8795462130","addressLine1":"1, Adam Street, Bob Lan","addressLine2":"Anna Nagar, Chennai - 600009","licenseNumber":"25TN0215479962","expiry":"12-12-2025","bank":"ABC Bank","ifscCode":"abc0001234","accNumber":"012301468897","status":"Active","credit":"0"},
    ];

export const bookingDetails =[
    {"bookingID":"BK0001","vehicleNumber":"TN 05 AY 9111","vehicleType":"20'","company":"UNITED TRANSPORTS","deliveryType":"IMPORT","fromLocation":"Cochin","toLocation":"Mumbai","driver":"John","phone":"9876543210","salary":"5000.00","advance":"2000.00","RTO":"0","pickupDate":"02-11-2020","dropDate":"","containerID":"","customer":"Bajaj","amount":"","toll":"0","status":"Open"},
    {"bookingID":"BK0003","vehicleNumber":"TN 05 AY 9111","vehicleType":"20'","company":"UNITED TRANSPORTS","deliveryType":"IMPORT","fromLocation":"Cochin","toLocation":"Mumbai","driver":"John","phone":"9876543210","salary":"5000.00","advance":"2000.00","RTO":"0","pickupDate":"03-11-2020","dropDate":"","containerID":"","customer":"Bajaj","amount":"","toll":"0","status":"Open"},
    {"bookingID":"BK0004","vehicleNumber":"TN 05 AY 9111","vehicleType":"20'","company":"UNITED TRANSPORTS","deliveryType":"IMPORT","fromLocation":"Cochin","toLocation":"Mumbai","driver":"John","phone":"9876543210","salary":"5000.00","advance":"2000.00","RTO":"800","pickupDate":"03-11-2020","dropDate":"05-11-2020","containerID":"YIDLE","customer":"Bajaj","amount":"18000","toll":"100","status":"Closed"},
    {"bookingID":"BK0002","vehicleNumber":"TN 04 AJ 1071","vehicleType":"40'","company":"GARUDA TRAVELS","deliveryType":"IMPORT","fromLocation":"Chennai","toLocation":"Hyderabad","driver":"-","phone":"-","salary":"-","advance":"-","RTO":"0","pickupDate":"04-11-2020","dropDate":"","containerID":"","customer":"Bajaj","amount":"","toll":"0","status":"Open"}
];

export const locationDetails=[
    {"fromLocation":"Chennai","toLocation":"Hyderabad","vehicleType":"20'","salary":"5000.00"},
    {"fromLocation":"Cochin","toLocation":"Mumbai","vehicleType":"40'","salary":"9000.00"},
    {"fromLocation":"Mumbai","toLocation":"Chennai","vehicleType":"20'","salary":"5000.00"},
    {"fromLocation":"Hyderabad","toLocation":"Cochin","vehicleType":"40'","salary":"9000.00"},
    {"fromLocation":"Bangalore","toLocation":"Mangalore","vehicleType":"20'","salary":"5000.00"},
    {"fromLocation":"Mangalore","toLocation":"Madurai","vehicleType":"40'","salary":"9000.00"},
    {"fromLocation":"Trichy","toLocation":"Hyderabad","vehicleType":"20'","salary":"5000.00"},
    {"fromLocation":"Madurai","toLocation":"Mumbai","vehicleType":"40'","salary":"9000.00"}
];

export const customerDetail=[
{"customer":"ABC","contactName":"Ganesh","contactPhone":"9513574860","amount":"10000","email":"abc@gmail.com","address1":"1, new street","address2":"yes nagar, chennai-001011","GST":"AY8487412B5547","status":"Active"},
{"customer":"Bajaj","contactName":"James","contactPhone":"8799514860","amount":"12000","email":"abc@gmail.com","address1":"1, new street","address2":"yes nagar, chennai-001011","GST":"AY8487412B5547","status":"Inactive"}
];

export const thridPartyVehicleDetails=[
    {"vehicleNumber":"TN 04 AJ 1071","vehicleType":"20'","company":"GARUDA TRAVELS","amount":"15000","paid":"10000","balance":"5000"},
    {"vehicleNumber":"TN 18 A 7677","vehicleType":"20'","company":"AWS TRAVELS","amount":"15000","paid":"10000","balance":"5000"}
];

export const haltingChargeDetails=[
    {"id":"HLT001","driver":"John","phone":"9876543210","type":"MT","fromDate":"20-10-2020","toDate":"22-10-2020","amount":"15000","status":"unpaid"},
    {"id":"HLT002","driver":"John","phone":"9876543210","type":"MT","fromDate":"20-10-2020","toDate":"22-10-2020","amount":"15000","status":"unpaid"},
    {"id":"HLT003","driver":"Adam","phone":"9876543210","type":"LOAD","fromDate":"23-10-2020","toDate":"23-10-2020","amount":"15000","status":"unpaid"}
];

export const otherExpenses=[
    {"id":"EXP0001","vehicleNumber":"TN 04 AJ 1071","vehicleType":"20'","expenseDate":"28-10-2020","expenseType":"Engine boring","amount":"15000","comments":"need service in 6 months"},
    {"id":"EXP0002","vehicleNumber":"TN 18 A 7677","vehicleType":"40'","expenseDate":"15-11-2020","expenseType":"Engine boring","amount":"18000","comments":""}
];