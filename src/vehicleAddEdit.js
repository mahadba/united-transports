import React, { useEffect } from 'react';
import "react-table-6/react-table.css";
import "./styles.css";
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import 'react-accessible-accordion/dist/fancy-example.css';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import moment from 'moment';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(2),
    width: '25ch',
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function App() {

	let history = useHistory();
  var [roadTaxAmount, setroadTaxAmount] = React.useState("");
  var [roadTaxNumber, setroadTaxNumber] = React.useState("");

  var [insuranceCompany, setinsuranceCompany] = React.useState("");
  var [insuranceComments, setinsuranceComments] = React.useState("");
  var [vehicleID, setvehicleID] = React.useState("");
  var [resetVehicleID, setresetVehicleID] = React.useState("resetVehicleID");
  var [typeError, settypeError] = React.useState(false);
  var [companyError, setcompanyError] = React.useState(false);
  var [insuranceCompanyError, setinsuranceCompanyError] = React.useState(false);
  var [spComments, setspComments] = React.useState("");
  var [npComments, setnpComments] = React.useState("");
  var [roadTaxAmountError, setroadTaxAmountError] = React.useState(false);
  var [hasNationalPermit, sethasNationalPermit] = React.useState("false");
  var [vehicleIDError, setvehicleIDError] = React.useState(false);
  const classes = useStyles();
  const [type, setType] = React.useState('');
  const [company, setCompany] = React.useState('');
  const [permit, setPermit] = React.useState('');
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [vehicleDetails, setvehicleDetails] = React.useState("");
  const [roadTaxComments, setroadTaxComments] = React.useState("");
  const [errorMsg, seterrorMsg] = React.useState("");
  const [fcComments, setfcComments] = React.useState("");
  const [vehicleModel, setvehicleModel] = React.useState(new Date(moment()));
  const [insuranceToDate, setInsuranceToDate] = React.useState(new Date(moment()));
  const [spermitToDate, setspermitToDate] = React.useState(new Date(moment()));
  const [npermitToDate, setnpermitToDate] = React.useState(new Date(moment()));
  const [roadtaxToDate, setRoadTaxToDate] = React.useState(new Date(moment()));
  const [fcToDate, setfcToDate] = React.useState(new Date(moment()));

  const handleTypeChange = (event) => {
    setType(event.target.value);
    settypeError(false);
  };
  const handleCompanyChange = (event) => {
    setCompany(event.target.value);
    setcompanyError(false);
  };

  const handleInsuranceToDate = (date) => {
    setInsuranceToDate(date);
  };
  const handleVehicleModelChange = (date) => {
    setvehicleModel(date);
  };
  const handleNPermitToDate = (date) => {
    setnpermitToDate(date);
  };
  const handleSPermitToDate = (date) => {
    setspermitToDate(date);
  };

  const handleRoadTaxToDate = (date) => {
    setRoadTaxToDate(date);
  };

  const handleFCToDate = (date) => {
    setfcToDate(date);
  };

  useEffect(() => {
	  
	  loadDataForAdmin();
   
  }, []);
  
  const loadDataForAdmin = async () => {
		
	if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				 axios.get("http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getAllVehicleDetails")
					  .then(function (response) {
						setvehicleDetails(response.data);
						console.log(response.data);
					  }).catch(function (error) {
						console.log(error);
					  })
	  } else {
		  history.push('/home');
	  }
	  
	}

  const handleClick = (e) => {
    var eflag = false;
    e.preventDefault();
    if (vehicleID === "") {
      eflag = true;
      setvehicleIDError(true);
    }
    if (type === "") {
      eflag = true;
      settypeError(true);
    }
    if (company === "") {
      eflag = true;
      setcompanyError(true);
    }

    if (insuranceCompany === "") {
      eflag = true;
      setinsuranceCompanyError(true);
    }


    if (eflag === true) {
      seterrorMsg("Please enter all mandatory fields");
      setSuccessOpen(false);
      setErrorOpen(true);
    }
    else {
      var nationalPermit = "";
      var nationalStatus = "";
      if(hasNationalPermit==="true")
      {
        nationalPermit = moment(npermitToDate).format("DD-MM-YYYY");
        nationalStatus = npComments;
      }
      console.log(vehicleID);
      setErrorOpen(false);
      axios.post("http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/addOrsaveVehicleDetails", {
        "vehicleNumber": vehicleID,
        "vehicleModel": moment(vehicleModel).format("DD-MM-YYYY"),
        "vehicleType": type,
        "vehicleCompany": company,
        "insuranceName": insuranceCompany,
        "insuranceValiditiy": moment(insuranceToDate).format("DD-MM-YYYY"),
        "insuranceStatus": insuranceComments,
        "fcValidity": moment(fcToDate).format("DD-MM-YYYY"),
        "fcStatus": fcComments,
        "roadTaxValiditiy": moment(roadtaxToDate).format("DD-MM-YYYY"),
        "roadTaxStatus": roadTaxComments,
        "roadTaxAmount": roadTaxAmount,
        "npValiditiy": nationalPermit,
        "npStatus": nationalStatus,
        "tnPermitValiditiy": moment(spermitToDate).format("DD-MM-YYYY"),
        "tnPermitStatus": spComments,
        "hasNationalPermit" : hasNationalPermit
      })
        .then(function (response) {
          console.log(response.data);
          setSuccessOpen(true);
          axios.get("http://ec2-13-235-90-192.ap-south-1.compute.amazonaws.com:8080/ut/getAllVehicleDetails")
            .then(function (response) {
              setvehicleDetails(response.data);
              console.log(response.data);
            }).catch(function (error) {
              console.log(error);
            })
        }).catch(function (error) {
          console.log(error);
          seterrorMsg(error);
          setErrorOpen(true);
        })
      var vtime = moment().format("hhmmss");
      setresetVehicleID("resetVehicleID" + vtime);
      setvehicleID("");
      setType("");
      setCompany("");
      setinsuranceCompany("");
      setroadTaxAmount("");
      setinsuranceComments("");
      setspComments("");
      setnpComments("");
      setroadTaxComments("");
      setfcComments("");
      setInsuranceToDate(new Date(moment()));
      setRoadTaxToDate(new Date(moment()));
      setspermitToDate(new Date(moment()));
      setnpermitToDate(new Date(moment()));
      setfcToDate(new Date(moment()));
      setvehicleModel(new Date(moment()));

    }
  };
  const handleVehicleIDSelect = (event, newvalue) => {
    setvehicleIDError(false);
    setvehicleID(newvalue);
    var vn = newvalue;
    var i = vehicleDetails.find(item => {
      return item.vehicleNumber === vn
    })
    console.log(i);
    if (i) {
      setType(i.vehicleType);
      setCompany(i.vehicleCompany);
      setvehicleModel(moment(i.vehicleModel,"DD/MM/YYYY"));
      setinsuranceCompany(i.insuranceName);
      setInsuranceToDate(moment(i.insuranceValiditiy, "DD/MM/YYYY"));
      setinsuranceComments(i.insuranceStatus);
      setfcToDate(moment(i.fcValidity, "DD/MM/YYYY"));
      setfcComments(i.fcStatus);
      setRoadTaxToDate(moment(i.roadTaxValiditiy, "DD/MM/YYYY"));
      setroadTaxAmount(i.roadTaxAmount);
      setroadTaxComments(i.roadTaxStatus);
      if(i.npValiditiy==="")
        setnpermitToDate(new Date(moment()));
      else
        setnpermitToDate(moment(i.npValiditiy, "DD/MM/YYYY"));
      setnpComments(i.npStatus);
      setspermitToDate(moment(i.tnPermitValiditiy, "DD/MM/YYYY"));
      setspComments(i.tnPermitStatus);
      sethasNationalPermit(i.hasNationalPermit);
    }
  };
  const handleVehicleIDBlur = (event) => {
    setvehicleID(event.target.value);
  };
  const handleVehicleIDChange = (event) => {
    console.log("here");
    setvehicleID(event.target.value);
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    setErrorOpen(false);
  };
  function handleToggle(){
    if(hasNationalPermit==="true")
        sethasNationalPermit("false");
    else
        sethasNationalPermit("true");
  }
  return (

    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <h2>Add/Edit Vehicle Details</h2>
        <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
          <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
            <Autocomplete
              id="vehicleID"
              freeSolo
              options={vehicleDetails ? vehicleDetails.map((option) => option.vehicleNumber) : null}
              key={resetVehicleID}
              value={vehicleID}
              onChange={handleVehicleIDSelect}
              renderInput={(params) => <TextField {...params} onInputCapture={handleVehicleIDChange} label="Vehicle ID" margin="normal" error={vehicleIDError} helperText={vehicleIDError ? "Vehicle ID is required" : ""} />}
            />
            <FormControl className={classes.formControl} error={typeError}  >
              <InputLabel id="select type label">Type</InputLabel>
              <Select
                className="mdb-select"
                labelId="select type label"
                id="select type"
                label="Type"
                value={type}
                onChange={handleTypeChange}
              >
                <MenuItem value={"20"}>20'</MenuItem>
                <MenuItem value={"40"}>40'</MenuItem>
              </Select>
              <FormHelperText>{typeError ? "Select valid vehicle type" : ""}</FormHelperText>
            </FormControl>
            <FormControl className={classes.formControl} error={companyError}>
              <InputLabel id="select company label">Company</InputLabel>
              <Select
                className="mdb-select"
                labelId="select company label"
                id="select type"
                label="Company"
                value={company}
                onChange={handleCompanyChange}
              >
                <MenuItem value={"UNITED TRANSPORTS"}>UNITED TRANSPORTS</MenuItem>
                <MenuItem value={"TIMESCAN LOGISTICS"}>TIMESCAN LOGISTICS</MenuItem>
              </Select>
              <FormHelperText>{companyError ? "Select valid Company" : ""}</FormHelperText>
            </FormControl>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                //disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Model make"
                value={vehicleModel}
                autoOk
                onChange={handleVehicleModelChange}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
          </div>
          <br />
          <div align="left" style={{ marginLeft: "4em" }}>

            <TextField required id="standard-required" label="Insurance Company" value={insuranceCompany} defaultValue=""
              onChange={e => { setinsuranceCompany(e.target.value); }}
            />
            <MuiPickersUtilsProvider utils={DateFnsUtils}>

              <KeyboardDatePicker
                //disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Insurance validity"
                value={insuranceToDate}
                autoOk
                onChange={handleInsuranceToDate}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <TextField required id="standard-required" label="Insurance Comments" value={insuranceComments} defaultValue=""
              onChange={e => { setinsuranceComments(e.target.value); }}
            />
          </div>
          <br />
          <div align="left" style={{ marginLeft: "4em" }}>

            <MuiPickersUtilsProvider utils={DateFnsUtils}>

              <KeyboardDatePicker
                //disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="State Permit Validity"
                value={spermitToDate}
                autoOk
                onChange={handleSPermitToDate}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <TextField required id="standard-required" label="State Permit Comments" value={spComments} defaultValue=""
              onChange={e => { setspComments(e.target.value); }}
            />
            <br/>
             <RadioGroup aria-label="quiz" name="radioValue" value={hasNationalPermit} onChange={handleToggle} row>
                        <FormControlLabel value="true" control={<Radio color="default" />} label="Has National Permit" />
                        <FormControlLabel value="false" control={<Radio color="default" />} label="No National Permit" />
                      </RadioGroup>
             {hasNationalPermit==="true"?     ( <div>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
             
              <KeyboardDatePicker
                //disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="National Permit Validity"
                value={npermitToDate}
                autoOk
                onChange={handleNPermitToDate}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <TextField required id="standard-required" label="National Permit Comments" value={npComments} defaultValue=""
              onChange={e => { setnpComments(e.target.value); }}
            />
            </div>):null}
          </div>
          <br />
          <div align="left" style={{ marginLeft: "4em" }}>

            <MuiPickersUtilsProvider utils={DateFnsUtils}>

              <KeyboardDatePicker
                //disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Road Tax Validity"
                value={roadtaxToDate}
                autoOk
                onChange={handleRoadTaxToDate}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <TextField required id="standard-required" label="Road Tax Comments" value={roadTaxComments} defaultValue=""
              onChange={e => { setroadTaxComments(e.target.value); }}
            />
            <TextField
              id="standard-number"
              label="Amount"
              value={roadTaxAmount}
              type="number"
              onChange={e => { setroadTaxAmount(e.target.value); setroadTaxAmountError(false); }}
            />
          </div>
          <br />
          <div align="left" style={{ marginLeft: "4em" }}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                //disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="FC Validity"
                value={fcToDate}
                autoOk
                onChange={handleFCToDate}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <TextField required id="standard-required" label="FC Comments" value={fcComments} defaultValue=""
              onChange={e => { setfcComments(e.target.value); }}
            />
          </div>
          <br />

          <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={handleClick}>Submit</Button>

        </form>
      </div>
      <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Details submitted successfully !
        </Alert>
      </Snackbar>
      <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errorMsg}
        </Alert>
      </Snackbar>
    </div>
  );
}

export default App;



