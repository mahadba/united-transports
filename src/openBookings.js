import React, { useState, useEffect } from 'react';
import { customerDetail } from "./Constants";
import "react-table-6/react-table.css";
import "./styles.css";
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Snackbar from '@material-ui/core/Snackbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import * as myConstClass from './Constants.js';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment';
import ReactTable from 'react-table-6';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { TramOutlined } from '@material-ui/icons';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import PictureAsPdf from '@material-ui/icons/PictureAsPdf';
import { PDFViewer } from '@react-pdf/renderer';
import { Document, Page, View, Text, PDFDownloadLink, StyleSheet, pdf } from '@react-pdf/renderer';
import Axios from 'axios';
import { saveAs } from 'file-saver';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';



const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(2),
    width: '25ch',
  },
  MuiSvgIcon: {
    color: "yellow",
  }
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};


function App() {

  const classes = useStyles();
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [currentID, setCurrentID] = React.useState("");
  const [ownVehicleID, setownVehicleID] = React.useState("");
  const [resetownVehicleID, setresetownVehicleID] = React.useState("ownVehicle");
  const [ownVehicleIDError, setownVehicleIDError] = React.useState(false);
  const [fromLocation, setfromLocation] = React.useState("");
  const [resetfromLocation, setresetfromLocation] = React.useState("fromLocation");
  const [fromLocationError, setfromLocationError] = React.useState(false);
  const [salary, setsalary] = React.useState("");
  const [salaryError, setsalaryError] = React.useState(false);
  const [toLocation, settoLocation] = React.useState("");
  const [resettoLocation, setresettoLocation] = React.useState("toLocation");
  const [toLocationError, settoLocationError] = React.useState(false);
  const [driverAdvanceAmount, setdriverAdvanceAmount] = React.useState("");
  const [delivery, setDelivery] = React.useState("");
  const [deliveryError, setDeliveryError] = React.useState(false);
  const [customer, setcustomer] = React.useState(customerDetail[0]["contactName"]);
  const [resetcustomer, setresetcustomer] = React.useState("customer");
  const [customerError, setcustomerError] = React.useState(false);
  const [driver, setDriver] = React.useState('');
  const [driverError, setdriverError] = React.useState(false);
  const [resetDriver, setresetDriver] = React.useState("driver");
  const [thirdPartyID, setthirdPartyID] = React.useState("");
  const [resetthirdPartyID, setresetthirdPartyID] = React.useState("thirdParty");
  const [thirdPartyVehicleIDError, setthirdPartyVehicleIDError] = React.useState(false);
  const [thirdPartyVehicleType, setthirdPartyVehicleType] = React.useState("");
  const [showOwnVehicle, setshowOwnVehicle] = React.useState(true);
  const [ownVehicleType, setownVehicleType] = React.useState("");
  const [phoneNumber, setphoneNumber] = React.useState("");
  const [vedhicleIDInput, setvedhicleIDInput] = React.useState("");
  const [mylocationDetails, setmylocationDetails] = React.useState("");
  const [vehicleCompany, setVehicleCompany] = React.useState("");
  const [infoMessage, setinfoMessage] = React.useState("");
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [infoopen, setinfoopen] = React.useState(false);
  const [thirdPartyCompany, setthirdPartyCompany] = React.useState("");
  const [vehicleNumber, setvehicleNumber] = React.useState('');
  const [vehicleNumberError, setvehicleNumberError] = React.useState(false);
  const [RTO, setRTO] = React.useState('');
  const [toll, settoll] = React.useState('');
  const [containerID, setcontainerID] = React.useState('');
  const [containerIDError, setcontainerIDError] = React.useState(false);
  const [pickupDate, setpickupDate] = React.useState(moment());
  const [dropDate, setdropDate] = React.useState(moment());
  const [invoiceOpen, setinvoiceOpen] = React.useState(false);
  const [custAddress, setcustAddress] = React.useState('');
  const [custGST, setCustGST] = React.useState('');
  const [successMsg, setsuccessMsg] = React.useState('');
  const [deleteOpen, setdeleteOpen] = React.useState('');
  const [hover, sethover] = React.useState(false);
  const [bookingDetails, setbookingDetails] = React.useState();
  const [editMode, seteditMode] = React.useState(false);
  const [dailogTitle, setdialogTitle] = React.useState("");
  const [invoiceAmount, setinvoiceAmount] = React.useState("");
  const [customerAdvance, setcustomerAdvance] = React.useState("");
  const [customerAdvanceError, setcustomerAdvanceError] = React.useState("");
  // const [cusotmerReset, setcustomerReset] = React.useState("");
  const [driverDetails, setdriverDetails] = React.useState([]);
  const [vehicleDetails, setvehicleDetails] = React.useState([]);
  const [locationDetails, setlocationDetails] = React.useState([]);
  const [customerDetails, setcustomerDetails] = React.useState([{ "customerName": "" }]);
  //const [currentBooking, setcurrentBooking] = React.useState([]);
  const [vehicleList, setvehicleList] = useState([]);
  const [bookingToApproveList, setBookingToApproveList] = useState([]);

  //  const [initialList, setInitialList] = useState([]);

  const API_URL = myConstClass.GET_BOOKING_DETAILS;
  const BOOKIMG_DETAILS_BY_ID = myConstClass.GET_BOOKING_DETAILS_BY_ID;
  const CUSTOMER_URL = myConstClass.GET_CUSTOMER_DETAILS;
  const LOCATION_URL = myConstClass.GET_LOCATION_DETAILS;
  const VECHICLE_URL = myConstClass.GET_VEHICLES_DETAILS;
  const DRIVER_URL = myConstClass.GET_DRIVER_DETAILS;

  const UPDATE_BOOKING_DETAILS = myConstClass.UPDATE_BOOKING_DETAILS;
  const APPROVE_OPEN_BOOKING = myConstClass.APPROVE_OPEN_BOOKING;
  const CLOSE_OPEN_AP_BOOKING = myConstClass.CLOSE_OPEN_AP_BOOKING;
  const APPROVE_CLOSE_BOOKING = myConstClass.APPROVE_CLOSE_BOOKING;
  const DELETE_BOOKING_DETAILS = myConstClass.DELETE_BOOKING_DETAILS;
  //const GET_LOCATION_DETAILS_BOOKING = myConstClass.GET_LOCATION_DETAILS_BOOKING;


  var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
  var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];


  useEffect(() => {
    loadData();
    loadCustomerData();
    loadLocationData();
    loadVehicleData();
    loadDriverData();
	//loadLocationDataForBooking();
  }, []);

  const loadData = async () => {
   var response = [];
	  if(localStorage.getItem("accessType") === 'admin' ){
			response = await fetch(API_URL);
	  } else {
		  response = await fetch(BOOKIMG_DETAILS_BY_ID+localStorage.getItem("userName"));
	  }
    const data = await response.json();
    setbookingDetails(data);
    console.log(data);
  }
  const loadCustomerData = async () => {
    const response = await fetch(CUSTOMER_URL);
    const data = await response.json();
    setcustomerDetails(data);
    setcustomer(data[0]["customerName"])
    console.log(data);
  }
  const loadLocationData = async () => {
    const response = await fetch(LOCATION_URL);
    const data = await response.json();
    setlocationDetails(data);
    console.log(data);
  }
  
  
  const loadVehicleData = async () => {
    const response = await fetch(VECHICLE_URL);
    const data = await response.json();
    setvehicleDetails(data);
    console.log(data);
  }
  const loadDriverData = async () => {
    const response = await fetch(DRIVER_URL);
    const data = await response.json();
    setdriverDetails(data);
    console.log(data);
  }

  const handleInputChange = (e, index) => {
    // const { name, value } = e.target;
    const { value } = e.target;
    const list = [...vehicleList];
    list[index][e.target.name] = value;
    console.log(e.target.name + value);
    if (e.target.name === "radioValue") {
      list[index]["vehicleOwnedBy"] = value;
      if (value === "THIRD_PARTY") {
        list[index]["driverName"] = "";
        list[index]["driverAdvance"] = "";
        list[index]["driverPhoneNumber"] = "";
        list[index]["driverSalary"] = "";
        list[index]["haltingCharges"] = "";
        list[index]["rtoCharges"] = "";
        list[index]["tollCharges"] = "";
        list[index]["containerNumber"] = "";
      }
      list[index]["vehicleNumber"] = "";
      list[index]["vehicleType"] = "";
      list[index]["vehicleCompany"] = "";
      list[index]["containerNumber"] = "";
      list[index]["haltingCharges"] = "";

    }
    else if (e.target.id === "vehicleNumber") {
      list[index]["thirdPartyIDError"] = false;
    }
    else if (e.target.id === "vehicleType") {
      list[index]["thirdPartyTypeError"] = false;
    }
    else if (e.target.id === "vehicleCompany") {
      list[index]["thirdPartyCompanyError"] = false;
    }
    else if (e.target.id === "rtoCharges") {
      list[index]["rtochargesError"] = false;
    }
    else if (e.target.id === "containerNumber") {
      list[index]["containerNumberError"] = false;
    }
    else if (e.target.id === "haltingCharges") {
      list[index]["haltingChargesError"] = false;
    }

    setvehicleList(list);
  };


  const handleAddClick = () => {
    setvehicleList([...vehicleList, { "tripId": "", "vehicleNumber": "", "vehicleType": "", "vehicleCompany": "", "importOrExport": "IMPORT", "containerNumber": null, "driverId": null, "driverName": null, "driverAdvance": "", "driverPhoneNumber": null, "driverSalary": null, "customerId": "", "customerName": "", "customerAdvance": "", "billAmount": "", "locationId": "", "loadingPoint": "", "destination": "", "tollCharges": "", "rtoCharges": "", "haltingCharges": "", "vehicleOwnedBy": "OWN_VEHICLE" }]);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...vehicleList];
    list.splice(index, 1);
    setvehicleList(list);
  };
  /*
    const returnCustomerName = (customerId) => {
      console.log(customerId);
      //var value = customerDetails.find(item => {return item.customerId === customerId});
      var i = customerDetails.filter(item => {
        return item.customerId === customerId
      });
      return i[0].customerName;
    }
  */
  function inWords(num) {
    console.log(num);
    if ((num = num.toString()).length > 9) return 'overflow';
    var n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    console.log(n);
    if (!n) return; var str = 'INR ';
    str += (parseInt(n[1]) !== 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (parseInt(n[2]) !== 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (parseInt(n[3]) !== 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (parseInt(n[4]) !== 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (parseInt(n[5]) !== 0) ? ((str !== '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : 'only';
    return str.toUpperCase();
  }
  const columns = [
    {
      Header: "",
      id: "checkbox",
      accessor: "bookingId",
      Cell: props => <input style={{ width: "20px", height: "20px" }} type={"checkbox"} id={props.value} name={"bookingCheck"} disabled={(props.row.status === "OPEN_APPROVED" || props.row.status === "CLOSE_APPROVED") ? true : false} />,
      Filter: ({ filter, onChange }) => (null),
      width: 30
    },
    {
      id: "bookingId",
      Header: 'Booking ID',
      accessor: 'bookingId',
      Cell: props => <span onClick={onLInkClick} style={{ color: "blue", cursor: "pointer" }} value={props.value}>{props.value}</span>
    },  // String-based value accessors!

    {
      id: "Customer",
      Header: "Customer", // Custom header components!
      accessor: d => d.tripDetails == null ? "" : d.tripDetails[0].customerName
    },
    {
      Header: "Delivery Type", // Custom header components!
      id: "DeliveryType",
      accessor: d => d.tripDetails == null ? "" : d.tripDetails[0].importOrExport
    },
    {
      Header: "Source Location", // Custom header components!
      accessor: d => d.tripDetails == null ? "" : d.tripDetails[0].loadingPoint,
      id: "loadingPoint"
    },
    {
      Header: "Destination Location",
      id: "Destination", // Custom header components!
      accessor: d => d.tripDetails == null ? "" : d.tripDetails[0].destination
    },
    {
      Header: "Movement-Date", // Custom header components!
      id: "bookingPickupDate",
      accessor: 'bookingPickupDate'
    },

    {
      id: "status",
      Header: "Status", // Custom header components!
      accessor: "bookingStatus",
      Filter: ({ filter, onChange }) => (
        <select
          onChange={event => onChange(event.target.value)}
          style={{ width: "90%", textAlign: "center" }}
          value={filter ? filter.value : ""}
        >

          <option value="OPEN">Open</option>
          <option value="CLOSE">Close</option>
          <option value="OPEN_APPROVED">Open Approved</option>
          <option value="CLOSE_APPROVED">Close Approved</option>
        </select>
      )
    },
    {
      accessor: 'bookingId',
      Cell: props => (<div id={props.value}><PictureAsPdf id={props.value} style={{ color: "#000033", cursor: "pointer" }} value={props.value} onClick={handlePDF} />&nbsp;&nbsp;<Edit id={props.value} style={{ color: "#000033", cursor: "pointer" }} value={props.value} onClick={handleEdit} onMouseOver={handleEditEnter} onMouseOut={handleLeave} />&nbsp;&nbsp;<Delete id={props.value} style={{ color: "#000033", cursor: "pointer" }} onClick={handleDelete} onMouseOver={handleEnter} onMouseOut={handleLeave} /> </div>),
      Filter: ({ filter, onChange }) => (null),
      width: 100
    }
  ];
  const innerColumns = [
    {
      id: "bookingId",
      Header: 'Vehicle ID',
      accessor: 'vehicleNumber',
      //Cell: props => <span onClick={onLInkClick} style={{ color: "blue", cursor: "pointer" }} value={props.value}>{props.value}</span>
    },  // String-based value accessors!
    {
      Header: "Type", // Custom header components!
      accessor: "vehicleType"
    },
    {
      Header: "Driver", // Custom header components!
      accessor: "driverName"
    },

    {
      Header: "Container Number", // Custom header components!
      accessor: "containerNumber",
      id: "loadingPoint"
    },
    {
      Header: "Driver Salary", // Custom header components!
      accessor: "driverSalary"
    },
    {
      Header: "Driver Advance", // Custom header components!
      accessor: "driverAdvance"
    },
    {
      Header: "RTO", // Custom header components!
      accessor: "rtoCharges"
    },
    {
      Header: "Toll", // Custom header components!
      accessor: "rtoCharges"
    },
    {
      Header: "Halting Charges", // Custom header components!
      accessor: "haltingCharges"
    },
    {
      Header: "Invoice Amount", // Custom header components!
      accessor: "customerInvoiceAmount"
    },
  ];
  const handleEditEnter = (e) => {
    e.target.style.color = "green";
  }

  /*
    const pdfDocument = () => (
      <Document>
        <Page>
  
        </Page>
      </Document>
    );
  */
  const printDocument = async (id) => {
    //  const input = document.getElementById('invoiceTable');
    //const pdf = new jsPDF('p', 'mm', 'a4');

    const blob = await pdf((mydocument(id))).toBlob();
    saveAs(blob, "Invoice");

    //setErrorOpen(false);
    //setSuccessOpen(true);
    // html2canvas(input)
    //   .then((canvas) => {
    //     var imgWidth = 200;
    //     var pageHeight = 290;
    //     var imgHeight = canvas.height;
    //     var imgWidth = canvas.width;
    //     var pheight = pdf.internal.pageSize.getHeight();
    //     var pwidth = pdf.internal.pageSize.getWidth();
    //     var ratio = imgHeight / imgWidth;
    //     pdf.setFillColor("white");

    //     const imgData = canvas.toDataURL('image/png');
    //     pheight = ratio * pwidth;
    //     var position = 30;
    //     //var heightLeft = imgHeight;
    //     //pdf.addImage(imgData, 'JPG', -40, 40, 300, 220,'','FAST');     
    //     if (window.innerWidth > 1200)
    //       pdf.addImage(imgData, 'JPEG', 30, 40, pwidth * 0.7, pheight * 0.7, '', 'FAST');
    //     else
    //       pdf.addImage(imgData, 'JPEG', 30, 60, pwidth * 0.7, pheight * 0.7, '', 'FAST');

    //     pdf.output('dataurlnewwindow');
    //     //pdf.save(moment().format("DDMMYYhhmmss")+".pdf");  
    //   });
  };
  const handleDelete = (e) => {
    setCurrentID(e.target.parentNode.id);
    setdeleteOpen(true);
  }
  const handleEnter = (e) => {
    e.target.style.color = "red";
  }
  const handleLeave = (e) => {
    e.target.style.color = "#000033";
  }
  const onLInkClick = (event) => {
    setSuccessOpen(false);
    event.preventDefault();
    setDialogOpen(true);
    setdialogTitle("Close Booking");
    console.log(event.target.innerText);
    populatePopUp(event.target.innerText);
    seteditMode(false);
  };
  const handleEdit = (e) => {
    seteditMode(true);
    setCurrentID(e.target.parentNode.id);
    console.log(e.target.parentNode.id);
    setDialogOpen(true);
    setdialogTitle("Edit Booking");
    populatePopUp(e.target.parentNode.id);
  }
  const handlePDF = (e) => {
    setCurrentID(e.target.parentNode.id);
    var i = bookingDetails.find(item => {
      return item.bookingId === e.target.parentNode.id
    });

    if(i.bookingStatus === "OPEN" || i.bookingStatus ==="OPEN_APPROVED")
    {
      setinfoMessage("Invoice is available only for closed bookings!");
            setinfoopen(true);
    }
    else
    printDocument(e.target.parentNode.id);
  }

  function populatePopUp(booking) {
    var i = bookingDetails.find(item => {
      return item.bookingId === booking
    });
    if (i) {
      console.log("afskfdjksljdfs : " + i.bookingPickupDate);
      console.log("afskfdjksljdfs vehicleCompany: " + i.tripDetails[0]["vehicleCompany"]);
      console.log(moment(i.bookingOpenDate).format("DD/MM/YYYY"));
      setpickupDate(moment(i.bookingOpenDate).format("MM/DD/YYYY"));
      setcustomer(i.tripDetails[0]["customerName"]);
      setvehicleList(i.tripDetails);
      setDelivery(i.tripDetails[0]["importOrExport"]);
      settoLocation(i.tripDetails[0]["destination"]);
      setfromLocation(i.tripDetails[0]["loadingPoint"]);
      setinvoiceAmount(i.tripDetails[0]["customerInvoiceAmount"]);
      setcustomerAdvance(i.tripDetails[0]["customerAdvance"]);

      // const list = i.tripDetails;
      for (var k = 0; k < i.tripDetails.length; k++) {
        if (i.tripDetails[k]["vehicleOwnedBy"] === "THIRD_PARTY") {

        }
      }

      console.log(i.tripDetails);

      if (i.status === "Closed")
        setdropDate(moment(i.dropDate, "DD/MM/YYYY"));
      /*var vn = i.fromLocation;
      var iloc = locationDetails.filter(item => {
        return item.fromLocation === vn
      });
      var cust = customerDetails.find(item => {
        return item.customer === i.customer
      })*/
      setErrorOpen(false);
      setinfoopen(false);
    }
    setCurrentID(booking);
  }

  const handleDeleteConfirm = () => {



    console.log("delete : " + currentID);

    const params = {
      "bookingId": currentID
    }

    Axios.post(DELETE_BOOKING_DETAILS, params)
      .then(response => {
        console.log("validated Axios");
        //	setCustValidationStatus(response.data.validationStatus);
        //	setCustValidationMsg(response.data.validationMessage);


        if (response.data.validationStatus) {

          loadData();

          setdeleteOpen(false);
          setsuccessMsg("Booking " + currentID + " deleted successfully!");
          setSuccessOpen(true);
        } else {
          setdeleteOpen(true);
          //setsuccessMsg("Entry deleted successfully!");
          setSuccessOpen(false);
        }

      })
      .catch(error => {
        console.log("Error Axios");
        console.log(error);
        setdeleteOpen(true);
        //setsuccessMsg("Entry deleted successfully!");
        setSuccessOpen(false);
      });




  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    seteditMode(false);
  };
  const handleInvoiceClose = () => {
    setinvoiceOpen(false);
    printDocument();
  };
  const handleDeleteClose = () => {
    setdeleteOpen(false);
  };
  const handledeliveryChange = (event) => {
    setDelivery(event.target.value);
    setDeliveryError(false);
  };
  const handleOwnVehicleIDChange = (e, i) => {
    //const { name, value } = e.target;
    const { value } = e.target;
    const list = [...vehicleList];

    var vn = value;
    var it = vehicleDetails.find(item => {
      return item.vehicleNumber === vn
    });
    if (it) {
      list[i]["vehicleNumber"] = it.vehicleNumber;
      list[i]["vehicleType"] = it.vehicleType;
      list[i]["vehicleCompany"] = it.vehicleCompany;
      list[i]["ownVehicleIDError"] = false;
      setvehicleList(list);
    }
  };
  /*const handlethirdPartyVehicleIDChange = (event) => {
    setthirdPartyID(event.target.value);
    setvehicleNumber(event.target.value);
    var vn = event.target.value;
    var i2 = vehicleDetails.find(item => {
      return item.vehicleNumber === vn
    });
    if (i2) {
      setthirdPartyVehicleType(i2.vehicleType);

      setvehicleNumberError(false);
    }
  };*/
  const handleDriverChange = (e, i) => {
    //  const { name, value } = e.target;
    const { value } = e.target;
    const list = [...vehicleList];
    console.log("driver" + value);
    var vn = value;
    var it = driverDetails.find(item => {
      return item.firstName === vn
    });
    if (it) {
      // const driverID = it.driverId;

      list[i]["driverPhoneNumber"] = it.phoneNumber;
      list[i]["driverError"] = false;
      list[i]["driverId"] = it.driverId;
      list[i]["driverName"] = value;
      setvehicleList(list);
    }
    var locDet = locationDetails.filter(item => {
      return item.customerName === customer
    });
    for (var j = 0; j < locDet.length; j++) {
      if ((locDet[j].sourceLocation === fromLocation) && (locDet[j].destinationLocation === toLocation && locDet[j].vehicleType === list[i]["vehicleType"])) {
        (list[i]["driverSalary"] = locDet[j].driverSalary);
      }
    }
  };
  const handleFromLocationChange = (event, newvalue) => {

    if (mylocationDetails) {
      setfromLocation(newvalue.sourceLocation);
      var vn = newvalue.sourceLocation;
      setfromLocationError(false);
      var i = mylocationDetails.filter(item => {
        return item.sourceLocation === vn
      });
      setmylocationDetails(i);
      setfromLocationError(false);
    }
    else {
      setinfoMessage("Please select customer!");
      setinfoopen(true);
      var vtime = moment().format("hhmmss");
      setresetfromLocation("resettoLocation" + vtime);
      settoLocation("");
    }
  }
  const handleToLocationChange = (event, newvalue) => {
    if (fromLocation) {
      settoLocation(newvalue.destinationLocation);
      var vn = customer;
      var i = locationDetails.filter(item => {
        return (item.customerName === vn)
      });

      for (var j = 0; j < i.length; j++) {
        if ((i[j].sourceLocation === fromLocation) && (i[j].destinationLocation === newvalue.destinationLocation)) {
          setinvoiceAmount(i[j].customerInvoiceAmount);
        }
      }
      settoLocationError(false);
    }
    else {
      setinfoMessage("Please select source location before selecting destination!");
      setinfoopen(true);
      var vtime = moment().format("hhmmss");
      setresettoLocation("resettoLocation" + vtime);
      settoLocation("");
    }
  };

  const handleCustomerChange = (event, newvalue) => {
    console.log(newvalue);
    console.log(event.target.value);
    setcustomer(newvalue.customerName);
    var vn = newvalue.customerName;
    var i = locationDetails.filter(item => {
      return item.customerName === vn
    });
    setmylocationDetails(i);
    setcustomerError(false);
  };
  const handleApprove = () => {
	  
	  if(localStorage.getItem("accessType") === 'admin' ) {
    var checkList = document.getElementsByName("bookingCheck");


    for (var i = 0; i < checkList.length; i++) {
      if (checkList[i].checked) {
        console.log(checkList[i].id);
     
        bookingToApproveList.push({ 
		"bookingId": checkList[i].id ,
		"bookingOpenApprovedBy": localStorage.getItem("userName"),
		"bookingCloseApprovedBy": localStorage.getItem("userName")
		});
		
       
      }

    }
    if (bookingToApproveList.length > 0) {
      var it = bookingDetails.find(item => { return item.bookingId === checkList[0].id });
      if (it) {
        console.log(it.bookingStatus);
        if (it.bookingStatus === "OPEN") {
          handleApproveOpenBooking(bookingToApproveList)
        }
        else if (it.bookingStatus === "CLOSE") {
          handleApproveClosedBooking(bookingToApproveList)
        }
      }


      console.log(bookingToApproveList);
      setBookingToApproveList([]);

    }
    else {
      setinfoopen(true);
      setinfoMessage("Please select atleast one booking id");
    }
	} else
    {
      setinfoopen(true);
      setinfoMessage("Only Admin Can Approve a booking");
    }
  }
  const handleReject = () => {
	  if(localStorage.getItem("accessType") === 'admin' ) {
    var checkList = document.getElementsByName("bookingCheck");
    for (var i = 0; i < checkList.length; i++) {
      if (checkList[i].checked)
        console.log(checkList[i].id);
    }
    var it = bookingDetails.find(item => { return item.bookingId === checkList[0].id });
    if (it) {
      console.log(it.bookingStatus);
    }
	} else
    {
      setinfoopen(true);
      setinfoMessage("Only Admin Can Reject a booking");
    }
  }

  function handleApproveClosedBooking(bookinglist) {

    Axios.post(APPROVE_CLOSE_BOOKING, bookingToApproveList)
      .then(response => {
        console.log("validated Axios");

        console.log("response.data.validationStatus : " + response.data.validationStatus);
        console.log("response.data.validationMessage : " + response.data.validationMessage);
        if (response.data.validationStatus) {
          //setSuccessOpen(true);
          //	clearAll();
          loadData();
        } else {
          //setErrorOpen(true);
        }
      })
      .catch(error => {
        console.log("Error Axios");
        console.log(error);
        //  seterrorMsg(error.message);
        //	setErrorOpen(true);

      });
  }
  /*
    function handleCloseOpenApprovedBooking(bookingList) {
  
      const params = {
        "bookingId": "",
        "bookingClosedBy": "",
        "paymentReceived": false,
        "tripDetailsList": [
          {
            "tripId": "",
            "containerNumber": "",
            "tollCharges": "",
            "rtoCharges": "",
            "haltingCharges": ""
          }
        ]
  
      }
      Axios.post(CLOSE_OPEN_AP_BOOKING, params)
        .then(response => {
          console.log("validated Axios");
  
          console.log("response.data.validationStatus : " + response.data.validationStatus);
          console.log("response.data.validationMessage : " + response.data.validationMessage);
          if (response.data.validationStatus) {
            //setSuccessOpen(true);
            //	clearAll();
          } else {
            //setErrorOpen(true);
          }
        })
        .catch(error => {
          console.log("Error Axios");
          console.log(error);
          //  seterrorMsg(error.message);
          //	setErrorOpen(true);
  
        });
  
    }
  */

  function handleApproveOpenBooking(bookingToApproveList) {

    Axios.post(APPROVE_OPEN_BOOKING, bookingToApproveList)
      .then(response => {
        console.log("validated Axios");

        console.log("response.data.validationStatus : " + response.data.validationStatus);
        console.log("response.data.validationMessage : " + response.data.validationMessage);
        if (response.data.validationStatus) {
          //setSuccessOpen(true);
          //	clearAll();

          loadData();
        } else {
          //setErrorOpen(true);
        }
      })
      .catch(error => {
        console.log("Error Axios");
        console.log(error);
        //  seterrorMsg(error.message);
        //	setErrorOpen(true);

      });


  }
  const styles = StyleSheet.create({
    em: {
      fontStyle: 'bold'
    },
    table: {
      width: '100%',
      borderWidth: 2,
      display: 'flex',
      flexDirection: 'column',
      marginVertical: 12
    },
    tableRow: {
      display: 'flex',
      flexDirection: 'row',
      border: 'none',
    },
    tableRowBody: {
      display: 'flex',
      flexDirection: 'row',
      border: 'none'
    },
    cell: {
      borderWidth: 1,
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      textAlign: 'center',
      flexWrap: 'wrap',
      fontSize: 12,
      backgroundColor: "#C2DFFF",
      padding: 5
    },
    cellBody: {
      borderWidth: 1,
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      textAlign: 'left',
      flexWrap: 'wrap',
      fontSize: 10,
      paddingTop: 5,
      paddingBottom: 5
    },
    cellBodyLeft: {
      borderWidth: 1,
      borderRight: 'none',
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      textAlign: 'left',
      flexWrap: 'wrap',
      fontSize: 10,
      paddingTop: 5,
      paddingBottom: 5
    },
    cellBodyRight: {
      borderWidth: 1,
      borderLeft: 'none',
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      textAlign: 'left',
      flexWrap: 'wrap',
      fontSize: 10,
      paddingTop: 5,
      paddingBottom: 5
    }
  })
  function mydocument(bookingid) {
    var book = bookingDetails.find(item => { return item.bookingId === bookingid });
    var cust = customerDetails.find(item => { return item.customerId === book.tripDetails[0].customerId });
    return (
      <Document>
        <Page size="A4" wrap style={{}}>
          <View style={[styles.table, { width: "93%", margin: 20, marginRight: 20, marginTop: 50 }]}>
            <View style={styles.tableRow}>
              <View style={[styles.cell, { width: "100%", padding: 15, fontSize: 18 }]}>
                <Text>UNITED TRANSPORT</Text>
              </View>
            </View>
            <View style={[styles.tableRow]}>
              <View style={[styles.cell, { width: "50%", height: "300%", textAlign: "left" }]}>
                <Text>{cust.customerName}</Text>
                <Text>{cust.customerAddress1 + ", " + cust.customerAddress2}</Text>
              </View>
              <View style={[styles.cell, { width: "15%", textAlign: "left" }]}>
                <Text>Invoice No.</Text>
              </View>
              <View style={[styles.cell, { width: "35%", textAlign: "left" }]}>
                <Text>{book.billingInvoiceNumber}</Text>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[{ width: "50%", height: "0%", padding: 0 }]}>
              </View>
              <View style={[styles.cell, { width: "15%", textAlign: "left" }]}>
                <Text>Date</Text>
              </View>
              <View style={[styles.cell, { width: "35%", textAlign: "left" }]}>
                <Text>{moment().format("DD/MM/YYYY")}</Text>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[{ width: "50%", height: "0%", padding: 0 }]}>
              </View>
              <View style={[styles.cell, { width: "15%", textAlign: "left" }]}>
                <Text>PAN</Text>
              </View>
              <View style={[styles.cell, { width: "35%", textAlign: "left" }]}>
                <Text>{cust.customerPAN}</Text>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[{ width: "50%", height: "0%", padding: 0 }]}>
              </View>
              <View style={[styles.cell, { width: "15%", textAlign: "left" }]}>
                <Text>GST</Text>
              </View>
              <View style={[styles.cell, { width: "35%", textAlign: "left" }]}>
                <Text>{cust.customerGST}</Text>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[styles.cellBody, { width: "70%", textAlign: 'center' }]}>
                <Text>Description</Text>
              </View>
              <View style={[styles.cellBody, { width: "30%", textAlign: "center" }]}>
                <Text>Amount</Text>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[styles.cellBodyLeft, { width: "35%", borderBottom: "none", borderTop: "none", paddingLeft: 5 }]}>
                <Text>Delivery Type</Text>
              </View>
              <View style={[styles.cellBodyRight, { width: "35%", borderBottom: "none", borderTop: "none" }]}>
                <Text>: {book.tripDetails[0].importOrExport}</Text>
              </View>
              <View style={[{ width: "30%", height: "0%", padding: 0 }]}>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[styles.cellBodyLeft, { width: "35%", borderBottom: "none", borderTop: "none", paddingLeft: 5 }]}>
                <Text>Loading Point</Text>
              </View>
              <View style={[styles.cellBodyRight, { width: "35%", borderBottom: "none", borderTop: "none" }]}>
                <Text>: {book.tripDetails[0].loadingPoint}</Text>
              </View>
              <View style={[{ width: "30%", height: "0%", padding: 0 }]}>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[styles.cellBodyLeft, { width: "35%", borderBottom: "none", borderTop: "none", paddingLeft: 5 }]}>
                <Text>Dropping Point</Text>
              </View>
              <View style={[styles.cellBodyRight, { width: "35%", borderBottom: "none", borderTop: "none" }]}>
                <Text>: {book.tripDetails[0].destination}</Text>
              </View>
              <View style={[{ width: "30%", height: "0%", padding: 0 }]}>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[styles.cellBodyLeft, { width: "35%", borderTop: "none", paddingLeft: 5 }]}>
                <Text>Movement Date</Text>
              </View>
              <View style={[styles.cellBodyRight, { width: "35%", borderTop: "none" }]}>
                <Text>: {moment(book.bookingPickupDate).format("DD/MM/YYYY")}</Text>
              </View>
              <View style={[{ width: "30%", height: "0%", padding: 0 }]}>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[styles.cellBody, { width: "20%", padding: "2", textAlign: "center" }]}>
                <Text>VEHICLE NUMBER</Text>
              </View>
              <View style={[styles.cellBody, { width: "10%", padding: "2", textAlign: "center" }]}>
                <Text>TYPE</Text>
              </View>
              <View style={[styles.cellBody, { width: "20%", padding: "2", textAlign: "center" }]}>
                <Text>CONTAINER ID</Text>
              </View>
              <View style={[styles.cellBody, { width: "20%", padding: "2", textAlign: "center" }]}>
                <Text>HALTING CHARGE</Text>
              </View>
			   
              <View style={[{ width: "30%", height: "0%", padding: 0 }]}>
              </View>
            </View>
            {(function (displayItem, ing, len) {
              console.log("loooooping ");
              for (ing; ing < len; ing++) {
                displayItem.push(
                  <View style={styles.tableRow}>
                    <View style={[styles.cellBody, { width: "20%", textAlign: "center" }]}>
                      <Text>{book.tripDetails[ing].vehicleNumber}</Text>
                    </View>
                    <View style={[styles.cellBody, { width: "10%", textAlign: "center" }]}>
                      <Text>{book.tripDetails[ing].vehicleType}</Text>
                    </View>
                    <View style={[styles.cellBody, { width: "20%", textAlign: "center" }]}>
                      <Text>{book.tripDetails[ing].containerNumber}</Text>
                    </View>
                    <View style={[styles.cellBody, { width: "20%", textAlign: "center" }]}>
                      <Text>{book.tripDetails[ing].haltingCharges}</Text>
                    </View>
					<View style={[{ width: "20%", textAlign: "center" }]}>
                      <Text>{book.tripDetails[ing].customerInvoiceAmount}</Text>
                    </View>
                    <View style={[{ width: "50%", height: "0%", padding: 0 }]}>
                    </View>
                  </View>);
              }
              return (displayItem);
            })([], 0, book.tripDetails.length)}
            <View style={styles.tableRow}>
              <View style={[styles.cellBody, { width: "70%", padding: "5", textAlign: "center" }]}>
                <Text>Total</Text>
              </View>
              <View style={[styles.cellBody, { width: "30%", padding: "5", textAlign: "left", textTransform: "capitalize" }]}>
                <Text>{book.bookingTotalAmount}</Text>
              </View>
            </View>
            <View style={styles.tableRow}>
              <View style={[styles.cellBody, { width: "100%", padding: "5", textAlign: "center", fontSize: 10 }]}>
                <Text>For United Transport</Text>
                <Text> </Text>
                <Text> </Text>
                <Text> </Text>
                <Text> </Text>
                <Text>Autorised Signatory</Text>
              </View>
            </View>
          </View>
        </Page>
      </Document>);
  }

  /* function displayvehicleList() {
     let displayItem = [];
     for (var i = 2; i < 4; i++) {
       displayItem.push(
         <View style={styles.tableRow}>
           <View style={[styles.cellBody, { width: "12.5%" }]}>
             <Text>TN 00 AA 000{i}</Text>
           </View>
           <View style={[styles.cellBody, { width: "10%" }]}>
             <Text>40</Text>
           </View>
           <View style={[styles.cellBody, { width: "15%" }]}>
             <Text>123456</Text>
           </View>
           <View style={[styles.cellBody, { width: "12.5%" }]}>
             <Text>20000</Text>
           </View>
           <View style={[{ width: "50%", height: "0%", padding: 0 }]}>
           </View>
         </View>);
     }
     return (displayItem);
   }
 */
  const handleUpdateBooking = () => {
    console.log("update Booking : " + currentID);
    var customerId = "";
    var locationId = "";
    // var it = bookingDetails.find(item => {
    //  return item.bookingId === currentID
    //});
    if (handleValidation(vehicleList)) {

      const list = [...vehicleList];
      var vn = customer
      var it = locationDetails.filter(item => {
        return item.customerName === vn
      });

      if (it) {
        for (var i = 0; i < it.length; i++) {
          if ((it[0].sourceLocation === fromLocation) && (it[0].destinationLocation === toLocation)) {
            customerId = it[0].customerId;
            locationId = it[0].locationId;
          }
        }
      }
      for (var k = 0; k < list.length; k++) {
        list[k].customerName = customer;
        list[k].customerId = customerId;
        list[k].locationId = locationId;
        list[k].importOrExport = delivery;
        list[k].customerAdvance = customerAdvance;
        list[k].billAmount = invoiceAmount;
        list[k].loadingPoint = fromLocation;
        list[k].destination = toLocation;
      }
      setvehicleList(list);
      console.log(list);

      /*  const params = {
          "bookingId": currentID,
          "bookingPickupDate": pickupDate,
          "tripDetailsList": [
            {
              "tripId": "",
              "importOrExport": "",
              "customerId": "",
              "customerName": "",
              "customerAdvance": "",
              "billAmount": "",
              "locationId": "",
              "loadingPoint": "",
              "destination": "",
              "vehicleDriverList": {
                "vehicleNumber": "",
                "vehicleType": "",
                "vehicleCompany": "",
                "driverId": "",
                "driverName": "",
                "driverAdvance": "",
                "driverSalary": "",
                "driverPhoneNumber": "",
              }
            }
          ]
        }*/


      const params = {
        "bookingId": currentID,
        "bookingPickupDate": pickupDate,
        "tripDetailsList": list
      }

      Axios.post(UPDATE_BOOKING_DETAILS, params)
        .then(response => {
          console.log("validated Axios");

          console.log("response.data.validationStatus : " + response.data.validationStatus);
          console.log("response.data.validationMessage : " + response.data.validationMessage);
          if (response.data.validationStatus) {

            setDialogOpen(false);
            setErrorOpen(false);
            setinfoopen(false);
            setsuccessMsg("Booking Updated successfully!");
            setSuccessOpen(true);

            loadData();
            //setSuccessOpen(true);
            //	clearAll();
          } else {
            //setErrorOpen(true);
          }
        })
        .catch(error => {
          console.log("Error Axios");
          console.log(error);
          //  seterrorMsg(error.message);
          //	setErrorOpen(true);

        });




    }
  }
  function handleCloseBooking() {
    //console.log("update Booking : " + currentID);
    // var customerId = "";
    //var locationId = "";
    // var it = bookingDetails.find(item => {
    //  return item.bookingId === currentID
    //});
    if (handleValidation(vehicleList)) {
      //setinvoiceOpen(true);
      console.log(vehicleList);
      const params =
      {
        "bookingId": currentID,
        "tripDetailsList": vehicleList,
		"bookingClosedBy":localStorage.getItem("userName")
      }


      Axios.post(CLOSE_OPEN_AP_BOOKING, params)
        .then(response => {
          // console.log("validated Axios");
          //console.log(response.data);
          //console.log("response.data.validationStatus : " + response.data.validationStatus);
          //console.log("response.data.validationMessage : " + response.data.validationMessage);
          if (response.data.validationStatus) {
            //setSuccessOpen(true);
            //	clearAll();
            loadData();
            //printDocument();
          } else {
            //   console.log("Error Axios");
            // console.log(response.data.validationStatus);
            setinfoMessage("Not closed! Only 'Open Approved' bookings can be closed!");
            setinfoopen(true);
          }
        })
        .catch(error => {
          // console.log("Error Axios");
          // console.log(error);
          //seterrorMsg(error.message);
          //setErrorOpen(true);

        });

    }
  }
  function handleValidation(vList) {
    console.log("vlist" + vList.length);
    setsuccessMsg('');
    var eflag = false;
    if (delivery === "") {
      setDeliveryError(true);
      eflag = true;
    }
    if (fromLocation === "") {
      setfromLocationError(true);
      eflag = true;
    }
    if (toLocation === "") {
      settoLocationError(true);
      eflag = true;
    }
    if (customer === "") {
      setcustomerError(true);
      eflag = true;
    }
    for (var i = 0; i < vList.length; i++) {
      if (vList[i].vehicleOwnedBy === "OWN_VEHICLE") {
        if (vList[i].vehicleNumber === "" || vList[i].vehicleNumber === null) {
          vList[i].ownVehicleIDError = true;
          eflag = true;
        }
        if (vList[i].driverName === "" || vList[i].driverName === null) {
          vList[i].driverError = true;
          eflag = true;
        }
        if (vList[i].driverAdvance === "" || vList[i].driverAdvance === null) {
          vList[i].driverAdvanceAmountError = true;
          eflag = true;
        }
        if (vList[i].tollCharges === "" || vList[i].tollCharges === null) {
          vList[i].tollChargesError = true;
          eflag = true;
        }
        if (vList[i].rtoCharges === "" || vList[i].rtoCharges === null) {
          vList[i].rtoChargesError = true;
          eflag = true;
        }
        if (vList[i].containerNumber === "" || vList[i].containerNumber === null) {
          vList[i].containerNumberError = true;
          eflag = true;
        }
      }
      else {
        if (vList[i].vehicleNumber === "" || vList[i].vehicleNumber === null) {
          vList[i].thirdPartyIDError = true;
          eflag = true;
        }
        if (vList[i].vehicleType === "" || vList[i].vehicleType === null) {
          vList[i].thirdPartyTypeError = true;
          eflag = true;
        }
        if (vList[i].vehicleCompany === "" || vList[i].vehicleCompany === null) {
          vList[i].thirdPartyCompanyError = true;
          eflag = true;
        }
        if (vList[i].containerNumber === "" || vList[i].containerNumber === null) {
          vList[i].containerNumberError = true;
          eflag = true;
        }
      }
    }
    if (eflag === false) {
      setDialogOpen(false);
      setErrorOpen(false);
      setinfoopen(false);
      return (true);
    }
    else {
      setErrorOpen(true);
      return (false);
    }
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    setErrorOpen(false);
    setinfoopen(false);
  };
  const handlepickupDate = (date) => {
    setpickupDate(date);
  };
  //const handledropDate = (date) => {
  // setdropDate(date);
  //};

  return (
    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <h2>Booking Details</h2>
        <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }}  >
          <ReactTable
            data={bookingDetails}
            columns={columns}
            defaultPageSize={10}
            filterable
            sorted={[
              {
                id: "bookingId",
                asc: true
              }
            ]}
            defaultFiltered={[
              {
                id: "status",
                value: "OPEN"
              }
            ]}
            defaultFilterMethod={(filter, row, column) => {
              if (column.id === "status" && String(row[filter.id]) != null)
                return (String(row[filter.id])).toLowerCase().endsWith((filter.value).toLowerCase())
              else
                return (String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())
            }}
            //onExpandedChange={(newExpanded, index, event) => this.handleRowExpanded(newExpanded, index, event)} 
            SubComponent={(v) =>
              bookingDetails[v.row._index]["tripDetails"] && (<ReactTable
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: '#dadada',
                  borderRadius: '8px',

                }}
                minRows={1}
                showPaginationBottom={false}
                data={bookingDetails[v.row._index]["tripDetails"]}
                columns={bookingDetails[v.row._index]["tripDetails"] ? innerColumns : [{ Header: "hello" }]} />)
            }
          //<div style={{ padding: '10px' }}>Hello {bookingDetails[v.row._index]["bookingId"]}</div>}
          />
          <div>

          </div>
          <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={handleApprove} >Approve</Button>&nbsp;&nbsp;&nbsp;
          <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={handleReject} >Reject</Button>
        </form>
        <Dialog
          open={dialogOpen}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleDialogClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dial0g-slide-description"
          maxWidth={"xl"}
          disableBackdropClick
        >
          <DialogTitle style={{ textAlign: "center" }} id="alert-dialog-slide-title">{dailogTitle}</DialogTitle>
          <DialogContent>
            <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }}  >
              <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    //disableToolbar
                    variant="inline"
                    format="dd/MM/yyyy"
                    margin="normal"
                    id="date-picker-inline"
                    label="Pickup Date"
                    value={pickupDate}
                    autoOk
                    disabled={!editMode}
                    onChange={handlepickupDate}
                    KeyboardButtonProps={{
                      'aria-label': 'change date',
                    }}
                  />
                </MuiPickersUtilsProvider>
                <FormControl className={classes.formControl} error={deliveryError}>
                  <InputLabel id="select company label">Delivery Type</InputLabel>
                  <Select
                    className="mdb-select"
                    labelId="select delivery"
                    id="select delivery"
                    label="Delivery"
                    value={delivery}
                    onChange={handledeliveryChange}
                    disabled={!editMode}
                  >
                    <MenuItem value={"EXPORT"}>EXPORT</MenuItem>
                    <MenuItem value={"IMPORT"}>IMPORT</MenuItem>
                  </Select>
                  <FormHelperText>{deliveryError ? "Select a delivery type" : ""}</FormHelperText>
                </FormControl>
                <Autocomplete
                  options={customerDetails}
                  getOptionLabel={option => option.customerName}
                  getOptionSelected={(option, value) => option.customerName === value}
                  key={resetcustomer}
                  id="customer"
                  clearOnEscape
                  value={{ customerName: customer }}
                  disabled={!editMode}
                  onChange={handleCustomerChange}
                  renderInput={(params) => <TextField {...params} label="Customer" margin="normal" error={customerError} helperText={customerError ? "Select a customer" : ""} />}
                />
                <Autocomplete
                  options={mylocationDetails ? mylocationDetails : locationDetails}
                  getOptionLabel={option => option.sourceLocation}
                  key={resetfromLocation}
                  id="fromLocation"
                  value={{ sourceLocation: fromLocation }}
                  clearOnEscape
                  onChange={handleFromLocationChange}
                  disabled={!editMode}
                  renderInput={(params) => <TextField {...params} label="From Location" margin="normal" error={fromLocationError} helperText={fromLocationError ? "Select a source location" : ""} />}
                />
                <Autocomplete
                  options={mylocationDetails ? mylocationDetails : locationDetails}
                  getOptionLabel={option => option.destinationLocation}
                  key={resettoLocation}
                  id="toLocation"
                  clearOnEscape
                  value={{ destinationLocation: toLocation }}
                  onChange={handleToLocationChange}
                  disabled={!editMode}
                  renderInput={(params) => <TextField {...params} label="To Location" margin="normal" error={toLocationError} helperText={toLocationError ? "Select a destination location" : ""} />}
                />



              </div>
              <br />
              {vehicleList.map((x, i) => {
                return (
                  <div >
                    <div align="left" style={{ marginLeft: "4em", display: "flex", flexWrap: "wrap" }} >
                      <RadioGroup aria-label="quiz" name="radioValue" value={x.vehicleOwnedBy} onChange={e => handleInputChange(e, i)} row>
                        <FormControlLabel value="OWN_VEHICLE" control={<Radio color="default" />} disabled={!editMode} label="Own" />
                        <FormControlLabel value="THIRD_PARTY" control={<Radio color="default" />} disabled={!editMode} label="Outside" />
                      </RadioGroup>
                    </div>

                    {x.vehicleOwnedBy === "OWN_VEHICLE" ? (
                      <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
                        <Autocomplete
                          options={vehicleDetails}
                          getOptionLabel={option => option.vehicleNumber}
                          getOptionSelected={(option, value) => option.vehicleNumber === value.vehicleNumber}
                          key={resetownVehicleID}
                          id="ownVehicleID"
                          name="ownVehicleID"
                          clearOnEscape
                          disabled={!editMode}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? { vehicleNumber: x.vehicleNumber } : ""}
                          onSelect={e => handleOwnVehicleIDChange(e, i)}
                          renderInput={(params) => <TextField {...params} style={{ width: 180 }} label="Vehicle ID" margin="normal" error={x.ownVehicleIDError} helperText={x.ownVehicleIDError ? "Select a Vehicle ID" : ""} />}
                        />
                        <TextField
                          id="ownVehicleType"
                          label="Type"
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.vehicleType : ""}
                          name="Type"
                          disabled={!editMode}
                          InputProps={{ readOnly: true, }}
                          style={{ width: 120 }}
                        />
                        <TextField
                          id="vehicleCompany"
                          label="Company"
                          name="vehicleCompany"
                          disabled={!editMode}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.vehicleCompany : ""}
                          InputProps={{ readOnly: true, }}
                        />
                        <Autocomplete
                          options={driverDetails}
                          getOptionLabel={option => option.firstName}
                          getOptionSelected={(option, value) => option.fistName === value.driverName}
                          key={resetDriver}
                          id="driver"
                          name="driver"
                          disabled={!editMode}
                          clearOnEscape
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? { firstName: x.driverName } : ""}
                          onSelect={e => handleDriverChange(e, i)}
                          renderInput={(params) => <TextField {...params} label="Driver" margin="normal" error={x.driverError} helperText={x.driverError ? "Select a driver" : ""} />}
                        />
                        <TextField
                          id="phoneNumber"
                          label="Phone Number"
                          name="phoneNumber"
                          disabled={!editMode}
                          defaultValue=""
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.driverPhoneNumber || "" : ""}
                          style={{ width: 120 }}
                          InputProps={{ readOnly: true, }}
                        />

                        <TextField
                          id="driverSalary"
                          label="Salary"
                          name="driverSalary"
                          disabled={!editMode}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.driverSalary || "" : ""}
                          style={{ width: 100 }}
                          InputProps={{ readOnly: true, }}
                        />
                        <TextField
                          id="driverAdvance"
                          label="Advance"
                          name="driverAdvance"
                          disabled={!editMode}
                          style={{ width: 100 }}
						  InputProps = {{ inputProps: { max: 10 , min: 0} }}
                          type="number" error={x.driverAdvanceAmountError} helperText={x.driverAdvanceAmountError ? "Enter a valid advance amount" : ""}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.driverAdvance || "" : ""}
                          onChange={e => handleInputChange(e, i)}
                        />
                        <TextField
                          id="Invoice Amount"
                          label="Invoice Amount"
                          value={x.customerInvoiceAmount}
                          name="invoiceAmount"
                          type="number"
                          InputProps={{ readOnly: true, }}
                          disabled={!editMode}
                          style={{ width: 100 }}
                        />
                        <TextField
                          id="containerNumber"
                          label="Container ID"
                          value={x.containerNumber || ""}
                          name="containerNumber"
                          type="number"
                          onChange={e => { handleInputChange(e, i); }}
                          error={x.containerNumberError} helperText={x.containerNumberError ? "Please enter the advance amount" : ""}
                          InputProps = {{ inputProps: { max: 10 , min: 0} }}
                          style={{ width: 150 }}
                        />
                        <TextField
                          id="haltingCharges"
                          label="Halt Charges"
                          name="haltingCharges"
                          style={{ width: 100 }}
						  InputProps = {{ inputProps: { max: 10 , min: 0} }}
                          type="number" error={x.haltingChargesError} helperText={x.haltingChargesError ? "Enter a valid halting charge amount" : ""}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.haltingCharges || "" : ""}
                          onChange={e => handleInputChange(e, i)}
                        />
                        <TextField
                          id="tollCharges"
                          label="Toll Charges"
                          name="tollCharges"
                          style={{ width: 100 }}
						  InputProps = {{ inputProps: { max: 10 , min: 0} }}
                          type="number" error={x.tollChargesError} helperText={x.tollChargesError ? "Enter a valid Toll amount" : ""}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.tollCharges || "" : ""}
                          onChange={e => handleInputChange(e, i)}
                        />
                        <TextField
                          id="rtoCharges"
                          label="RTO Charges"
                          name="rtoCharges"
                          style={{ width: 100 }}
						  InputProps = {{ inputProps: { max: 10 , min: 0} }}
                          type="number" error={x.rtoChargesError} helperText={x.rtoChargesError ? "Enter a valid RTO amount" : ""}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? x.rtoCharges : ""}
                          onChange={e => handleInputChange(e, i)}
                        />

                      </div>) : (

                      <div align="left" style={{ marginLeft: "4em", display: "flex", flexWrap: "wrap" }}>
                        <TextField
                          id="vehicleNumber"
                          label="Outside Vehicle Number"
                          disabled={!editMode}
                          name="vehicleNumber"
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? "" : x.vehicleNumber}
                          onChange={e => handleInputChange(e, i)}
                          InputProps={{ readOnly: false, }}
                          error={x.thirdPartyIDError} helperText={x.thirdPartyIDError ? "Enter a valid vehicle Number" : ""}
                        />
                        <TextField
                          id="vehicleType"
                          label="Type"
                          name="vehicleType"
                          disabled={!editMode}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? "" : x.vehicleType}
                          onChange={e => handleInputChange(e, i)}
                          InputProps={{ readOnly: false, }}
                          error={x.thirdPartyTypeError} helperText={x.thirdPartyTypeError ? "Enter a valid vehicle type" : ""}
                        />
                        <TextField
                          id="vehicleCompany"
                          label="Company"
                          name="vehicleCompany"
                          disabled={!editMode}
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? "" : x.vehicleCompany}
                          onChange={e => handleInputChange(e, i)}
                          InputProps={{ readOnly: false, }}
                          error={x.thirdPartyCompanyError} helperText={x.thirdPartyCompanyError ? "Enter a valid company value  " : ""}
                        />
                        <TextField
                          id="Invoice Amount"
                          label="Invoice Amount"
                          value={x.customerInvoiceAmount}
                          name="invoiceAmount"
                          type="number"
                          InputProps={{ readOnly: true, }}
                          disabled={!editMode}
                          style={{ width: 100 }}
                        />
                        <TextField
                          id="haltingCharges"
                          label="Halt Charges"
                          name="haltingCharges"
                          style={{ width: 100 }}
                          type="number"
                          value={x.vehicleOwnedBy === "OWN_VEHICLE" ? "" : x.haltingCharges}
                          onChange={e => handleInputChange(e, i)}
                        />
                        <TextField
                          id="containerNumber"
                          label="Container ID"
                          value={x.containerNumber}
                          name="containerNumber"
                          type="number"
                          onChange={e => { handleInputChange(e, i); }}
                          error={x.containerNumberError} helperText={x.containerNumberError ? "Please enter the advance amount" : ""}
                          InputProps={{ readOnly: false, }}
                          style={{ width: 150 }}
                        />
                      </div>)}
                    <br />
                    <div align="left" style={{ marginLeft: "4em", display: "flex", flexWrap: "wrap" }}>
                      {vehicleList.length - 1 === i && <Button variant="contained" color="primary" allign="center" style={{ height: 30, fontSize: "90%", opacity: "1" }} disabled={!editMode} onClick={handleAddClick}>Add</Button>}
                      <br /><br /> &nbsp;&nbsp;
                      {vehicleList.length !== 1 && <Button variant="contained" color="primary" allign="center" style={{ height: 30, fontSize: "90%", opacity: "1" }} disabled={!editMode} onClick={() => handleRemoveClick(i)}>Delete</Button>}
                      <br />
                    </div>
                  </div>
                );
              })}
              <br /><br />

            </form>
          </DialogContent>
          <DialogActions>
            {editMode ?
              <Button onClick={handleUpdateBooking} color="primary">
                Update
              </Button>
              :
              <Button onClick={handleCloseBooking} color="primary">
                Close
              </Button>
            }
            <Button onClick={handleDialogClose} color="primary">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
        <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="success">
            {successMsg}
          </Alert>
        </Snackbar>
        <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="error">
            Please fill all the mandatory fields.
          </Alert>
        </Snackbar>
        <Snackbar open={infoopen} autoHideDuration={7000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="info">
            {infoMessage}
          </Alert>
        </Snackbar>
      </div>
      <Dialog
        open={invoiceOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleInvoiceClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dial0g-slide-description"
        maxWidth={"xl"}
        disableBackdropClick
      >
        <DialogTitle id="alert-dialog-slide-title">{"INVOICE"}
          <DialogContent>
            <TableContainer id="invoice" component={Paper} style={{ backgroundColor: "transparent" }}>
              <Table id="invoiceTable" className={classes.table} aria-label="spanning table" style={{ width: 600, borderStyle: "solid" }}>
                <TableHead>
                  <TableRow>
                    <TableCell align="left" rowSpan={4} colSpan={2} style={{ color: "white", borderLeftColor: "black", borderLeftStyle: "solid", borderTopColor: "black", borderTopStyle: "solid", borderRightColor: "#C0C0C0", borderRightStyle: "groove", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      {customer},
                      <br />{custAddress}.
                      <br />{custGST}
                    </TableCell>
                    <TableCell align="left" style={{ color: "white", borderTopColor: "black", borderTopStyle: "solid", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      ID
                    </TableCell>
                    <TableCell align="left" colSpan={2} style={{ color: "white", borderTopColor: "black", borderRightColor: "black", borderTopStyle: "solid", borderRightStyle: "solid", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      : {currentID}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ color: "white", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      Date
                    </TableCell>
                    <TableCell align="left" colSpan={2} style={{ color: "white", borderRightColor: "black", borderRightStyle: "solid", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      : {moment().format("DD/MM/YYYY")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ color: "white", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      Pan No
                    </TableCell>
                    <TableCell align="left" colSpan={2} style={{ color: "white", borderRightColor: "black", borderRightStyle: "solid", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      : ABCD1452E
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ color: "white", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      GST
                    </TableCell>
                    <TableCell align="left" colSpan={2} style={{ color: "white", borderRightColor: "black", borderRightStyle: "solid", borderBottomStyle: "groove", backgroundColor: "#3366CC" }}>
                      : IEG5843648HEI1
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow style={{ borderStyle: "" }}>
                    <TableCell align="center" colSpan={2} style={{ borderLeftStyle: "solid", borderBottomStyle: "groove", borderBottomColor: "#C0C0C0", borderRightColor: "#C0C0C0", borderRightStyle: "groove" }}>
                      DESCRIPTION
                    </TableCell>
                    <TableCell align="center" colSpan={3} style={{ borderRightStyle: "solid", borderBottomColor: "#C0C0C0", borderBottomStyle: "groove" }}>
                      AMOUNT
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ borderLeftStyle: "solid" }}>
                      Type
                    </TableCell>
                    <TableCell align="left" style={{ borderRightStyle: "groove", borderRightColor: "#C0C0C0" }}>
                      : {delivery}
                    </TableCell>
                    <TableCell align="center" rowSpan={7} colSpan={3} style={{ borderRightStyle: "solid", }}>
                      15000.00
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ borderLeftStyle: "solid" }}>
                      From
                    </TableCell>
                    <TableCell align="left" style={{ borderRightStyle: "groove", borderRightColor: "#C0C0C0" }}>
                      : {fromLocation}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ borderLeftStyle: "solid" }}>
                      To
                    </TableCell>
                    <TableCell align="left" style={{ borderRightStyle: "groove", borderRightColor: "#C0C0C0" }}>
                      : {toLocation}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ borderLeftStyle: "solid" }}>
                      Pickup Date
                    </TableCell>
                    <TableCell align="left" style={{ borderRightStyle: "groove", borderRightColor: "#C0C0C0" }}>
                      :
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ borderLeftStyle: "solid" }}>
                      Drop Date
                    </TableCell>
                    <TableCell align="left" style={{ borderRightStyle: "groove", borderRightColor: "#C0C0C0" }}>
                      : {dropDate.format("DD/MM/YYYY").toString()}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ borderLeftStyle: "solid" }}>
                      Container ID
                    </TableCell>
                    <TableCell align="left" style={{ borderRightStyle: "groove", borderRightColor: "#C0C0C0" }}>
                      : {containerID}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="left" style={{ borderLeftStyle: "solid" }}>
                      Vehicle No
                    </TableCell>
                    <TableCell align="left" style={{ borderRightStyle: "groove", borderRightColor: "#C0C0C0" }}>
                      : {vehicleNumber}
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ borderStyle: "solid", maxWidth: 200 }}>
                    <TableCell align="right" colspan={2} style={{ borderLeftStyle: "solid", borderTopStyle: "groove", borderBottomStyle: "groove", borderTopColor: "#C0C0C0", borderBottomColor: "#C0C0C0" }}>
                      Total :
                    </TableCell>
                    <TableCell align="center" colspan={3} style={{ maxWidth: "10px", whiteSpace: 'normal', wordBreak: 'break-word', borderRightStyle: "solid", borderBottomStyle: "groove", borderBottomColor: "#C0C0C0", borderTopStyle: "groove", borderTopColor: "#C0C0C0" }}>
                      15000.00<br />(INR {inWords(15000.00)})
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ borderStyle: "solid", maxWidth: 200 }}>
                    <TableCell align="center" colspan={2} style={{ borderLeftStyle: "solid", borderRightStyle: "groove", borderRightColor: "#C0C0C0", borderBottomStyle: "solid", borderBottomColor: "black" }}>
                      <br /><br /><br />
                      Authorised Signatory
                    </TableCell>
                    <TableCell align="center" colspan={3} style={{ maxWidth: "10px", whiteSpace: 'normal', wordBreak: 'break-word', borderRightStyle: "solid", borderBottomStyle: "solid", borderBottomColor: "black" }}>
                      FOR UNITED TRANSPORT
                      <br /><br /><br />
                      Authorised Signatory
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>

          </DialogContent>
          <DialogActions>

            <Button onClick={handleInvoiceClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </DialogTitle>
      </Dialog>
      <Dialog
        open={deleteOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleDeleteClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dial0g-slide-description"
        maxWidth={"xl"}
        disableBackdropClick>
        <DialogTitle style={{ textAlign: "center" }} id="alert-dialog-slide-title">{"DELETE"}
          <DialogContent>
            All Trip and Salary details associated with : {currentID} will be deleted ?
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDeleteConfirm} color="primary">
              Yes
            </Button>
            <Button onClick={handleDeleteClose} color="primary">
              No
            </Button>
          </DialogActions>
        </DialogTitle>
      </Dialog>
    </div>
  );
}
export default App;