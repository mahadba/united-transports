
import React, { Component } from 'react';
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faLock} from "@fortawesome/free-solid-svg-icons";
import AccountBox from '@material-ui/icons/AccountBox';
import Lock from '@material-ui/icons/Lock'; 
import Axios from 'axios';
import * as myConstClass from './Constants.js';

class Home extends React.Component {
	
	
	constructor(props) {
        super(props)
        this.state = {
            userName: '',
			password: '',
			showErrorMessage: false,
			validation: []
            
        }

    }
	
	handleChange = (e) => {
       
		 this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleOnSubmit = (e) => {

        e.preventDefault();
	

 const params = {
       
                    "userName": this.state.userName,
                    "password": this.state.password
                }
            
        


		const url = myConstClass.LOGIN_URL;
		Axios.post(url, params)
				.then(response => {
				 
				  this.state.validation = response.data;
				  
				  if (this.state.validation  != null && this.state.validation.validationStatus) {
					    console.log("validated");
						localStorage.setItem('isLoggedIn', true);
						localStorage.setItem('accessType', this.state.validation.accessType);
						localStorage.setItem('userFirstName', this.state.validation.userFirstName);
						localStorage.setItem('userName', this.state.validation.userName);
						
					  this.props.history.push('/newbooking');
				  } else {
					  
					  console.log("Credentails not valid ");
					  this.setState({
                        showErrorMessage: true
                    })
				  }
				 
				 
				})
            .catch(error => {
                console.log("Error Axios");
                console.log(error);
               
            });

       
    };
   
    render() {
        var width = window.innerWidth/4;   
        
        if(window.innerWidth > 1200)
            var widthval = width + "px";
        else 
            var widthval = "";
       
        return (
            <div   className="homeClassName" >
                 
                <React.Fragment >
      <CssBaseline /> 
      <Container maxWidth="sm" >
        <Typography component="div" className="typoGraphyClass" style={{height: '100vh', marginLeft:widthval }}>
        <form onSubmit={this.handleOnSubmit} style={{alignSelf:"right", alignItems:"baseline",}}>
        <div word-wrap="break-word" className="unitedText">
		United Transport</div><br></br>
       
	   {this.state.showErrorMessage ?
                        <div><h1 style={{
                            color: 'red'
                        }}> Credentials are not Valid</h1>
                        </div>
                        : null}
                            <div className='form-group row'>
                        <TextField className='input' type='text' id="outlined-helperText" name='userName' onChange={this.handleChange}
                        label="&nbsp; User Name" style={{backgroundColor:"white", opacity :"0.5"}} InputProps={{style: {fontSize: "150%"},
                            startAdornment: (
                              <InputAdornment position="start">
                                <AccountBox />
                              </InputAdornment>
                            ),
                          }} />
                    </div>
                    <br/>
                    <div className='form-group row'>                                        
                    <TextField className='input' type='text' id="outlined-helperText" name='password' onChange={this.handleChange} type="password"
                        label="&nbsp; Password" style={{backgroundColor:"white", opacity :"0.5" , alignContent:"center"}} InputProps={{style: {fontSize: "150%"},
                        startAdornment: (
                            <InputAdornment position="start">
                              <Lock />
                            </InputAdornment>
                          ),
                          }}   />                   
                    </div>
                    <br/><div className='form-group row'>
                        <Button variant="contained" color="primary" type='submit' style={{fontSize:"150%", opacity:"1"}}>Sign in</Button>
                    </div>
                        </form>
                        </Typography>
      </Container>
    </React.Fragment>
              
            </div>
        )
    }
}

export default Home;