import React from 'react';
import SideBar from "./sidebar";
import "react-table-6/react-table.css";
import "./styles.css";

function App() {
    return (   
      
        <div id="App">
        <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} /> 
        <div id="page-wrap">
        <div style={{height:"150px", backgroundImage: "url(/highway24.jpg)", backgroundSize:"cover", color:"white", textAlign:"center", alignItems:"center", display:"flex", justifyContent:"center", fontSize:"200%"}}> UNITED TRANSPORT
        </div>
        <h2>New Booking</h2>
        </div>
        </div>
    );
}
export default App;