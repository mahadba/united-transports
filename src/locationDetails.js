import React, { useState ,useEffect }  from 'react';
import "./styles.css";
import SideBar from "./sidebar";
import "react-table-6/react-table.css";
import ReactTable from 'react-table-6';
import { makeStyles } from '@material-ui/core/styles';
import * as myConstClass from './Constants.js';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      '& .MuiTextField-root': {
        margin: theme.spacing(3),
        width: '25ch',
        
      },
    },
    formControl: {
      margin: theme.spacing(3),
      width: '25ch',
    },
    
  }));

function App() {
	let history = useHistory();
    const classes = useStyles(); 
	const API_URL = myConstClass.GET_LOCATION_DETAILS;
   const [locationDetails, setLocationDetails] = useState([]);
	
	 useEffect(() => {
    loadData();
  },[]);
  
  const loadData = async () => {
	  
	  
	    if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				  const response = await fetch(API_URL);
				const data = await response.json();
				
				
				setLocationDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
	  
	  
    
  }
  
    const columns = [
		{
          Header: 'Company Name',
          accessor: 'customerName',
          width:"30%",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)
        },
        {
          Header: 'Source Location',
          accessor: 'sourceLocation',
          width:"30%",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)
        },
         {
          Header: 'Destination Location',
          accessor: 'destinationLocation',
          width:"30%",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)
        }, 
        {          
          Header: 'Vehicle Type',
          accessor: "vehicleType",
          width:"20%",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)}, 
        {
          Header: "Invoice Amount", // Custom header components!
          accessor:"customerInvoiceAmount",
          width:"20%",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)  }, 
        {
          Header: "Driver Salary", // Custom header components!
          accessor:"driverSalary",
          width:"20%",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)  }
       
      ];
  return (
    <div id="App">
    <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />      
    <div id="page-wrap" style={{backgroundImage: "url(/hexa3.jpg)", backgroundSize:"cover", widht:"100%", height:"100%"}}>
    <div className="appHeaderClass"> UNITED TRANSPORT</div> 
    <h2>Location Details</h2>
    <form className={classes.root} noValidate autoComplete="new-password"  style={{ display: 'block'}}  >  
      <ReactTable      
      data={locationDetails}
      columns={columns}      
      defaultPageSize={10}
      minRows={1}
        filterable
        filterOptions={(options, state) => options}
        autoComplete="none"
        defaultFilterMethod={(filter, row) =>
            (String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())}
           
    />
    </form>
    </div>
    </div>
    
  );
}

export default App;



