import React, { useState, useEffect } from 'react';
import "react-table-6/react-table.css";
import "./styles.css";
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Snackbar from '@material-ui/core/Snackbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { vehicleDetails, driverDetails, thridPartyVehicleDetails } from './Constants';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment'
import * as myConstClass from './Constants.js';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(3),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(3),
    width: '25ch',
    minWidth: 120,
  },

}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function App() {

let history = useHistory();

  const classes = useStyles();
  const [age, setAge] = React.useState('');

  const [locationDetails, setLocationDetails] = useState([]);

  const [customerDetails, setCustomerDetails] = useState([]);
  const [sourceLocation, setSourceLocation] = useState([]);
  const [destinationLocation, setDestinationLocation] = useState([]);

  const [inputValue, setInputValue] = React.useState('');
  const [fromInputLocation, setFromInputLocation] = React.useState('');
  const [toInputLocation, setToInputLocation] = React.useState('');

  const [driverSalary, setDriverSalary] = React.useState('');
  const [invoiceAmount, setInvoiceAmount] = React.useState('');

  const [customerError, setcustomerError] = React.useState(false);
  const [customer, setcustomer] = React.useState("");
  const [resetcustomer, setresetcustomer] = React.useState("reset");
  const [customerId, setCustomerId] = React.useState('');

  const CUST_API_URL = myConstClass.GET_CUSTOMER_DETAILS;
  const API_URL = myConstClass.GET_LOCATION_DETAILS;
  const LOC_DET_BY_CUST = myConstClass.GET_LOC_DET_BY_CUST_ID

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
	  
	  
	   if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				  const response = await fetch(CUST_API_URL);
				const data = await response.json();


				setCustomerDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
	  
   
  }





  const loadLocData = async (newvalue) => {
    console.log("customerId : " + customerId);
    console.log("newvalue : " + newvalue);
    console.log("customerId : " + LOC_DET_BY_CUST + newvalue);
    const response = await fetch(LOC_DET_BY_CUST + newvalue);
    const data = await response.json();


    setLocationDetails(data);
    setSourceLocation(data.sourceLocation);
    setDestinationLocation(data.destinationLocation);
    console.log(data);
  }

  const handleCustomerSelect = (event, newvalue) => {
    console.log("sdfsfd : " + newvalue);
    setcustomer(newvalue);
    setcustomerError(false);
    setSourceLocation([]);
    setDestinationLocation([]);

    settype("");
    setSourceLocation([]);
    setDestinationLocation([]);
    setToInputLocation('');
    setFromInputLocation('');
    setInputValue('');
    setInvoiceAmount('');
    setDriverSalary('');

    setfromLocationError(false);
    settoLocationError(false);
    settypeError(false);
    setsalaryError(false);
    setInvoiceAmountError(false);
    setcustomerError(false);


    var vn = newvalue;
    var i = customerDetails.find(item => {
      return item.customerName === vn
    })
    console.log(i);
    if (i) {
      setCustomerId(i.customerId);
      console.log("i.customerId : " + i.customerId);

      loadLocData(i.customerId);

    }
  };
  const handleChange = (event) => {
    setAge(event.target.value);
  };
  const [fromLocation, setFromLocation] = React.useState("");
  const [resetFromLocation, setresetFromLocation] = React.useState("");
  const [fromLocationError, setfromLocationError] = React.useState(false);
  const [mylocationDetails, setmylocationDetails] = React.useState("");
  const [toLocation, setToLocation] = React.useState("");
  const [resetToLocation, setresetToLocation] = React.useState("");
  const [toLocationError, settoLocationError] = React.useState(false);
  const [type, settype] = React.useState("");
  const [typeError, settypeError] = React.useState(false);
  const [salary, setsalary] = React.useState("");
  const [salaryError, setsalaryError] = React.useState(false);

  const [invoiceAmountError, setInvoiceAmountError] = React.useState(false);
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const handleFromLocationSelect = (event, newvalue) => {
    console.log("fromLocation : " + newvalue);

    setFromLocation(newvalue);

  };
  const handleToLocationSelect = (event, newvalue) => {
    console.log("toLocation : " + newvalue);
    setToLocation(newvalue);

  };
  const handleTypeChange = (type, from, to, customer, customerId) => {
    console.log("type : " + type);
    console.log("from : " + from);
    console.log("to : " + to);
    console.log("customer : " + customer);
    console.log("customerId : " + customerId);

    const url = myConstClass.GET_SAL_DET_BY_CUST_ID;


    console.log("type : " + type);
    console.log("from : " + from);
    console.log("to : " + to);
    console.log("customer : " + customer);
    console.log("customerId : " + customerId);
    const params =
    {
      "vehicleType": type,
      "sourceLocation": from,
      "destinationLocation": to,
      "customerName": customer,
      "customerId": customerId


    }

    Axios.post(url, params)
      .then(response => {
        console.log("validated Axios");

        //setCustDtlValidation(response.data);
        console.log("response.data.validationStatus : " + response.data.driverSalary);
        console.log("response.data.validationMessage : " + response.data.customerInvoiceAmount);

        setDriverSalary(response.data.driverSalary);
        setInvoiceAmount(response.data.customerInvoiceAmount);


      })
      .catch(error => {
        console.log("Error Axios");
        console.log(error);

      });




  };


  const saveLocDetails = (type, fromInputLocation, toInputLocation, inputValue, customerId, invoiceAmount, driverSalary) => {

    const url = myConstClass.SAVE_lOC_DETAILS;


    console.log("type : " + type);
    console.log("from : " + fromInputLocation);
    console.log("to : " + toInputLocation);
    console.log("customer : " + inputValue);
    console.log("customerId : " + customerId);
    console.log("driverSalary : " + driverSalary);
    console.log("invoiceAmount : " + invoiceAmount);

    const params =
    {
      "vehicleType": type,
      "sourceLocation": fromInputLocation,
      "destinationLocation": toInputLocation,
      "customerName": inputValue,
      "customerId": customerId,
      "driverSalary": driverSalary,
      "customerInvoiceAmount": invoiceAmount

    }

    Axios.post(url, params)
      .then(response => {
        console.log("validated Axios");

        //setCustDtlValidation(response.data);
        console.log("response.data.validationStatus : " + response.data.validationStatus);
        console.log("response.data.validationMessage : " + response.data.validationMessage);


        if (response.data.validationStatus) {

          setErrorOpen(false);
          setSuccessOpen(true);


          settype("");
          setSourceLocation([]);
          setDestinationLocation([]);
          setToInputLocation('');
          setFromInputLocation('');
          setInputValue('');
          setInvoiceAmount('');
          setDriverSalary('');

          setfromLocationError(false);
          settoLocationError(false);
          settypeError(false);
          setsalaryError(false);
          setInvoiceAmountError(false);
          setcustomerError(false);

        } else {

          setErrorOpen(true);
          setSuccessOpen(false);

        }



      })
      .catch(error => {
        console.log("Error Axios");
        console.log(error);

      });



  };



  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    setErrorOpen(false);
  };

  const handleClick = (event) => {
    console.log("type : " + type);
    console.log("from : " + fromInputLocation);
    console.log("to : " + toInputLocation);
    console.log("customer : " + inputValue);
    console.log("customerId : " + customerId);
    console.log("driverSalary : " + driverSalary);
    console.log("invoiceAmount : " + invoiceAmount);
    console.log('ok')

    var eflag = false;
    if (fromInputLocation === "") {
      setfromLocationError(true);
      eflag = true;
    }
    if (toInputLocation === "") {
      settoLocationError(true);
      eflag = true;
    }
    if (type === "") {
      settypeError(true);
      eflag = true;
    }
    if (driverSalary === "") {
      setsalaryError(true);
      eflag = true;
    }

    if (invoiceAmount === "") {
      setInvoiceAmountError(true);
      eflag = true;
    }
    if (inputValue === "") {
      setcustomerError(true);
      eflag = true;
    }

    console.log("eflag : " + eflag);
    if (eflag) {
      setErrorOpen(true);
      setSuccessOpen(false);
    }
    else {
      saveLocDetails(type, fromInputLocation, toInputLocation, inputValue, customerId, invoiceAmount, driverSalary);
    }
  }


  return (

    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <div>
          <h2>Add/Edit Location Details</h2>
          <br /> <br />
          <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
            <div id="locationAddEditWrap" className="locationAddEditWrap">
              <FormControl className={classes.root} error={fromLocationError}>
                <Autocomplete
                  id="customer"
                  clearOnEscape
                  options={customerDetails.map((option) => option.customerName)}
                  key={resetcustomer}
                  inputValue={inputValue}
                  onInputChange={(event, newInputValue) => {
                    setInputValue(newInputValue);
                  }}
                  select={customer}
                  onChange={handleCustomerSelect}
                  renderInput={(params) => <TextField {...params} required label="Customer Company" margin="normal" error={customerError} helperText={customerError ? "Company name is required" : ""} />}
                />
              </FormControl>
              <FormControl className={classes.root} error={fromLocationError}>
                <Autocomplete
                  //options={locationDetails}
                  //getOptionLabel={option=>option.fromLocation}
                  options={sourceLocation.map((option) => option)}
                  id="fromLocation"
                  freeSolo
                  key={resetFromLocation}
                  inputValue={fromInputLocation}
                  onInputChange={(event, newInputValue) => {
                    console.log("source : " + newInputValue);
                    setFromInputLocation(newInputValue);
                  }}
                  select={fromLocation}
                  onChange={handleFromLocationSelect}
                  renderInput={(params) => <TextField {...params} label="Source Location" margin="normal" error={fromLocationError} helperText={fromLocationError ? "Please enter a Source location" : ""} />}
                />
              </FormControl>
              <FormControl className={classes.root} error={toLocationError}>
                <Autocomplete
                  //  options={mylocationDetails==""? locationDetails:mylocationDetails}                
                  //  getOptionLabel={destinationLocation.map((option)=>option )}
                  options={destinationLocation.map((option) => option)}
                  id="toLocation"
                  freeSolo
                  key={resetToLocation}
                  inputValue={toInputLocation}
                  onInputChange={(event, newInputValue) => {
                    console.log("dest ;" + newInputValue);
                    setToInputLocation(newInputValue);
                  }}
                  select={toLocation}
                  onChange={handleToLocationSelect}
                  renderInput={(params) => <TextField {...params} label="Destination Location" margin="normal" error={toLocationError} helperText={toLocationError ? "Please enter a Destination location" : ""} />}
                />
              </FormControl>


              <FormControl className={classes.formControl}>
                <InputLabel id="select company label">Vehicle Type</InputLabel>
                <Select
                  className="mdb-select"
                  labelId="select type"
                  id="select type"
                  label="Vehicle Type"
                  value={type}
                  //  onChange={handleTypeChange}          
                  onChange={(event, fromLocation, toLocation) => {
                    settype(event.target.value);
                    handleTypeChange(event.target.value, fromInputLocation, toInputLocation, inputValue, customerId);
                  }}
                >
                  <MenuItem value={"20"}>20'</MenuItem>
                  <MenuItem value={"40"}>40'</MenuItem>
                </Select>
                <FormHelperText>{typeError ? "Select a vehicle type" : ""}</FormHelperText>
              </FormControl>

              <TextField
                id="invoiceAmount"
                label="InvoiceAmount"
                value={invoiceAmount}
                onChange={(event) => {
                  console.log("driver Salary : " + event.target.value);
                  if(event.target.value>0)
                  setInvoiceAmount(event.target.value);
                }}
                error={invoiceAmountError} helperText={invoiceAmountError ? "Enter the Invoive Amount" : ""}
              />

              <TextField
                id="salary"
                label="Salary"
                value={driverSalary}
                onChange={(event, newInputValue) => {
                  console.log("driver Salary : " + event.target.value);
                  if(event.target.value>0)
                  setDriverSalary(event.target.value);
                }}
                error={salaryError} helperText={salaryError ? "Enter the Driver salary" : ""}
              />
            </div>
            <br /><br /><br />
            <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={handleClick}>Submit</Button>
            <br />
          </form>
          <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Details submitted successfully !
            </Alert>
          </Snackbar>
          <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              Please fill all the mandatory fields.
            </Alert>
          </Snackbar>
        </div>
      </div>
    </div>
  );
}
export default App;