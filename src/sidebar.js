import React from "react";
import { scaleRotate as Menu } from "react-burger-menu";
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import MenuBook from '@material-ui/icons/MenuBook';
import Block from '@material-ui/icons/Block';
import SupervisorAccount from '@material-ui/icons/SupervisorAccount';
import LocationOn from '@material-ui/icons/LocationOn'
import Contacts from '@material-ui/icons/Contacts';
import InsertDriveFile from '@material-ui/icons/InsertDriveFile';
import Edit from '@material-ui/icons/Edit';
import FormatListBulleted from '@material-ui/icons/FormatListBulleted';
import Person from '@material-ui/icons/Person';
import AssignmentInd from '@material-ui/icons/AssignmentInd';
import DirectionsBus from '@material-ui/icons/DirectionsBus';
import AccountCircle from '@material-ui/icons/AccountCircle';
import AttachMoney from '@material-ui/icons/AttachMoney';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import ExitToApp from '@material-ui/icons/ExitToApp';
import { useHistory } from 'react-router-dom';

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,

  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));
export default (props) => {
	
	let history = useHistory();

  const classes = useStyles();
  const [driverOpen, setdriverOpen] = React.useState(false);
  const [vehicleOpen, setvehicleOpen] = React.useState(false);
  const [bookingOpen, setbookingOpen] = React.useState(false);
  const [customerOpen, setcustomerOpen] = React.useState(false);
  const [locationOpen, setlocationOpen] = React.useState(false);
  const [thirdPartyOpen, setthirdPartyOpen] = React.useState(false);
  const handledriverClick = () => {
    setdriverOpen(!driverOpen);
    setvehicleOpen(false);
    setbookingOpen(false);
    setcustomerOpen(false);
    setlocationOpen(false);
    setthirdPartyOpen(false);
  };
  const handlevehicleClick = () => {
    setvehicleOpen(!vehicleOpen);
    setdriverOpen(false);
    setbookingOpen(false);
    setcustomerOpen(false);
    setlocationOpen(false);
    setthirdPartyOpen(false);
  };
  const handlebookingClick = () => {
    setbookingOpen(!bookingOpen);
    setdriverOpen(false);
    setvehicleOpen(false);
    setcustomerOpen(false);
    setlocationOpen(false);
    setthirdPartyOpen(false);

  };
  const handlecustomerClick = () => {
    setcustomerOpen(!customerOpen);
    setdriverOpen(false);
    setvehicleOpen(false);
    setbookingOpen(false);
    setlocationOpen(false);
    setthirdPartyOpen(false);
  };
  const handlelocationClick = () => {
    setlocationOpen(!locationOpen);
    setdriverOpen(false);
    setvehicleOpen(false);
    setbookingOpen(false);
    setcustomerOpen(false);
    setthirdPartyOpen(false);
  };
  const handleSignOutClick = () => {
   console.log("signout-");
	localStorage.setItem('isLoggedIn', false);
	localStorage.setItem('accessType', '');
	localStorage.setItem('userFirstName', '');
	localStorage.setItem('userName', '');
	history.push('/home');
  };
  const handlethirdPartyClick = () => {
    setthirdPartyOpen(!thirdPartyOpen);
    setlocationOpen(false);
    setdriverOpen(false);
    setvehicleOpen(false);
    setbookingOpen(false);
    setcustomerOpen(false);
  };
  return (
    // Pass on our props
    <Menu {...props} className="menu" style={{ verticalAlign: "top", alignContent: "start", marginTop: "1" }}>
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        style={{ verticalAlign: "top", alignContent: "start", marginTop: "-15em", alignItems: "flex-start", backgroundColor: "#DBF1FF" }}

        subheader={
          <ListSubheader component="div" id="nested-list-subheader" style={{ color: "black", fontSize: "130%" }}>
            <ListItemIcon style={{ verticalAlign: "bottom" }} >
              <AccountCircle style={{ fontSize: 35 }} />
            </ListItemIcon>
         Hi {localStorage.getItem("userFirstName") }
        </ListSubheader>
        }
        className={classes.root}
      >
        <br />
        <br />
        <ListItem button onClick={handlebookingClick}>
          <ListItemIcon>
            <MenuBook />
          </ListItemIcon>
          <ListItemText primary="Bookings" style={{ color: "black" }} />
          {bookingOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={bookingOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>

            <ListItemLink className={classes.nested} href="/newBooking">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>

              <ListItemText primary="New Booking" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/openBookings">
              <ListItemIcon>
                <FormatListBulleted />
              </ListItemIcon>
              <ListItemText primary="Booking Details" style={{ color: "black" }} />
            </ListItemLink>
          </List>
        </Collapse>
        <Divider />
		
		{localStorage.getItem("accessType") === 'admin' ?
        <ListItem button onClick={handledriverClick}>
          <ListItemIcon>
            <AssignmentInd />
          </ListItemIcon>
          <ListItemText primary="Driver" style={{ color: "black" }} />
          {driverOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>  : null }
		{localStorage.getItem("accessType") === 'admin' ?
        <Collapse in={driverOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemLink className={classes.nested} href="/driverDetails">
              <ListItemIcon>
                <FormatListBulleted />
              </ListItemIcon>

              <ListItemText primary="Driver Details" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/driverAddEdit">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>

              <ListItemText primary="Edit Details" style={{ color: "black" }} />
            </ListItemLink>
			<ListItemLink className={classes.nested} href="/driverAddOnly">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>

              <ListItemText primary="Add Details" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/paySalary">
              <ListItemIcon>
                <AttachMoney />
              </ListItemIcon>

              <ListItemText primary="Pay Salary" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/haltingCharges">
              <ListItemIcon>
                <Block />
              </ListItemIcon>
              <ListItemText primary="Halting Charges" style={{ color: "black" }} />
            </ListItemLink>
          </List>
        </Collapse>
		: null }
        <Divider />
		{localStorage.getItem("accessType") === 'admin' ?
        <ListItem button onClick={handlevehicleClick}>
          <ListItemIcon>
            <DirectionsBus />
          </ListItemIcon>
          <ListItemText primary="Vehicle" style={{ color: "black" }} />
          {vehicleOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
		: null }
		{localStorage.getItem("accessType") === 'admin' ?
        <Collapse in={vehicleOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>

            <ListItemLink className={classes.nested} href="/vehicleDetails">
              <ListItemIcon>
                <FormatListBulleted />
              </ListItemIcon>
              <ListItemText primary="Vehicle Details" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/vehicleAddEdit">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <ListItemText primary="Add/Edit Details" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/otherExpenses">
              <ListItemIcon>
                <AttachMoney />
              </ListItemIcon>
              <ListItemText primary="Other Vehicle Expenses" style={{ color: "black" }} />
            </ListItemLink>
          </List>
        </Collapse>
		: null }
        <Divider />
		{localStorage.getItem("accessType") === 'admin' ?
        <ListItem button onClick={handlecustomerClick}>
          <ListItemIcon>
            <Person />
          </ListItemIcon>
          <ListItemText primary="Customer" style={{ color: "black" }} />
          {customerOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
		: null }
		{localStorage.getItem("accessType") === 'admin' ?
        <Collapse in={customerOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemLink className={classes.nested} href="/customerDetails">
              <ListItemIcon>
                <FormatListBulleted />
              </ListItemIcon>
              <ListItemText primary="Customer Details" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/customerAddOnly">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>

              <ListItemText primary="Add Details" style={{ color: "black" }} />
            </ListItemLink>
			 <ListItemLink className={classes.nested} href="/customerAddEdit">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>

              <ListItemText primary="Edit Details" style={{ color: "black" }} />
            </ListItemLink>
          </List>
        </Collapse>
		: null }
        <Divider />
		{localStorage.getItem("accessType") === 'admin' ?
        <ListItem button onClick={handlelocationClick}>
          <ListItemIcon>
            <LocationOn />
          </ListItemIcon>
          <ListItemText primary="Location" style={{ color: "black" }} />
          {locationOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
		: null }
		{localStorage.getItem("accessType") === 'admin' ?
        <Collapse in={locationOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>

            <ListItemLink className={classes.nested} href="/locationDetails">
              <ListItemIcon>
                <FormatListBulleted />
              </ListItemIcon>

              <ListItemText primary="Location Details" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/locationAddEdit">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>

              <ListItemText primary="Add/Edit Details" style={{ color: "black" }} />
            </ListItemLink>
          </List>
        </Collapse>
		: null }
        <Divider />
		{localStorage.getItem("accessType") === 'admin' ?
        <ListItem button onClick={handlethirdPartyClick}>
          <ListItemIcon>
            <SupervisorAccount />
          </ListItemIcon>
          <ListItemText primary="Third Party Vehicle" style={{ color: "black" }} />
          {thirdPartyOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
		: null }
		{localStorage.getItem("accessType") === 'admin' ?
        <Collapse in={thirdPartyOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemLink className={classes.nested} href="/thirdPartyVehicleDetails">
              <ListItemIcon>
                <FormatListBulleted />
              </ListItemIcon>
              <ListItemText primary="Third Party Vehicle Details" style={{ color: "black" }} />
            </ListItemLink>
            <ListItemLink className={classes.nested} href="/thirdPartyVehicleAddEdit">
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <ListItemText primary="Add/Edit Details" style={{ color: "black" }} />
            </ListItemLink>
          </List>
        </Collapse>
		: null }
        <Divider />
		{localStorage.getItem("accessType") === 'admin' ?
        <ListItemLink href="/reports">
          <ListItemIcon>
            <InsertDriveFile />
          </ListItemIcon>
          <ListItemText primary="Reports" style={{ color: "black" }} />
        </ListItemLink>
		: null }
        <Divider />
        <ListItemLink href="/contacts">
          <ListItemIcon>
            <Contacts />
          </ListItemIcon>
          <ListItemText primary="Contacts" style={{ color: "black" }} />
          </ListItemLink>
        <Divider />
		{localStorage.getItem("accessType") === 'admin' ?
		<ListItemLink href="/appAccess">
          <ListItemIcon>
            <AssignmentInd />
          </ListItemIcon>
          <ListItemText primary="App Access" style={{ color: "black" }} />
          </ListItemLink>
		  : null }
        <Divider />
        <br />
        
		
		<ListItem button onClick={handleSignOutClick}>
          <ListItemIcon>
            <ExitToApp />
          </ListItemIcon>
          <ListItemText primary="Sign Out" style={{ color: "black" }} />
        </ListItem>
		
      </List>
    </Menu>
  );
};
