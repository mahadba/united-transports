import React , { useEffect } from 'react';
import "./styles.css";
import SideBar from "./sidebar";
import * as myConstClass from './Constants.js';
import "react-table-6/react-table.css";
import ReactTable from 'react-table-6';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      // margin: theme.spacing(3),
      // width: '25ch',

    },
  },
  formControl: {
    // margin: theme.spacing(3),
    // width: '25ch',
  },

}));

function App() {
	let history = useHistory();
	
  const [driverDetails, setDriverDetails] = React.useState([]);
  const API_URL = myConstClass.GET_DRIVER_DETAILS;
  const classes = useStyles();
  const columns = [
    {
      Header: 'Name',
      id: 'Name',
      //width:"10%",
      accessor: d => d.lastName == null? `${d.firstName}`: `${d.firstName}, ${d.lastName}`,
      Filter: ({ filter, onChange }) => (
        <input width="48"
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: 'Phone Number',
      accessor: 'phoneNumber',
      //width:'10%',
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: 'Alternate Number',
      accessor: "alternatePhoneNumber",
      //width:"10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      id: "Address",
      Header: "Address", // Custom header components!
      //width:'30%',
      accessor: d => d.driverAddress2 == null? `${d.driverAddress1}`: `${d.driverAddress1}, ${d.driverAddress2}`,
      Filter: ({ filter, onChange }) => (
        <input width="48"
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "License", // Custom header components!
      accessor: "driverLicense",
      //width:"10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Expiry", // Custom header components!
      accessor: "licenseExpDate",
      //width:"30%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Bank", // Custom header components!
      accessor: "driverBankDetails.bankName",
      //width:"30%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },

    {
      Header: "IFSC Code", // Custom header components!
      accessor: "driverBankDetails.ifscCode",
      //width:"30%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Account Number", // Custom header components!
      accessor: "driverBankDetails.accountNumber",
      //width:"30%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Status", // Custom header components!
      accessor: "driverStatus",
      //width:"10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    }
  ];
  useEffect(() => {
    loadData();
  },[]);
  
  const loadData = async () => {
	  
	  if("booking"=== localStorage.getItem('accessType')) {
		history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
	
				 const response = await fetch(API_URL);
				const data = await response.json();	
				setDriverDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
   
  }
  return (
    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <h2>Driver Details</h2>
        <form className={classes.root} noValidate autoComplete="new-password" style={{ display: 'block' }}  >
          <ReactTable
            data={driverDetails}
            columns={columns}
            defaultPageSize={10}
            filterable
            minRows={1}
            filterOptions={(options, state) => options}
            autoComplete="none"
            defaultFilterMethod={(filter, row) =>
              (String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())}
          />
        </form>
      </div>
    </div>

  );
}

export default App;



