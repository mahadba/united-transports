import React, { useState ,useEffect }  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Link from '@material-ui/core/Link';
import * as myConstClass from './Constants.js';
import Axios from 'axios';


import "./styles.css";
import SideBar from "./sidebar";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

export default function App() {
    const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const [deleteOpen, setDeleteOpen] = React.useState(false);
	const [errorFlag, setErrorFlag] = React.useState(false);
	const [editOpen, setEditOpen] = React.useState(false);
	
	const [contactType, setContactType] = React.useState('');
	const [contactTypeError, setContactTypeError] = React.useState('');
	
	const [contactName, setContactName] = React.useState('');
	const [contactNameError, setContactNameError] = React.useState('');
	
	const [contactNumber, setContactNumber] = React.useState('');
	const [contactNumberError, setContactNumberError] = React.useState('');
	
	const [contactComments, setContactComments] = React.useState('');
	const [contactId, setContactId] = React.useState('');
	
	const [deleteContactId, setDeleteContactId] = React.useState('');
	const [deleteContactError, setDeleteContactError] = React.useState(false);
	
	const [contactDetails, setContactDetails] = useState([]);
	
	const CONTACT_API_URL = myConstClass.GET_CONTACT_DETAILS;
	const CONTACT_SAVE = myConstClass.SAVE_CONTACT_DETAILS;
	const CONTACT_DELETE = myConstClass.DELETE_CONTACT_DETAILS;
	
	useEffect(() => {
		loadData();
	},[]);
  
	const loadData = async () => {
		const response = await fetch(CONTACT_API_URL);
		const data = await response.json();
	
		setContactDetails(data);
		console.log(data);
	}

	
  const handleClickEdit = (item) => () => {
	  
	   console.log(item);
	     console.log(item.contactType);
		 
		 setContactType(item.contactType);
		setContactName(item.contactName);
		setContactNumber(item.contactNumber);
		setContactComments(item.contactComments);
		setContactId(item.contactId);
		setOpen(true);
		
		setEditOpen(true);
	  
  };

	const handleClickOpen = () => {
		setContactTypeError(false);
		setContactNameError(false);
		setContactNumberError(false);
		setErrorFlag(false);
		
		setContactType('');
		setContactName('');
		setContactNumber('');
		setContactComments('');
		
		setOpen(true);
	};
	
	const handleDeleteOpen = (item) => () => {
		setDeleteContactId(item.contactId);
		setDeleteOpen(true);
		
	};
	
	
	const handleDelete = () => {
		console.log("deleteContactId : "+deleteContactId);
		
		 const params = 
		{
			"contactId": deleteContactId
        }
	  
	  Axios.post(CONTACT_DELETE, params)
            .then(response => {
				console.log("validated Axios");
				console.log("response.data.validationStatus : "+response.data.validationStatus);
				console.log("response.data.validationMessage : "+response.data.validationMessage);	
				console.log("custValidationMsg.validationStatus :"+response.data.validationStatus===true);
		
					if(response.data.validationStatus){
							setDeleteOpen(false);
							setContactTypeError(false);
							setContactNameError(false);
							setContactNumberError(false);
							setErrorFlag(false);
							setOpen(false);
							setDeleteContactError(false);
							loadData();
					} else {
							setDeleteOpen(true);
							setDeleteContactError(true);
							
					}
				})
            .catch(error => {
                console.log("Error Axios");
                console.log(error);
               
            });
		
		
		
		
		
	};
	
	const handleDeleteClose = () => {
		setDeleteContactId('');
		setDeleteOpen(false);
		
	};
	
	
	
	const handleClose = () => {
		setContactTypeError(false);
		setContactNameError(false);
		setContactNumberError(false);
		setErrorFlag(false);
		
		setContactType('');
		setContactName('');
		setContactNumber('');
		setContactComments('');
		
		setOpen(false);
		setEditOpen(false);
	};
	
	const saveData = async () => {
	 
	   const params = 
		{
			"contactType": contactType,
			"contactName": contactName,
			"contactNumber": contactNumber,
			"contactComments": contactComments    
        }
	  
	  Axios.post(CONTACT_SAVE, params)
            .then(response => {
				console.log("validated Axios");
				console.log("response.data.validationStatus : "+response.data.validationStatus);
				console.log("response.data.validationMessage : "+response.data.validationMessage);	
				console.log("custValidationMsg.validationStatus :"+response.data.validationStatus===true);
		
					if(response.data.validationStatus){
							setContactTypeError(false);
							setContactNameError(false);
							setContactNumberError(false);
							setErrorFlag(false);
							setOpen(false);
							
							loadData();
					} else {
							setOpen(true);
							setContactTypeError(true);
					}
				})
            .catch(error => {
                console.log("Error Axios");
                console.log(error);
               
            });
		}
  
	const handleUpdateContact = () => {
		
			console.log("contactId : "+contactId);
			console.log("contactType : "+contactType);
			console.log("contactName : "+contactName);
			console.log("contactNumber : "+contactNumber);
			console.log("contactComments : "+contactComments);
			
			
			
			const params = 
			{
				"contactId":contactId,
				"contactType": contactType,
				"contactName": contactName,
				"contactNumber": contactNumber,
				"contactComments": contactComments    
			}
	  
	  Axios.post(CONTACT_SAVE, params)
            .then(response => {
				console.log("validated Axios");
				console.log("response.data.validationStatus : "+response.data.validationStatus);
				console.log("response.data.validationMessage : "+response.data.validationMessage);	
				console.log("custValidationMsg.validationStatus :"+response.data.validationStatus===true);
		
					if(response.data.validationStatus){
							setContactTypeError(false);
							setContactNameError(false);
							setContactNumberError(false);
							setErrorFlag(false);
							setOpen(false);
							setEditOpen(false);
							
							loadData();
					} else {
							setOpen(true);
							setContactTypeError(true);
					}
				})
            .catch(error => {
                console.log("Error Axios");
                console.log(error);
               
            });
			
	};
	const handleAddContact = () => {
		
		 var eflag = false;
		 setContactTypeError(false);
		setContactNameError(false);
		setContactNumberError(false);
		setErrorFlag(false);
		
		if(contactType === ''){
			setContactTypeError(true);
			setErrorFlag(true);
			 eflag = true;
		} else if(contactName === ''){
			setContactNameError(true);
			setErrorFlag(true);
			 eflag = true;
		} else if(contactNumber === ''){
			setContactNumberError(true);
			setErrorFlag(true);
			 eflag = true;
		}
		
		console.log("errorFlag : "+errorFlag);
		console.log("eflag : "+eflag);
		
		if(!eflag){
			console.log("contactType : "+contactType);
			console.log("contactName : "+contactName);
			console.log("contactNumber : "+contactNumber);
			console.log("contactComments : "+contactComments);
		
			saveData();
		}
	};
  
  
    const bull = <span className={classes.bullet}>•</span>;

    return (
        <div id="App">
            <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
            <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
                <div className="appHeaderClass"> UNITED TRANSPORT</div>
                <div>
                    <h2>Contact Details</h2>
                    <br />
					{localStorage.getItem("accessType") === 'admin' ?
					<Button variant="outlined" color="primary" style={{marginRight:20 , marginBottom:20 }} onClick={handleClickOpen}>
						Add Contact
					</Button> : null }
					
					 <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
						<DialogTitle id="form-dialog-title">Contact Details</DialogTitle>
							<DialogContent>
							{ editOpen ?
							
							<DialogContentText>
									Edit below details
								</DialogContentText>
							
							:
								<DialogContentText>
									Add below details to add a new contact.
								</DialogContentText>
							}
								<TextField style={{marginRight:20 , marginBottom:20 }} autoFocus margin="dense" id="contactType" label="Contact Type" 
									value={contactType} error={contactTypeError} helperText={contactTypeError ? "Please Enter a calid Contact Type":""}
									onChange={(event,newInputValue) => {
											setContactType(event.target.value);
										}}	
										/>
								
								<TextField style={{marginRight:20 , marginBottom:20 }}  margin="dense" id="contactName" label="Contact Name"  
									value={contactName} error={contactNameError} helperText={contactNameError ? "Please Enter a calid Contact Name":""}
									onChange={(event,newInputValue) => {
											setContactName(event.target.value);
										}}	
										/>
								<TextField style={{marginRight:20 , marginBottom:20 }}  margin="dense" id="contactNumber" label="Contact Number"  
								value={contactNumber} error={contactNumberError} helperText={contactNumberError ? "Please Enter a calid Contact Number":""}
									onChange={(event,newInputValue) => {
											setContactNumber(event.target.value);
										}}	
								/>
								<TextField  style={{marginRight:20 , marginBottom:20 }}  margin="dense" id="contactComments" label="Contact comments" 
								value={contactComments}
									onChange={(event,newInputValue) => {
											setContactComments(event.target.value);
										}}	
								/>
								
								
								</DialogContent>
								<DialogActions>
								  <Button onClick={handleClose} color="primary">
									Cancel
								  </Button>
								  { editOpen ?
								  <Button onClick={handleUpdateContact} color="primary">
									Update
								  </Button>
								  :
								  <Button onClick={handleAddContact} color="primary">
									Add
								  </Button>
								  }
								</DialogActions>
							  </Dialog>
							  
							  <Dialog open={deleteOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
						<DialogTitle id="form-dialog-title">Contact Details</DialogTitle>
							<DialogContent>			
								{ deleteContactError ?
									<DialogContentText>
										Unable to delete please try after some time.															
									</DialogContentText>
								:
									<DialogContentText>
																									
									</DialogContentText>
								}
									<DialogContentText>
										Add you sure you want to Delete ?																
									</DialogContentText>
							</DialogContent>
								<DialogActions>
								  <Button onClick={handleDelete} color="primary">
									YES
								  </Button>
								  <Button onClick={handleDeleteClose} color="primary">
									NO
								  </Button>
								</DialogActions>
							  </Dialog>
							  
							  
                    <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
                        <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
						
						{contactDetails.map((item) =>
    
	
	
						<Card  key={item.contactId} data-div_id={item.contactId}className={classes.root} style={{marginRight:20 , marginBottom:20 }}>
                                <CardContent>
                                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                                       
										{item.contactType}
										
                                    </Typography>
									
                                    <Typography variant="h5" component="h2">
                                     
										{item.contactName}
                                     </Typography>
                                    <Typography className={classes.pos} color="textSecondary">
                                   
									{item.contactNumber}
                                    </Typography>
                                    <Typography variant="body2" component="p">
                                   
									{item.contactComments}
                                    </Typography>
									{localStorage.getItem("accessType") === 'admin' ?
									<Typography variant="body2" component="p">
                                    <Button variant="outlined" color="primary" style={{marginRight:20 , marginTop:20 }} onClick={handleClickEdit(item)}>
										EDIT
									</Button>
									<Button variant="outlined" color="primary" style={{marginRight:20 , marginTop:20 }} onClick={handleDeleteOpen(item)}>
										DELETE
									</Button>
                                    </Typography> : null }
									
                                </CardContent>
                            </Card>
						)}
						
                        </div>
                    </form>
                </div>

            </div>
        </div>
    );
}