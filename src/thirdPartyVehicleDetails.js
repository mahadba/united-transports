
import "./styles.css";
import SideBar from "./sidebar";
import React, { useState, useEffect } from 'react';
import "react-table-6/react-table.css";
import * as myConstClass from './Constants.js';
import ReactTable from 'react-table-6';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      '& .MuiTextField-root': {
        margin: theme.spacing(3),
        width: '25ch',
        
      },
    },
    formControl: {
      margin: theme.spacing(3),
      width: '25ch',
    },
    
  }));

function App() {
	
	let history = useHistory();
	
	 const [bookingDetails, setbookingDetails] = React.useState();
	 const API_URL = myConstClass.THIRD_PARTY_BOOKED_DETAILS;
	 
	 
    const classes = useStyles(); 
	 useEffect(() => {


    loadData();
  }, []);
  
  const loadData = async () => {
	  
	  
	  console.log("---------"+localStorage.getItem('accessType')+"-------------------")
	  
	  if("booking"=== localStorage.getItem('accessType')) {
		history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
	
				var response = [];
				response = await fetch(API_URL);
				  
				const data = await response.json();
				setbookingDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
	
  }
  
    const columns = [
        {
          Header: 'Vehicle Number',
          accessor: 'vehicleNumber',
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)
        },
         {
          Header: 'IMPORT/ EXPORT',
          accessor: 'importOrExport',
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)
        }, 
        {          
          Header: 'Company',
          accessor: "vehicleCompany",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)}, 
        {
          Header: "Loading Point", // Custom header components!
          accessor:"loadingPoint",
          Filter: ({filter, onChange}) => (
            <input
              onChange={event => onChange(event.target.value)}
              autoComplete ="none"
              value={filter ? filter.value : ''}/>)  },
        {
          Header: "Destination", // Custom header components!
          accessor:"destination",
          Filter: ({filter, onChange}) => (
          <input
            onChange={event => onChange(event.target.value)}
            autoComplete ="none"
            value={filter ? filter.value : ''}/>)  },
        {
          Header: "Invoice Amount", // Custom header components!
          accessor:"customerInvoiceAmount",
          Filter: ({filter, onChange}) => (
          <input
            onChange={event => onChange(event.target.value)}
            autoComplete ="none"
            value={filter ? filter.value : ''}/>)  }
       
      ];
  return (
    <div id="App">
    <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />      
    <div id="page-wrap" style={{backgroundImage: "url(/hexa3.jpg)", backgroundSize:"cover", widht:"100%", height:"100%"}}>
    <div className="appHeaderClass"> UNITED TRANSPORT</div> 
    <h2>Third Party Vehicle Details</h2>
    <form className={classes.root} noValidate autoComplete="new-password"  style={{ display: 'block'}}  >  
      <ReactTable      
      data={bookingDetails}
      columns={columns}      
      defaultPageSize={10}
        filterable
        filterOptions={(options, state) => options}
        autoComplete="none"
        defaultFilterMethod={(filter, row) =>
            (String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())}
           
    />
    </form>
    </div>
    </div>
    
  );
}

export default App;



