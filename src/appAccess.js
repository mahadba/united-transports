import React, { useState ,useEffect }  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Link from '@material-ui/core/Link';
import * as myConstClass from './Constants.js';
import Axios from 'axios';
import "react-table-6/react-table.css";
import ReactTable from 'react-table-6';
import validator from 'validator';
import "./styles.css";
import SideBar from "./sidebar";
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
	formControl: {
    //  margin: theme.spacing(3),
		marginRight:200, 
      width: '25ch',
	  minWidth: 120,
    },
});

export default function App() {
	  let history = useHistory();
    const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const [deleteOpen, setDeleteOpen] = React.useState(false);
	const [errorFlag, setErrorFlag] = React.useState(false);
	const [editOpen, setEditOpen] = React.useState(false);
	
	const [contactType, setContactType] = React.useState('');
	const [contactTypeError, setContactTypeError] = React.useState('');
	
	const [contactName, setContactName] = React.useState('');
	const [contactNameError, setContactNameError] = React.useState('');
	
	const [contactNumber, setContactNumber] = React.useState('');
	const [contactNumberError, setContactNumberError] = React.useState('');
	
	const [contactComments, setContactComments] = React.useState('');
	const [contactId, setContactId] = React.useState('');
	
	const [deleteAccessId, setDeleteAccessId] = React.useState('');
	const [deleteAccessError, setDeleteAccessError] = React.useState(false);
	
	const [validationError, setValidationError] = React.useState(false);
	const [validationMessage, setValidationMessage] = React.useState('');
	
	const [contactDetails, setContactDetails] = useState([]);
	
	const [editAccessId, setEditAccessId] = React.useState('');
	const [accessType, setAccessType] = React.useState('');
	const [approvalRequired, setApprovalRequired] = React.useState('');
	const [firstName, setFirstName] = React.useState('');
	const [lastName, setLastName] = React.useState('');
	const [userName, setUserName] = React.useState('');
	const [newPassword, setNewPassword] = React.useState('');
	const [confirmPassword, setConfirmPassword] = React.useState('');
	
	const [confirmPasswordError, setConfirmPasswordError] = React.useState(false);
	const [accessTypeError, setAccessTypeError] = React.useState(false);
	const [firstNameError, setFirstNameError] = React.useState(false);
	const [lastNameError, setLastNameError] = React.useState(false);
	const [userNameError, setUserNameError] = React.useState(false);
	const [newPasswordError, setNewPasswordError] = React.useState(false);
	const [approvalRequiredError, setApprovalRequiredError] = React.useState(false);
	
	
	const GET_ACCESS_DETAILS = myConstClass.GET_ACCESS_DETAILS;
	const SAVE_ACCESS_DETAILS = myConstClass.SAVE_ACCESS_DETAILS;
	const UPDATE_ACCESS_DETAILS = myConstClass.UPDATE_ACCESS_DETAILS;
	const DELETE_ACCESS_DETAILS = myConstClass.DELETE_ACCESS_DETAILS;
	
	 const [accessDetails, setAccessDetails] = React.useState([]);

	
	
	
	
	useEffect(() => {
		loadData();
	},[]);
  
	const loadData = async () => {
		
		   if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
			
				const response = await fetch(GET_ACCESS_DETAILS);
				const data = await response.json();
			
				setAccessDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
	  
	  
	  
	  
	}

	
  const handleClickEdit = (item) => () => {
	  
	   console.log(item);
	     console.log(item.contactType);
		 
		 setContactType(item.contactType);
		setContactName(item.contactName);
		setContactNumber(item.contactNumber);
		setContactComments(item.contactComments);
		setContactId(item.contactId);
		setOpen(true);
		
		setEditOpen(true);
	  
  };
  
  
  
  const handleEditOpen = (item) => {
		setContactTypeError(false);
		setContactNameError(false);
		setContactNumberError(false);
		setErrorFlag(false);
		setConfirmPasswordError(false);
		
		console.log("name  : "+item.accessPassword);
		
		setFirstName(item.firstName);
		setLastName(item.lastName);
		setAccessType(item.accessType);
		setUserName(item.userName);
		setNewPassword(item.accessPassword);
		setConfirmPassword(item.accessPassword);
		setApprovalRequired(item.approvalRequired);
		setEditAccessId(item.accessId);
		
		
				
			
		
		setOpen(true);
		setEditOpen(true);
  };

	const handleClickOpen = () => {
		setContactTypeError(false);
		setContactNameError(false);
		setContactNumberError(false);
		setErrorFlag(false);
		setConfirmPasswordError(false);
		
		
		
		setContactType('');
		setContactName('');
		setContactNumber('');
		setContactComments('');
		
		
		setAccessType('');
		setFirstName('');
		setLastName('');
		setUserName('');
		setNewPassword('');
		setConfirmPassword('');
		setApprovalRequired('');
		
		
		
		
		setOpen(true);
	};
	
	const handleDeleteOpen = (item) => {
		setDeleteAccessId(item.accessId);
	
		console.log("handle delete open is calles");
		setDeleteOpen(true);
		
	};
	
	
	const handleDelete = () => {
		console.log("deleteAccessId : "+deleteAccessId);
		
		 const params = 
		{
			"accessId": deleteAccessId
        }		
		
		 Axios.post(DELETE_ACCESS_DETAILS, params)
					.then(response => {
						console.log("validated Axios");
						console.log("response.data.validationStatus : "+response.data.validationStatus);
						console.log("response.data.validationMessage : "+response.data.validationMessage);	
						console.log("custValidationMsg.validationStatus :"+response.data.validationStatus===true);
				
							if(response.data.validationStatus){								
									setDeleteOpen(false);
									loadData();
							} else {
									setDeleteOpen(true);
									setDeleteAccessError(true);
							}
						})
					.catch(error => {
						console.log("Error Axios");
						console.log(error);
					   
					});
		
	};
	
	const handleEdit = () => {
		console.log("editContactId : "+deleteAccessId);
		
		 const params = 
		{
			"contactId": deleteAccessId
        }		
	};
	
	const handleDeleteClose = () => {
		setDeleteAccessId('');
		setDeleteOpen(false);
		
	};
	
	
	
	const handleClose = () => {
		setContactTypeError(false);
		setContactNameError(false);
		setContactNumberError(false);
		setErrorFlag(false);
		
		setContactType('');
		setContactName('');
		setContactNumber('');
		setContactComments('');
		
		setOpen(false);
		setEditOpen(false);
		
		setValidationError(false);
		setValidationMessage('');
	};
	
	const saveData = async () => {
	 
	   const params = 
		{
			"contactType": contactType,
			"contactName": contactName,
			"contactNumber": contactNumber,
			"contactComments": contactComments    
        }
	  
	
		}
  
	const handleUpdateUser= () => {
		
			console.log("accessType : "+accessType);
			console.log("firstName : "+firstName);
			console.log("lastName : "+lastName);
			console.log("userName : "+userName);
			console.log("newPassword : "+newPassword);
			console.log("confirmPassword : "+confirmPassword);
			console.log("approvalRequired : "+approvalRequired);
			console.log("accessId : "+editAccessId);
			
			
			setAccessTypeError(false);
		setFirstNameError(false);
		setLastNameError(false);
		setUserNameError(false);
		setNewPasswordError(false);
		setConfirmPasswordError(false);
		setApprovalRequiredError(false);
			
			let validCase = true;
			
		if(accessType === ''){
			setAccessTypeError(true);
			validCase = false;
		}

		if(firstName === ''){
			setFirstNameError(true);
				validCase = false;
			}
			
			if(lastName === ''){
						setLastNameError(true);
							validCase = false;
			}
			
		if(userName === ''){
			setUserNameError(true);
				validCase = false;
			}
			
		if(newPassword === ''){
			setNewPasswordError(true);
				validCase = false;
			}
			
		if(confirmPassword === ''){
			setConfirmPasswordError(true);
				validCase = false;
			}
			
		if((accessType === 'admin' && approvalRequired === '')||(accessType === 'admin' && !approvalRequired)){
			setApprovalRequiredError(true);
				validCase = false;
			}	
			
			if(newPassword != confirmPassword){
			setConfirmPasswordError(true);
				validCase = false;
			}
			
			
			
			
	
	console.log(validCase);
			
		if(validCase){
			
			const params = 
			{
				"accessId": editAccessId,
				"accessType":accessType,
				"firstName": firstName,
				"lastName": lastName,
				"userName": userName,
				"accessPassword": newPassword,
				"approvalRequired": approvalRequired
			}
	  
	  
	  Axios.post(UPDATE_ACCESS_DETAILS, params)
					.then(response => {
						console.log("validated Axios");
						console.log("response.data.validationStatus : "+response.data.validationStatus);
						console.log("response.data.validationMessage : "+response.data.validationMessage);	
						console.log("custValidationMsg.validationStatus :"+response.data.validationStatus===true);
				
							if(response.data.validationStatus){								
									
									setOpen(false);
									setEditOpen(false);
									loadData();
									
									
									setAccessType('');
									setFirstName('');
									setLastName('');
									setUserName('');
									setNewPassword('');
									setConfirmPassword('');
									setApprovalRequired('');
									
									
									setAccessTypeError(false);
									setFirstNameError(false);
									setLastNameError(false);
									setUserNameError(false);
									setNewPasswordError(false);
									setConfirmPasswordError(false);
									setApprovalRequiredError(false);
									
									setValidationError(false);
									setValidationMessage('');
											
							} else {
									
									setOpen(true);
									setEditOpen(true);
									setValidationError(true);
									setValidationMessage(response.data.validationMessage);
							}
						})
					.catch(error => {
						console.log("Error Axios");
						console.log(error);
					   
					});
			  
					
		
		}
		
		
		
	  
	  
			
	};
	const handleAddUser = () => {
		
		
		
		 
			console.log("accessType : "+accessType);
			console.log("firstName : "+firstName);
			console.log("lastName : "+lastName);
			console.log("userName : "+userName);
			console.log("newPassword : "+newPassword);
			console.log("confirmPassword : "+confirmPassword);
			console.log("approvalRequired : "+approvalRequired);
			
			setAccessTypeError(false);
		setFirstNameError(false);
		setLastNameError(false);
		setUserNameError(false);
		setNewPasswordError(false);
		setConfirmPasswordError(false);
		setApprovalRequiredError(false);
			
			let validCase = true;
			
		if(accessType === ''){
			setAccessTypeError(true);
			validCase = false;
		}

		if(firstName === ''){
			setFirstNameError(true);
				validCase = false;
			}
			
			if(lastName === ''){
						setLastNameError(true);
							validCase = false;
			}
			
		if(userName === ''){
			setUserNameError(true);
				validCase = false;
			}
			
		if(newPassword === ''){
			setNewPasswordError(true);
				validCase = false;
			}
			
		if(confirmPassword === ''){
			setConfirmPasswordError(true);
				validCase = false;
			}
			
		if(accessType === 'admin' && approvalRequired === ''){
			setApprovalRequiredError(true);
				validCase = false;
			}	
			
			if(newPassword != confirmPassword){
			setConfirmPasswordError(true);
				validCase = false;
			}
			
			
			
			
	
	console.log(validCase);
			
		if(validCase){
			
			const params = 
			{
				"accessType":accessType,
				"firstName": firstName,
				"lastName": lastName,
				"userName": userName,
				"accessPassword": newPassword,
				"approvalRequired": approvalRequired
			}
	  
			  Axios.post(SAVE_ACCESS_DETAILS, params)
					.then(response => {
						console.log("validated Axios");
						console.log("response.data.validationStatus : "+response.data.validationStatus);
						console.log("response.data.validationMessage : "+response.data.validationMessage);	
						console.log("custValidationMsg.validationStatus :"+response.data.validationStatus===true);
				
							if(response.data.validationStatus){
								//	setContactTypeError(false);
								//	setContactNameError(false);
								//	setContactNumberError(false);
								//	setErrorFlag(false);
									setOpen(false);
							//		setEditOpen(false);
									
									loadData();
									
									setAccessType('');
									setFirstName('');
									setLastName('');
									setUserName('');
									setNewPassword('');
									setConfirmPassword('');
									setApprovalRequired('');
									
									
									setAccessTypeError(false);
									setFirstNameError(false);
									setLastNameError(false);
									setUserNameError(false);
									setNewPasswordError(false);
									setConfirmPasswordError(false);
									setApprovalRequiredError(false);
									
									setValidationError(false);
									setValidationMessage('');
									
												
							} else {
									setOpen(true);
									setValidationError(true);
									setValidationMessage(response.data.validationMessage);
							}
						})
					.catch(error => {
						console.log("Error Axios");
						console.log(error);
					   
					});
					
					
		}
		
		
		
			
			
	};
  
  
    const bull = <span className={classes.bullet}>•</span>;
	
	
	
	const editButton = (<button style={{  borderRadius: "5px", padding: "5px",  }}   >EDIT</button>);
	
	
	
	
	
	const columns = [
		{
          Header: 'First Name',
          accessor: 'firstName',
          width:"30%"
         
        },
		{
          Header: 'User Name',
          accessor: 'userName',
          width:"30%"
         
        },
        {
          Header: 'Access Type',
          accessor: 'accessType',
          width:"10%"
          
        },
         {
          Header: 'Action',
         
          width:"30%",
        Cell: row => (
           <div>
               <Button variant="outlined" color="primary"  onClick={() => handleEditOpen(row.original)}>Edit</Button>   
			   /     
               <Button variant="outlined" color="primary"  onClick={() => handleDeleteOpen(row.original)}>Delete</Button>
			   
			 
           </div>
       )
        }
       
      ];

    return (
        <div id="App">
            <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
            <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
                <div className="appHeaderClass"> UNITED TRANSPORT</div>
                <div>
                    <h2>Application Access</h2>
                    <br />
					<Button variant="outlined" color="primary" style={{marginRight:20 , marginBottom:20 }} onClick={handleClickOpen}>
						Add USER
					</Button>
					
					 <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
						<DialogTitle id="form-dialog-title">User Details</DialogTitle>
							<DialogContent>
							{ editOpen ?
							
							<DialogContentText>
									Edit below details
								</DialogContentText>
							
							:
								<DialogContentText>
									Add below details to provide access to a user.
								</DialogContentText>
							}
								<FormControl className={classes.formControl}>
									  <InputLabel id="select User Access Type" style={{marginRight:20 , marginBottom:20 }} >Access Type</InputLabel>
										<Select className="mdb-select" labelId="select type" id="select type" label="Access Type" value={accessType}           
									         
										onChange={(event,newInputValue) => {
											setAccessType(event.target.value);
										}}
										
										>
									  <MenuItem value={"admin"}>Admin</MenuItem>
									  <MenuItem value={"booking"}>Booking</MenuItem>        
									  </Select>
									  <FormHelperText error={true}>{accessTypeError ? "Select a Access type": ""}</FormHelperText>
								</FormControl>
								<br/>
								<TextField style={{marginRight:20 , marginBottom:20 }} autoFocus margin="dense" id="firstName" label="First Name" 
									value={firstName} error={firstNameError} helperText={firstNameError ? "Please Enter a valid First Name":""}
									onChange={(event,newInputValue) => {
											setFirstName(event.target.value);
										}}	
										/>
								
								<TextField style={{marginRight:20 , marginBottom:20 }}  margin="dense" id="lastName" label="Last Name"  
									value={lastName} error={lastNameError} helperText={lastNameError ? "Please Enter a valid Last Name":""}
									onChange={(event,newInputValue) => {
											setLastName(event.target.value);
										}}	
										/>
								<TextField style={{marginRight:20 , marginBottom:20 }}  margin="dense" id="userName" label="User Name"  
								value={userName} error={userNameError} helperText={userNameError ? "Please Enter a valid UserName":""}
									onChange={(event,newInputValue) => {
											setUserName(validator.trim(event.target.value));
										}}	
								/>
								{accessType === 'admin' ?
								<FormControl className={classes.formControl}>
									  <InputLabel id="Approval_Required" style={{marginRight:20 , marginBottom:20 }} >Approval Required</InputLabel>
										<Select className="mdb-select" labelId="Approval_Required" id="Approval_Required" label="Approval_Required" value={approvalRequired}           
									         
										onChange={(event,newInputValue) => {
											setApprovalRequired(event.target.value);
										}}
										
										>
									  <MenuItem value={"approvalYes"}>YES</MenuItem>
									  <MenuItem value={"approvalNo"}>NO</MenuItem>        
									  </Select>
									  <FormHelperText error={true}> {approvalRequiredError ? "Select required approval": ""}</FormHelperText>
								</FormControl>
								: null}
								<br/>
								<TextField  style={{marginRight:20 , marginBottom:20 }}  margin="dense" id="newPassword" label="New Password" 
								value={newPassword}  error={newPasswordError} helperText={newPasswordError ? "Passwords should not be empty":""}
									onChange={(event,newInputValue) => {
											setNewPassword(event.target.value);
										}}	
								/>
								<TextField  style={{marginRight:20 , marginBottom:20 }}  margin="dense" id="confirmPassword" label="Confirm New Password" 
								value={confirmPassword} error={confirmPasswordError} helperText={confirmPasswordError ? "Passwords are not Matching":""}
									onChange={(event,newInputValue) => {
											setConfirmPassword(event.target.value);
										}}	
								/>
								
								
								{ validationError ?
									<DialogContentText style={{color: '#0000FF' }}>
									{validationMessage}															
									</DialogContentText>
								:
									<DialogContentText>
																									
									</DialogContentText>
								}
								
								</DialogContent>
								<DialogActions>
								  <Button onClick={handleClose} color="primary">
									Cancel
								  </Button>
								  { editOpen ?
								  <Button onClick={handleUpdateUser} color="primary">
									Update
								  </Button>
								  :
								  <Button onClick={handleAddUser} color="primary">
									Add
								  </Button>
								  }
								</DialogActions>
							  </Dialog>
							  
							  <Dialog open={deleteOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
						<DialogTitle id="form-dialog-title">Access Details</DialogTitle>
							<DialogContent>			
								{ deleteAccessError ?
									<DialogContentText>
										Unable to delete please try after some time.															
									</DialogContentText>
								:
									<DialogContentText>
																									
									</DialogContentText>
								}
									<DialogContentText>
										Add you sure you want to Delete ?																
									</DialogContentText>
							</DialogContent>
								<DialogActions>
								  <Button onClick={handleDelete} color="primary">
									YES
								  </Button>
								  <Button onClick={handleDeleteClose} color="primary">
									NO
								  </Button>
								</DialogActions>
							  </Dialog>
							  
							  
                    <h2>User Access Details</h2>
						<form className={classes.root} noValidate autoComplete="new-password"  style={{ display: 'block', marginRight:200 , marginLeft:200 }}  >  
						  <ReactTable      
						  data={accessDetails}
						  columns={columns}      
						  defaultPageSize={10}
						
							
							   
						/>
						</form>
                </div>

            </div>
        </div>
    );
}