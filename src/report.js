import React, {  useState , useEffect } from 'react';
import "./styles.css";
import SideBar from "./sidebar";
import * as myConstClass from './Constants.js';
import "react-table-6/react-table.css";
import ReactTable from 'react-table-6';
import { makeStyles } from '@material-ui/core/styles';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import moment from 'moment';
import Delete from '@material-ui/icons/Delete';
import exportFromJSON from 'export-from-json'
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Fab from '@material-ui/core/Fab';
import { CSVLink, CSVDownload } from "react-csv";
import { useHistory } from 'react-router-dom';

import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            width: '25ch',

        },
    },
    formControl: {
        margin: theme.spacing(2),
        width: '25ch',
    },

}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

var dict = {};
var data = "";
var transforms = [];
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 450,
        },
    },
};

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
};

function App() {
	  let history = useHistory();
    const classes = useStyles();
    const { Parser, transforms: { unwind } } = require('json2csv');
    const flatten = require('flat').flatten;
    const [successopen, setSuccessOpen] = React.useState(false);
    const [erroropen, setErrorOpen] = React.useState(false);
    const [reportType, setreportType] = React.useState("");
    const [reportTypeError, setreportTypeError] = React.useState(false);
    const [toDate, settoDate] = React.useState(moment());
    const [fromDate, setfromDate] = React.useState(moment());
    const [reportSelected, setreportSelected] = React.useState(false);
    const [reportDetails, setreportDetails] = React.useState("helll");
    const [showReport, setshowReport] = React.useState(false);
    const [fields, setfields] = React.useState([]);
    const [myfield, setmyfield] = React.useState([]);


 useEffect(() => {
		loadData();
	},[]);
  
  const loadData = async () => {
	  
	  
	    if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
			
	  } else {
		  history.push('/home');
	  }
	  
	  
    
  }
    const handlereportTypeChange = (event) => {
        setreportType(event.target.value);
        setreportTypeError(false);
        setshowReport(false);
        setmyfield([]);
        setfields([]);
    };
    const handlefromDate = (date) => {
        setfromDate(date);
    };
    const handletoDate = (date) => {
        settoDate(date);
    };
    const handleChange = (event) => {
        setmyfield(event.target.value);
    };

    const handleGenerate = async (e) => {
        var eflag = false;
        if (reportType == "") {
            setreportTypeError(true);
            eflag = true;
        }
        e.preventDefault();
        if (eflag) {
            setErrorOpen(true);
            setSuccessOpen(false);
        }
        else {
            var myUrl = "";
            setErrorOpen(false);            
            var jsonfields = "";
            var actualfields = "";
            switch (reportType) {
                case "Driver Details":
                    myUrl = myConstClass.GET_DRIVER_DETAILS;
                    actualfields = ['Driver ID', 'Driver FirstName', 'Driver LastName', 'Phone', 'Alternate Number', 'License', 'License Expiry', 'Salary', 'Address 1', 'Address 2', 'Status', 'Salary Bank Name', 'Salary Account Number', 'IFSC Code', 'Salary Details'];
                    jsonfields = ['driverId', 'firstName', 'lastName', 'phoneNumber', 'alternatePhoneNumber', 'driverLicense', 'licenseExpDate', 'driverSalary', 'driverAddress1', 'driverAddress2', 'driverStatus', 'driverBankDetails.bankName', 'driverBankDetails.accountNumber', 'driverBankDetails.ifscCode', 'driverSalaryDetails'];
                    transforms = [unwind({ paths: ['driverBankDetails'] })];                   
                    
                    break;
                case "Booking Details":
                    myUrl = myConstClass.GET_BOOKING_DETAILS;
                    actualfields = ['Booking ID', 'Trip ID','Vehicle Number', 'Vehicle Type', 'Company','Trip Type','Container Number','Driver ID', 'Driver Name', 'Driver Advance', 'Driver Phone', 'Driver Salary', 'Customer ID','Customer Name', 'Invoice Amount', 'Location ID', 'Loading Point','Unloading Point','Toll','RTO','Halting Charges', 'Vechile Owned By', 'Booking Open Date','Open Approved Date','Booking Close Date', 'Close Approved Date','Opened Approved By','Close Approved By', 'Payment Received','Payment Received Date', 'Booking Status', 'Invoice Number','Booking Opened By','Booking Closed By', 'Movement Date'];
                    jsonfields = ['bookingId', 'tripDetails.tripId', 'tripDetails.vehicleNumber', 'tripDetails.vehicleType', 'tripDetails.vehicleCompany', 'tripDetails.importOrExport', 'tripDetails.containerNumber', 'tripDetails.driverId', 'tripDetails.driverName', 'tripDetails.driverAdvance', 'tripDetails.driverPhoneNumber', 'tripDetails.driverSalary', 'tripDetails.customerId', 'tripDetails.customerName',  'tripDetails.billAmount', 'tripDetails.locationId', 'tripDetails.loadingPoint', 'tripDetails.destination', 'tripDetails.tollCharges', 'tripDetails.rtoCharges', 'tripDetails.haltingCharges', 'tripDetails.vehicleOwnedBy', 'bookingOpenDate', 'bookingOpenApprovedDate', 'bookingCloseDate', 'bookingCloseApprovedDate', 'bookingOpenApprovedBy', 'bookingCloseApprovedBy', 'paymentReceived', 'paymentReceivedDate', 'bookingStatus', 'billingInvoiceNumber', 'bookingOpenedBy', 'bookingClosedBy', 'bookingPickupDate']
                    transforms = [unwind({ paths: ['tripDetails'] })];
                    break;
                case "Halting Details":
                    myUrl = myConstClass.GET_HALTING_DETAILS;
                    actualfields = ['Halting ID', 'Driver ID', 'Driver Name', 'Driver Phone', 'Amount', 'Date', 'Type of Charge', 'Credit/Debit', 'Comments', 'Payment Received', 'Received Date'];
                    jsonfields = ['haltingId', 'driverId', 'driverName', 'driverPhoneNumber', 'haltingCharges', 'haltingChargedDate', 'typeOfHalt',  'creditOrDebit','haltingComments', 'paymentReceived', 'paymentReceivedDate'];
                    transforms = [unwind({ paths: [''] })];
                    break;
                case "Vehicle Details":
                    myUrl = myConstClass.GET_VEHICLES_DETAILS;
                    actualfields = ['Vehicle Number', 'Vehicle Model', 'Type', 'Company', 'Insurance', 'Insurance Validity', 'Insurance Status', 'FC Validity', 'FC Status', 'Road Tax Validity', 'Road Tax Status', 'Road Tax Amount', 'NP Validity', 'NP Status', 'TN Permit Validity', 'TN Permit Status'];
                    jsonfields = ['vehicleNumber', 'vehicleModel', 'vehicleType', 'vehicleCompany', 'insuranceName', 'insuranceValiditiy', 'insuranceStatus', 'fcValidity', 'fcStatus', 'roadTaxValiditiy', 'roadTaxStatus', 'roadTaxAmount', 'npValiditiy', 'npStatus', 'tnPermitValiditiy', 'tnPermitStatus'];
                    transforms = [unwind({ paths: [''] })];
                    break;
                case "Vehicle Expenses":
                    myUrl = myConstClass.GET_OTHEREXPENSE_DETAILS;
                    jsonfields = [];
                    transforms = [unwind({ paths: [''] })];
                    break;
                case "Location Details":
                    myUrl = myConstClass.GET_LOCATION_DETAILS;
                    actualfields = ['Location ID', 'Customer ID', 'Customer Name', 'Loading Point', 'Unloading Point', 'Vehicle Type', 'Invoice Amount', 'Driver Salary'];
                    jsonfields = ['locationId', 'customerId', 'customerName', 'sourceLocation', 'destinationLocation', 'vehicleType', 'customerInvoiceAmount', 'driverSalary'];
                    transforms = [unwind({ paths: [''] })];
                    break;
                case "Customer Details":
                    myUrl = myConstClass.GET_CUSTOMER_DETAILS;
                    actualfields = ['Customer ID', 'Customer Name', 'Address 1', 'Address2','GST', 'PAN Number', 'Email ID', 'Status', 'Contact Name', 'Contact Number'];
                    jsonfields = ['customerId', 'customerName', 'customerAddress1', 'customerAddress2', 'customerGST', 'customerPAN', 'customeremail', 'customerStatus', 'customerPOC', 'customerContact'];
                    transforms = [unwind({ paths: [''] })];
                    break;
                case "User Details":
                    myUrl = myConstClass.GET_ACCESS_DETAILS;
                    actualfields = ['Access ID', 'Access Type', 'First Name', 'Last Name', 'User Name', 'Password', 'Approval Required'];
                    jsonfields = ['accessId', 'accessType', 'firstName', 'lastName', 'userName', 'accessPassword', 'approvalRequired']
                    transforms = [unwind({ paths: [''] })];
                    break;
            }
            var i = 0;
            for (const f of actualfields) {
                dict[f] = jsonfields[i];
                i++;
            }
            i = 0;
            for (const f of jsonfields) {
                dict[f] = actualfields[i];
                i++;
            }
            setfields(actualfields);
            setreportSelected(true);
            console.log(dict);
            const response = await fetch(myUrl);
            data = await response.json();
            // var flatArray = [];
            // var flatObject = {};
            // for (var index = 0; index < data.length; index++) {
            //     console.log("out" + data.length)
            //     for (var prop in data[index]) {

            //         var value = data[index][prop];
            //         if (Array.isArray(value)) {
            //             for (var i = 0; i < value.length; i++) {
            //                 console.log("in" + prop)
            //                 for (var inProp in value[i]) {
            //                     flatObject[inProp] = value[i][inProp];

            //                 }
            //             }
            //         }
            //             else
            //                 flatObject[prop] = value;
            //                 console.log(flatObject);
            //                 flatArray.push(flatObject);
            //     }

            //     flatArray.push(flatObject);
            // }
            // setreportDetails(flatArray);



        }
    };

    function handlePullReport(){
        var fields = [];
        for (const f of myfield) {
            fields.push(dict[f]);
        }
        const json2csvParser = new Parser({ fields, transforms });
        console.log(json2csvParser);
        var csv = json2csvParser.parse(data);
        for(const f of fields)
        {
            console.log(f + dict[f]);
            console.log(typeof(csv));
            csv = csv.replace(f,dict[f]);
            
        }
        setreportDetails(csv);
        console.log(csv);
        setSuccessOpen(true);
        setreportSelected(false);
        setshowReport(true);

    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSuccessOpen(false);
        setErrorOpen(false);
    };



    return (
        <div id="App">
            <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
            <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
                <div className="appHeaderClass"> UNITED TRANSPORT</div>
                <div>
                    <h2>Pull Reports</h2>
                    <br /> <br />
                    <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
                        <div align="center" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
                            <FormControl className={classes.formControl} error={reportTypeError}>
                                <InputLabel id="select company label">Report Type</InputLabel>
                                <Select
                                    className="mdb-select"
                                    labelId="select delivery"
                                    id="select delivery"
                                    label="Report Type"
                                    value={reportType}
                                    onChange={handlereportTypeChange}
                                >
                                    <MenuItem value={"Driver Details"}>Driver Details</MenuItem>
                                    <MenuItem value={"Booking Details"}>Booking Details</MenuItem>
                                    <MenuItem value={"Halting Details"}>Halting Details</MenuItem>
                                    <MenuItem value={"Vehicle Details"}>Vehicle Details</MenuItem>
                                    <MenuItem value={"Vehicle Expenses"}>Vehicle Expenses</MenuItem>
                                    <MenuItem value={"Location Details"}>Location Details</MenuItem>
                                    <MenuItem value={"Customer Details"}>Customer Details</MenuItem>
                                    <MenuItem value={"User Details"}>User Details</MenuItem>
                                </Select>
                                <FormHelperText>{reportTypeError ? "Select a report type" : ""}</FormHelperText>
                            </FormControl>
                            &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                            <Fab aria-label="add" onClick={handleGenerate} style={{ opacity: 0.5 }}>
                                <ArrowForwardIosIcon color="primary" style={{ opacity: 1 }} disabled={!reportSelected} />
                            </Fab>
                            &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-mutiple-checkbox-label">Fields</InputLabel>
                                <Select
                                    labelId="demo-mutiple-checkbox-label"
                                    id="demo-mutiple-checkbox"
                                    multiple
                                    value={myfield}
                                    onChange={handleChange}
                                    input={<Input />}
                                    renderValue={(selected) => selected.join(', ')}
                                //MenuProps={MenuProps}

                                >
                                    {fields.map((name) => (
                                        <MenuItem key={name} value={name} >
                                            <Checkbox checked={myfield.indexOf(name) > -1} />
                                            <ListItemText primary={name} style={{ color: 'black' }} />
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </div>

                    </form>
                    <br /><br /><br />
                    
                </div>
                <Button variant="contained" color="primary" allign="center"  disabled={!reportSelected} style={{ fontSize: "90%", opacity: "1", height: 40 }} onClick={handlePullReport}>Pull</Button>

                &nbsp;&nbsp;
                <CSVLink data={reportDetails} disabled={!showReport} filename={reportType + ".csv"}
                    style={{
                        backgroundColor: showReport ? "#3f51b5" : "#D3D3D3",
                        display: "inline-block",
                        cursor: "pointer",
                        color: showReport ? "#FFFFFF" : "#BEBEBE",
                        fontSize: "14px",
                        fontWeight: "bold",
                        padding: "10px",
                        textDecoration: "none",
                    }} > DOWNLOAD REPORT </CSVLink>
                <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success">
                        Report generated successfully ! You can now download the report.
                    </Alert>
                </Snackbar>
                <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="error">
                        Please fill all the mandatory fields.
                    </Alert>
                </Snackbar>
            </div>

        </div>
    );
}

export default App;



