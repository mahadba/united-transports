import React, {  useEffect, useState } from 'react';
import "react-table-6/react-table.css";
import "./styles.css";
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Snackbar from '@material-ui/core/Snackbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { haltingChargeDetails, vehicleDetails } from './Constants';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import html2canvas from 'html2canvas'; 
import jsPDF from 'jspdf';
import { PDFDownloadLink} from '@react-pdf/renderer'
import { PDFViewer } from '@react-pdf/renderer';
import { Document, Page, View, Text } from '@react-pdf/renderer';
import * as myConstClass from './Constants.js';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';


	
const useStyles = makeStyles(theme => ({
  root: {
    //display: 'flex',
    //flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',
      
    },
  },
  formControl: {
    margin: theme.spacing(1),
    width: '25ch',
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    //maxWidth: 500,
  },
  table: {
   // minWidth: 100,
  },
  
}));



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}



function App() {
	let history = useHistory();
  const classes = useStyles(); 
  const [driver, setdriver] = React.useState("");
  const [driverId, setDriverId] = React.useState("");
  const [phoneNumber, setphoneNumber] = React.useState("");
  const [resetDriver, setresetDriver] = React.useState("driver");
  const [driverError, setdriverError] = React.useState(false);
  const [previousCredit, setpreviousCredit] = React.useState(0);
  const [currentCredit, setcurrentCredit] = React.useState(0);
  const [bookingFound, setbookingFound] = React.useState(false);
  const [haltingFound, sethaltingFound] = React.useState(false);
  const [bookingTotal, setbookingTotal] = React.useState(0);
  const [haltingTotal, sethaltingTotal] = React.useState(0);
  const [bank, setbank] = React.useState("");
  const [ifscCode, setifscCode] = React.useState("");
  const [accNumber, setaccNumber] = React.useState("");
  const [license, setlicense] = React.useState("");
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [pdfurl,setpdfurl] = React.useState("");
  const GET_DRIVER_DETAILS = myConstClass.GET_DRIVER_DETAILS;
  const [driverDetails, setDriverDetails] = React.useState([]);
  const [driverHaltingDetails, setDriverHaltingDetails] = React.useState([]);
  const [driverFirstName, setDriverFirstName] = React.useState('');
  const [driverIDNum, setDriverIDNum] = React.useState('');
  const DRIVER_SALARY_DETAILS = myConstClass.DRIVER_SALARY_DETAILS;
  const GET_DRIVER_HALTING_DETAILS = myConstClass.GET_DRIVER_HALTING_DETAILS;
  
  const [bookingDetails, setbookingDetails] = React.useState([]);
  
  function reset(){
    setphoneNumber("");
    setdriverError(false);
    setpreviousCredit(0);
    setcurrentCredit(0);
    setbookingTotal(0);
    sethaltingTotal(0);
    setbookingFound(false);
    sethaltingFound(false);
    setbank("");
    setifscCode("");
    setaccNumber("");
    setlicense("");
  }
  
  useEffect(() => {
		loadDriveDetails();
		//loadDriverHaltingDetails();
		
		//loadBookingDetails();
	},[]);
  
	const loadDriveDetails = async () => {
		
		
		
		  if("booking"=== localStorage.getItem('accessType')) {
		history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				const response = await fetch(GET_DRIVER_DETAILS);
				const data = await response.json();
			
				setDriverDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
	  
	}
	
	const loadDriverHaltingDetails = async (drivernumber) => {
		const response = await fetch(GET_DRIVER_HALTING_DETAILS+drivernumber);
		const data = await response.json();
		setDriverHaltingDetails(data);
		console.log("loadDriverHaltingDetails : "+drivernumber);
	}

	const loadDriverSalaryDetails = async (drivernumber) => {
		const response = await fetch(DRIVER_SALARY_DETAILS+drivernumber);
		const data = await response.json();
		setbookingDetails(data);
		console.log("loadDriverSalaryDetails : "+driverId);
		console.log("driverIDNum : "+driverIDNum);
		console.log("drivernumber : "+drivernumber);
	  }

	const defaultDriverProps = {
		options: driverDetails,
		getOptionLabel: (option) => option.firstName};
  
  const handleDriverSelect= (event,newvalue) => {   
    reset();
	
	console.log("newvalue.firstName : "+newvalue.firstName);
	console.log("newvalue.driverId : "+newvalue.driverId);
    setdriver(newvalue.firstName);
	setDriverId(newvalue.driverId);
	
    var vn = newvalue.driverId;
	setDriverFirstName(newvalue.firstName);
	setDriverIDNum(newvalue.driverId);
	
    setdriverError(false);
    
	var i = driverDetails.find(item=> {
        return item.driverId === vn
    }) 
        if(i)
        {
            setphoneNumber(i.phoneNumber);  
			loadDriverSalaryDetails(newvalue.driverId);
			loadDriverHaltingDetails(newvalue.driverId);			
        }
  };


  function HaltingComponent(){
    let haltingItem=[];
	
    var totalHalting=0;
    
    
	var totalCreditAmt = 0;
	var totalDebitAmt = 0;
	
	
	var i = driverHaltingDetails;
	
//	if(i){
  //    var found = i.find(item=>{return item.status.toString().toLowerCase()==="unpaid"})
   //   if(found){
		  
        sethaltingFound(true);
      haltingItem.push( <TableRow>
        <TableCell align="center"><b>HALT ID</b></TableCell>
        <TableCell align="right"><b>TYPE</b></TableCell>
		<TableCell align="right"><b>CR/DB</b></TableCell>
        <TableCell align="right"><b>Date</b></TableCell>
        <TableCell align="right"><b>Comments</b></TableCell>        
        <TableCell align="right"><b>AMOUNT</b></TableCell>
      </TableRow>);
     
	 
      for(var j=0; j<i.length; j++){
        haltingItem.push(<TableRow>
          <TableCell  align="center">{i[j].haltingId}</TableCell>
          <TableCell align="right">{i[j].typeOfHalt}</TableCell>
          <TableCell align="right">{i[j].creditOrDebit}</TableCell>
          <TableCell align="right">{i[j].haltingChargedDate}</TableCell> 
		  <TableCell align="right">{i[j].haltingComments}</TableCell> 		  
          <TableCell align="right">{parseFloat(i[j].haltingCharges).toFixed(2)}</TableCell>
        </TableRow>);
		
		if('CREDIT' === i[j].creditOrDebit){
			totalCreditAmt = Number(totalCreditAmt) + Number(i[j].haltingCharges) ;
		} else if ('DEBIT' === i[j].creditOrDebit){
			totalDebitAmt = Number(totalDebitAmt) + Number(i[j].haltingCharges) ;
		}
       // totalHalting = (parseFloat(totalHalting) + parseFloat(i[j].amount)).toFixed(2);
     } 
	 
	 totalHalting = Number(totalCreditAmt) - Number(totalDebitAmt) ;
	 
	 console.log(" final totalHalting :" +totalHalting );		  
      sethaltingTotal(parseFloat(totalHalting).toFixed(2));
      //haltingItem.push();
     //   sethaltingTotal(totalHalting);
  //  }
   // }
    return (haltingItem);
  };
  
  function BookingComponent(){
    let bookingItem=[];
    var totalTripAmt=0;
    
	var i=bookingDetails;
	
	console.log("BookingComponent : "+i);
	

	
    var dvalue=driverDetails.find(item => {
      return item.driverId === driverId
      })
    if(dvalue){
      setpreviousCredit(dvalue.credit);
      setbank(dvalue.driverBankDetails.bankName);
      setifscCode(dvalue.driverBankDetails.ifscCode);
      setaccNumber(dvalue.driverBankDetails.accountNumber);
      setlicense(dvalue.driverLicense);
    }
    
      bookingItem.push( <TableRow>
        <TableCell  align="center"><b>BOOKING ID</b></TableCell>
        <TableCell align="right"><b>SALARY<br/>(A)</b></TableCell>
        <TableCell align="right"><b>ADVANCE<br/>(B)</b></TableCell>
        <TableCell align="right"><b>RTO<br/>(C)</b></TableCell>
        <TableCell align="right"><b>TOLL<br/> (D)</b></TableCell>
        <TableCell align="right"><b>BOOKING PAYOUT<br/>(A-B+C+D)</b></TableCell>
      </TableRow>);
      if(i.length){
      setbookingFound(true);
      for(var j=0; j<i.length; j++){
        bookingItem.push(<TableRow>
          <TableCell  align="center">{i[j].bookingId}</TableCell>
          <TableCell align="right">{parseFloat(i[j].salary).toFixed(2)}</TableCell>
          <TableCell align="right">{parseFloat(i[j].salaryAdvance).toFixed(2)}</TableCell>
          <TableCell align="right">{parseFloat(i[j].rtoCharges).toFixed(2)}</TableCell>
          <TableCell align="right">{parseFloat(i[j].tollCharges).toFixed(2)}</TableCell>
		  <TableCell align="right">{parseFloat(i[j].remainingSal).toFixed(2)}</TableCell>
        </TableRow>);
       // totalBooking = (parseFloat(totalBooking) + parseFloat(i[j].salary)-parseFloat(i[j].salaryAdvance)+parseFloat(i[j].RTO)+parseFloat(i[j].toll)).toFixed(2);
		
		totalTripAmt = Number(totalTripAmt) + Number(i[j].remainingSal) ;

console.log("totalTripAmt :" +totalTripAmt );		
      }
      //bookingItem.push();
		console.log(" final totalTripAmt :" +totalTripAmt );		  
      setbookingTotal(parseFloat(totalTripAmt).toFixed(2));
    }
    return (bookingItem);
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }  
    setSuccessOpen(false);  
    setErrorOpen(false);  
  };
    
  function printDocument(){
		const url = myConstClass.PAY_SALARY_DETAILS;
		
		var netSalaryAmount = Number(bookingTotal)+ Number(haltingTotal);
		
		if(netSalaryAmount === 0.00){
			setErrorOpen(true);
			setSuccessOpen(false);
		} else {
			
		const params = 
			{
				"driverId": driverId,
				"totalTripSalaryAmount": bookingTotal,
				"haltingChargesAmount": haltingTotal,
				"netSalaryAmount": netSalaryAmount
			}



		Axios.post(url, params)
				.then(response => {
					console.log("validated Axios");
					console.log("validationStatus :"+response.data.validationStatus === 'true');
					
						if(response.data.validationStatus){
								const input = document.getElementById('salaryTable');  
								const pdf = new jsPDF('p', 'mm', 'a4');
								setErrorOpen(false);
								setSuccessOpen(true);
								html2canvas(input)  
								  .then((canvas) => {  
									var imgWidth = 200;  
									var pageHeight = 290;  
									var imgHeight = canvas.height;  
									var imgWidth = canvas.width;  
									var pheight = pdf.internal.pageSize.getHeight();
									var pwidth = pdf.internal.pageSize.getWidth();
									var ratio  = imgHeight/imgWidth;
									const imgData = canvas.toDataURL('image/png');  
									pheight = ratio*pwidth;
									var position = 30;  
									pdf.setFillColor("white");
									//var heightLeft = imgHeight
									//pdf.addImage(imgData, 'JPG', -40, 40, 300, 220,'','FAST');       
									pdf.addImage(imgData, 'JPEG', 10,30, pwidth*0.9, pheight*0.9,'','FAST');  
									//pdf.fromHTML(BookingComponent);
									var string = pdf.output('dataurlnewwindow');
									console.log(string);
								   setpdfurl(string);
									//x.document.close();
									//pdf.save(driver+"_"+moment().format("DDMMYYhhmmss")+".pdf");  
								  });  
							
      
						} else {
									setErrorOpen(true);
									setSuccessOpen(false);
						
						}
					}).catch(error => {
							console.log("Error Axios");
							console.log(error);
					});
		} 
  };

  return (
  <div id="App">
    <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />      
    <div id="page-wrap" style={{backgroundImage: "url(/hexa3.jpg)", backgroundSize:"cover", widht:"100%", height:"100%"}}>
    <div className="appHeaderClass"> UNITED TRANSPORT</div>   
    <h2>Pay Salary</h2>
    <form className={classes.root} noValidate autoComplete="off"  style={{ display: 'block', width:"100%"}}  >      
      <div align="left"  style={{marginLeft:"4em", display: 'flex', flexWrap:"wrap", justifyContent:"center"}}>  
      <Autocomplete
            {...defaultDriverProps}
            //getOptionSelected={(option, value) => option.driver === value.driver}
            key={resetDriver}
            openOnFocus
            id="driver"
            clearOnEscape
            Select={driver}        
            onChange={handleDriverSelect}
            renderInput={(params) => <TextField {...params} label="Driver" margin="normal"  error={driverError} helperText={driverError ? "Select a driver" : ""}/>}
      />
      <TextField
            id="phoneNumber"
            label="Phone Number"            
            value={phoneNumber}
            InputProps={{readOnly: true,}}          
      />
      </div>
      {driver===""? null : (
      <div align="center" id="salarydiv" style={{justifyContent:"center"}}>  
       <TableContainer id="salary" component={Paper} style={{  backgroundColor:"transparent"}}>
      <Table id="salaryTable" className={classes.table} aria-label="spanning table" style={{width:"800px" , borderStyle:"groove"}}>
        <TableHead>
        <TableRow style={{borderStyle:"solid"}}>
            <TableCell align="center" colSpan={6} style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>UNITED TRANSPORT - {moment().format("DD/MM/YYYY")}</b>
            </TableCell>
        </TableRow>
        <TableRow style={{borderStyle:"solid"}}>
            <TableCell align="left" style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
              <b>NAME</b>
            </TableCell>
            <TableCell align="left" colSpan={2} style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>{driver}</b>
            </TableCell>  
            <TableCell align="left" style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>PHONE</b>
            </TableCell>
            <TableCell align="left" colSpan={2} style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>{phoneNumber}</b>
            </TableCell>         
          </TableRow>
          <TableRow style={{borderStyle:"solid"}}>
            <TableCell align="left" style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>LICENSE</b>
            </TableCell>
            <TableCell align="left" colSpan={2} style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>{license}</b>
            </TableCell>  
            <TableCell align="left" style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>BANK</b>
            </TableCell>
            <TableCell align="left" colSpan={2} style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>{bank}</b>
            </TableCell>
            </TableRow> 
            <TableRow style={{borderStyle:"solid"}}>
            <TableCell align="left" style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>ACCOUNT NUMBER</b>
            </TableCell>
            <TableCell align="left" colSpan={2} style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>{accNumber}</b>
            </TableCell>  
            <TableCell align="left" style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>IFSC CODE</b>
            </TableCell>
            <TableCell align="left" colSpan={2} style={{borderStyle:"solid",borderColor:"black", backgroundColor:"#3366CC", color:"white"}}>
            <b>{ifscCode}</b>
            </TableCell>         
          </TableRow>
        </TableHead>
        <TableBody>
        <TableRow style={{borderStyle:"solid"}}>
            <TableCell align="center" colSpan={6} style={{backgroundColor:"", borderStyle:"solid"}}>
              <b>PAYOUT FROM BOOKINGS</b>
            </TableCell>            
          </TableRow>
        <BookingComponent/>
        <TableRow >
        <TableCell align="right" colSpan={5}><b>Total (E)</b></TableCell>
        <TableCell align="right">{bookingTotal}</TableCell>
        </TableRow>
          <TableRow style={{borderStyle:"solid"}}>
          <TableCell align="center" colSpan={6} style={{backgroundColor:"", borderStyle:"solid"}}>
              <b>PAYOUT FROM HALTING CHARGES</b>
            </TableCell>            
          </TableRow>
          <HaltingComponent/>
          <TableRow>
        <TableCell align="right" colSpan={5}><b>Total (F)</b></TableCell>
        <TableCell align="right">{haltingTotal}</TableCell>
        </TableRow>          
          <TableRow>
          <TableCell align="right" colSpan={5}>
              <b>NET PAY (E+F)</b>
            </TableCell> 
            <TableCell align="right">
            <b>{parseFloat(Number(bookingTotal)+ Number(haltingTotal)).toFixed(2)}</b>
            </TableCell>            
          </TableRow>
        </TableBody>
      </Table>
      </TableContainer>
      <br/>
      <Button variant="contained" color="primary" allign="center" style={{fontSize:"90%"}} onClick={printDocument} >Pay</Button>
      <br/>
      </div>)}
    </form>
    
 {/* <PDFViewer style={{width:"842px",height:"595px", backgroundColor:"transparent"}}>
  <Document file={pdfurl}>
   
</Document>
      </PDFViewer>*/}
  </div>
    <div>
    <div>
  </div>
  </div>
  
  <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
        Salary paid! Downloading salary slip...
        </Alert>
    </Snackbar>
    <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Error saving values in DB.
        </Alert>
    </Snackbar>
    </div>
  );
}
  export default App;