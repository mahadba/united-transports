import React, {  useState, useEffect} from 'react';
import logo from './logo.svg';
import ReactDOM from "react-dom";
import ReactTable from "react-table";
import CssBaseline from '@material-ui/core/CssBaseline';
import "react-table-6/react-table.css";
import "./styles.css";
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Snackbar from '@material-ui/core/Snackbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import * as myConstClass from './Constants.js';
import moment from 'moment';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(3),
      width: '25ch',
      
    },
  },
  formControl: {
    margin: theme.spacing(3),
    width: '25ch',
  },
  
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function App() {
	let history = useHistory();
  const classes = useStyles();  
  const [driver, setdriver] = React.useState("");
  const [driverId, setDriverId] = React.useState("");
  const [resetDriver, setresetDriver] = React.useState("driver");
  const [driverError, setdriverError] = React.useState(false);
  const [phoneNumber, setphoneNumber] = React.useState("");
  const [alternatePhoneNumber, setalternatePhoneNumber] = React.useState("");
  const [phoneNumberError, setphoneNumberError] = React.useState(false);
  const [alternatePhoneNumberError, setalternatePhoneNumberError] = React.useState(false);
  const [address1, setaddress1] = React.useState("");
  const [address2, setaddress2] = React.useState("");
  const [address1Error, setaddress1Error] = React.useState(false);
  const [licenseNumber, setlicenseNumber] = React.useState("");
  const [licenseNumberError, setlicenseNumberError] = React.useState(false);
  const [expiry, setexpiry] = React.useState(moment().format("YYYY-MM-DD"));
  const [expiryError, setexpiryError] = React.useState(false);
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [bank, setbank] = React.useState("");
  const [bankError, setbankError] = React.useState(false);
  const [ifscCode, setifscCode] = React.useState("");
  const [ifscCodeError, setifscCodeError] = React.useState(false);
  const [accNumber, setaccNumber] = React.useState("");
  const [status, setstatus] = React.useState("ACTIVE");
  const [statusError, setstatusError] = React.useState(false);
  const [errorMsg, seterrorMsg] = React.useState("");
  const [accNumberError, setaccNumberError] = React.useState(false);
  const [driverDetails, setdriverDetails] = React.useState([]);
  const POST_API_URL = myConstClass.POST_DRIVER_DETAILS;
  const GET_API_URL = myConstClass.GET_DRIVER_DETAILS;

  
  useEffect(() => {
    loadData();
  },[]);
  
  const loadData = async () => {
	  
	   if("booking"=== localStorage.getItem('accessType')) {
		history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				
	  } else {
		  history.push('/home');
	  }
	  
    
  }
 
 const handleDriverUpdate = event =>{
   setdriver(event.target.value);
 }
  const handleDriverChange = (event, newvalue) => { 
    setdriver(newvalue);
    setdriverError(false);
    var vn=newvalue;    
    var i = driverDetails.find(item=> {
      return item.firstName ===vn
    });
    if (i){
			//console.log("driverId  : "+i.driverId);
			//setDriverId(i.driverId);
			setphoneNumber(i.phoneNumber);
			setalternatePhoneNumber(i.alternatePhoneNumber);
			setaddress1(i.driverAddress1);
			setaddress2(i.driverAddress2);
			setlicenseNumber(i.driverLicense);
			setbank(i.driverBankDetails.bankName);
			setifscCode(i.driverBankDetails.ifscCode);
			setaccNumber(i.driverBankDetails.accountNumber);
			setdriverError(false);
			setbankError(false);
			setifscCodeError(false);
			setaccNumberError(false);
			setphoneNumberError(false);
			setlicenseNumberError(false);
			setalternatePhoneNumberError(false);
			setaddress1Error(false);    
			setexpiry(moment(i.licenseExpDate,"DD/MM/YYYY"));
			setstatus(i.driverStatus);
    }
  };
  const handleDriverBlur = (event) => {
    setdriver(event.target.value);
  }
  const handleDriverValue = (event) => {
    setdriverError(false);
  }
  const handleStatusChange = (event) => {
    setstatus(event.target.value);
    setstatusError(false);
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }  
    setSuccessOpen(false);  
    setErrorOpen(false);  
  };
  const handleClick= (e) => { 
    var eflag=false;
    if(driver==""){
        setdriverError(true);
        eflag=true;
    }
    if(licenseNumber==""){
        eflag=true;
        setlicenseNumberError(true);
    }
    if(phoneNumber==""){
        setphoneNumberError(true);
        eflag=true;
    }
    if(address1==""){
        setaddress1Error(true);
        eflag=true;
    }
    if(bank==""){
      setbankError(true);
      eflag=true;
    }
    if(ifscCode==""){
      setifscCodeError(true);
      eflag=true;
    }
    if(accNumber==""){
      setaccNumberError(true);
      eflag=true;
    }
    e.preventDefault();
    if(eflag){
      seterrorMsg("Please fill all the mandatory fields!");
        setErrorOpen(true);
        setSuccessOpen(false);
    }
    else  
    {   setErrorOpen(false);
      axios.post(POST_API_URL, {
		"driverId":driverId,
        "firstName": driver,
        "phoneNumber": phoneNumber,
        "alternatePhoneNumber": alternatePhoneNumber,
        "driverLicense": licenseNumber,
        "licenseExpDate": moment(expiry).format("DD-MM-YYYY"),
        "driverAddress1": address1,
        "driverAddress2": address2,
        "driverStatus": status,
        "driverBankDetails": {
          "bankName": bank,
          "accountNumber": accNumber,
          "ifscCode": ifscCode
      }
      })
        .then(function (response) {
          console.log(response.data);
        //  loadData();
          setSuccessOpen(true);
		  
				var vtime=moment().format("hhmmss");
				setresetDriver("resetDriver"+vtime);
				setDriverId("");
				setdriver("");
				setphoneNumber("");
				setalternatePhoneNumber("");
				setlicenseNumber("");
				setaddress1("");
				setaddress2("");
				setexpiry(moment().format("YYYY-MM-DD"));
				setbank("");
				setifscCode("");
				setaccNumber("");
				setstatus("ACTIVE");
        }).catch(function (error) {
          console.log(error);
          seterrorMsg("Unable to save Details");
          setErrorOpen(true);
        })
       
    }
  };
  const handleExpiryChange = (date) => {
    setexpiry(date);
  };
return (   
      
    <div id="App">
    <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"}/> 
    <div id="page-wrap" style={{backgroundImage: "url(/hexa3.jpg)", backgroundSize:"cover", widht:"100%", height:"100%"}}>
    <div className="appHeaderClass"> UNITED TRANSPORT</div> 
    <div>
    <h2>Add Driver Details</h2>
    <br/> <br/> 
    <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
    <div align="left" style={{marginLeft:"4em",display: 'flex', flexWrap:"wrap"}}>         
        <Autocomplete
            id="driver"
            freeSolo  
            options={driverDetails.map((option) => option.firstName)}
            key={resetDriver}
            value={driver}     
            onChange={handleDriverChange}                              
            renderInput={(params) => <TextField {...params} onChange={handleDriverUpdate} label="Driver Name" margin="normal" error={driverError} helperText={driverError ? "Driver name is required" : ""}/>}
        />
        <TextField
          id="standard-number"
          label="License Number"
          error={licenseNumberError} helperText={licenseNumberError ? "Enter a valid license number" : ""}
          value={licenseNumber}         
          onChange={e => {setlicenseNumber(e.target.value); setlicenseNumberError(false);}}
        />
        <TextField
          id="standard-number"
          label="Phone"
          type="tel" error={phoneNumberError} helperText={phoneNumberError ? "Enter a valid phone number" : ""}
          value={phoneNumber}         
          onChange={e => {setphoneNumber(e.target.value); setphoneNumberError(false);}}
        />  
        <TextField
          id="standard-number"
          label="Alternate Phone"
          type="tel" error={alternatePhoneNumberError} helperText={alternatePhoneNumberError ? "Enter a valid phone number" : ""}
          value={alternatePhoneNumber}         
          onChange={e => {setalternatePhoneNumber(e.target.value); setalternatePhoneNumberError(false);}}
          onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault()}}  
        />
       <MuiPickersUtilsProvider utils={DateFnsUtils}>
    <KeyboardDatePicker
          //disableToolbar
          variant="inline"
          format="dd/MM/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Expiry Date"
          value={expiry}
          autoOk
          onChange={handleExpiryChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />       
    </MuiPickersUtilsProvider>   
    </div>
    <div align="left" style={{marginLeft:"4em",display: 'flex', flexWrap:"wrap"}}>         
        <TextField required id="standard-required" label="Address Line 1" style = {{width: 500}} value={address1} error={address1Error} helperText={address1Error ? "Enter a valid address" : ""}        
        onChange={e => {setaddress1(e.target.value); setaddress1Error(false)}}
        />
        <TextField id="standard-required" label="Address Line 2" style = {{width: 500}} value={address2}       
        onChange={e => {setaddress2(e.target.value); }}
        />
    </div>
    <div align="left" style={{marginLeft:"4em",display: 'flex', flexWrap:"wrap"}}>
    <TextField
          id="standard-number"
          label="Bank Name"
          error={bankError} helperText={bankError ? "Enter a valid phone numbert" : ""}
          value={bank}         
          onChange={e => {setbank(e.target.value); setbankError(false);}}
        />
      <TextField
          id="standard-number"
          label="IFSC Code"
          error={ifscCodeError} helperText={ifscCodeError ? "Enter a valid phone numbert" : ""}
          value={ifscCode}         
          onChange={e => {setifscCode(e.target.value); setifscCodeError(false);}}
        />
      <TextField
          id="standard-number"
          label="Account Number"
          error={accNumberError} helperText={accNumberError ? "Enter a valid phone numbert" : ""}
          value={accNumber}         
          onChange={e => {setaccNumber(e.target.value); setaccNumberError(false);}}
        />
        <FormControl className={classes.formControl} error={statusError}  >
              <InputLabel id="select type label">Type</InputLabel>
              <Select
                className="mdb-select"
                labelId="select type label"
                id="select type"
                label="Status"
                value={status}
                onChange={handleStatusChange}
              >
                <MenuItem value={"ACTIVE"}>ACTIVE</MenuItem>
                <MenuItem value={"INACTIVE"}>INACTIVE</MenuItem>
              </Select>
              <FormHelperText>{statusError ? "Select valid vehicle type" : ""}</FormHelperText>
            </FormControl>
     </div>
    <br/><br/>
    <Button variant="contained" color="primary" allign="center" style={{fontSize:"90%", opacity:"1"}} onClick={handleClick}>Submit</Button>
    <br/>
    </form>
    <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Details submitted successfully !
        </Alert>
      </Snackbar>
      <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
         {errorMsg}
        </Alert>
      </Snackbar>
    </div>
    </div>
    </div>
);
}
export default App;