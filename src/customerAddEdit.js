import React, { useState, useEffect } from 'react';
import "react-table-6/react-table.css";
import "./styles.css";
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import * as myConstClass from './Constants.js';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(3),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(3),
    width: '25ch',
  },

}));



function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function App() {
	let history = useHistory();
  const classes = useStyles();
  const [customer, setcustomer] = React.useState("");
  const [customerError, setcustomerError] = React.useState(false);
  const [resetcustomer, setresetcustomer] = React.useState("reset");
  const [contact, setcontact] = React.useState("");
  const [contactError, setcontactError] = React.useState(false);
  const [phone, setphone] = React.useState("");
  const [phoneError, setphoneError] = React.useState(false);
  const [status, setstatus] = React.useState("Active");
  const [statusError, setstatusError] = React.useState(false);
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [mail, setmail] = React.useState("");
  const [mailError, setmailError] = React.useState(false);
  const [gst, setgst] = React.useState("");
  const [gstError, setgstError] = React.useState(false);
  const [pan, setPan] = React.useState("");
  const [panError, setpanError] = React.useState(false);
  const [address1, setaddress1] = React.useState("");
  const [address1Error, setaddress1Error] = React.useState(false);
  const [address2, setaddress2] = React.useState("");
  const [customerId, setCustomerId] = React.useState("");
  const [inputValue, setInputValue] = React.useState('');
  const [address2Error, setaddress2Error] = React.useState(false);

  const [custValidationStatus, setCustValidationStatus] = React.useState(false);
  const [custValidationMsg, setCustValidationMsg] = React.useState("");
  const [validationError, setValidationError] = React.useState(false);

  const API_URL = myConstClass.GET_CUSTOMER_DETAILS;
  const [customerDetails, setCustomerDetails] = useState([]);


  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
	  
	   if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				 setValidationError(false);
				const response = await fetch(API_URL);
				const data = await response.json();


				setCustomerDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
    
  }



  const saveData = async () => {

    const url = myConstClass.POST_CUSTOMER_DETAILS;




    console.log("customerId : " + customerId);
    console.log("customer : " + inputValue);
    console.log("address1 : " + address1);
    console.log("address2 : " + address2);
    console.log("gst : " + gst);
    console.log("mail : " + mail);
    console.log("status : " + status);
    console.log("contact : " + contact);
    console.log("phone : " + phone);
    const params =
    {
      "customerId": customerId,
      "customerName": inputValue,
      "customerAddress1": address1,
      "customerAddress2": address2,
      "customerGST": gst,
      "customerPAN": pan,
      "customeremail": mail,
      "customerStatus": status,
      "customerPOC": contact,
      "customerContact": phone


    }

    Axios.post(url, params)
      .then(response => {
        console.log("validated Axios");

        //setCustDtlValidation(response.data);
        console.log("response.data.validationStatus : " + response.data.validationStatus);
        console.log("response.data.validationMessage : " + response.data.validationMessage);
        setCustValidationStatus(response.data.validationStatus);
        setCustValidationMsg(response.data.validationMessage);


        console.log("custValidationStatus :" + custValidationStatus);
        console.log("custValidationMsg.validationStatus :" + response.data.validationStatus === true);


        console.log("validationStatus :" + response.data.validationStatus === 'true');
        if (response.data.validationStatus) {

          loadData();


          setErrorOpen(false);
          setSuccessOpen(true);
          var vt = moment().format("hhmmss");
          setresetcustomer(vt);
          setcustomer("");
          setphone("");
          setcontact("");
          setmail("");
          setgst("");
          setPan("");
          setaddress1("");
          setaddress2("");
          setstatus("Active");
        } else {
          setErrorOpen(false);
          setSuccessOpen(false);
          setValidationError(true);
        }





      })
      .catch(error => {
        console.log("Error Axios");
        console.log(error);

      });



    // console.log(data);
  }

  const handleCustomerSelect = (event, newvalue) => {
    console.log("sdfsfd : " + newvalue);
    setcustomer(newvalue);
    setcustomerError(false);
    var vn = newvalue;
    var i = customerDetails.find(item => {
      return item.customerName === vn
    })
    console.log(i);
    if (i) {
      setCustomerId(i.customerId);
      setcontact(i.customerPOC);
      setcontactError(false);
      setphone(i.customerContact);
      setphoneError(false);
      setstatus(i.customerStatus);
      setmail(i.customeremail);
      setmailError(false);
      setgst(i.customerGST);
      setPan(i.customerPAN);
      setgstError(false);
      setaddress1(i.customerAddress1);
      setaddress1Error(false);
      setaddress2(i.customerAddress2);
    }
  };
  const handleStatusChange = (event) => {
    setstatus(event.target.value);
    setstatusError(false);
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    setErrorOpen(false);
  };
  const handleClick = (e) => {
    var eflag = false;

    console.log("customer NAme : " + customer);
    console.log("inputValue NAme : " + inputValue);

    if (customer) {
      console.log("customer NAme : " + customer);
    } else {
      console.log("customer null value ");
    }

    if (inputValue) {
      console.log("inputValue NAme : " + inputValue);
    } else {
      console.log("inputValue null value ");
    }

    if ((customer && inputValue)) {
      console.log("both values are not null");
    } else if (customer || inputValue) {
      console.log("either one is null");
    } else {
      setcustomerError(true);
      eflag = true;
    }

    if ((customer && inputValue)) {
      // "customerName": customer,
      setcustomer(customer);
      console.log("both values are not null");
    } else if (customer) {
      // "customerName": customer,
      setcustomer(customer);
      console.log("customer  is not null");
    } else if (inputValue) {
      console.log("input  is not null" + inputValue);
      //"customerName": inputValue,
      setcustomer(inputValue);
    }


    if (customer === "" && inputValue === "") {
      setcustomerError(true);
      eflag = true;
    }


    if (phone === "") {
      setphoneError(true);
      eflag = true;
    }
    if (contact === "") {
      setcontactError(true);
      eflag = true;
    }

    if (mail === "") {
      setmailError(true);
      eflag = true;
    }
    if (gst === "") {
      setgstError(true);
      eflag = true;
    }
    if (address1 === "") {
      setaddress1Error(true);
      eflag = true;
    }
    if (eflag) {
      setErrorOpen(true);
      setSuccessOpen(false);
    }
    else {


      saveData();



    }
  }

  return (

    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <div>
          <h2>Edit Customer Details</h2>

          <br />
          <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
            <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
              <Autocomplete
                id="customer"
                freeSolo
                options={customerDetails.map((option) => option.customerName)}
                key={resetcustomer}
                inputValue={inputValue}
                onInputChange={(event, newInputValue) => {
                  setInputValue(newInputValue);
                }}
                select={customer}
                onChange={handleCustomerSelect}
                renderInput={(params) => <TextField {...params} required label="Customer Company" margin="normal" error={customerError} helperText={customerError ? "Company name is required" : ""} />}
              />
              <TextField required id="standard-required" label="Contact Name" defaultValue="" value={contact} error={contactError} helperText={contactError ? "Enter a contact name" : ""}
                onChange={e => { setcontact(e.target.value); setcontactError(false) }}
              />
              <TextField required id="standard-required" type="tel" label="Contact Phone" defaultValue="" value={phone} error={phoneError} helperText={phoneError ? "Enter a contact number" : ""}
                onChange={e => { setphone(e.target.value); setphoneError(false) }}
              />

              <FormControl className={classes.formControl} error={statusError}  >
                <InputLabel id="select type label">Status</InputLabel>
                <Select
                  className="mdb-select"
                  labelId="select type label"
                  id="select status"
                  label="Status"
                  value={status}
                  onChange={handleStatusChange}
                >
                  <MenuItem value={"Active"}>Active</MenuItem>
                  <MenuItem value={"Inactive"}>Inactive</MenuItem>
                </Select>
                <FormHelperText>{statusError ? "Select valid status" : ""}</FormHelperText>
              </FormControl>
            </div>
            <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
              <TextField required id="standard-required" label="Mail" defaultValue="" value={mail} error={mailError} helperText={mailError ? "Enter a valid mail ID" : ""}
                onChange={e => { setmail(e.target.value); setmailError(false) }}
              />
              <TextField required id="standard-required" label="GST" defaultValue="" value={gst} error={gstError} helperText={gstError ? "Enter a valid GST number" : ""}
                onChange={e => { setgst(e.target.value); setgstError(false) }}
              />

              <TextField required id="standard-required" label="PAN" defaultValue="" value={pan} error={panError} helperText={gstError ? "Enter a valid PAN number" : ""}
                onChange={e => { setPan(e.target.value); setpanError(false) }}
              />

            </div>
            <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
              <TextField required id="standard-required" label="Address Line1" defaultValue="" style={{ width: 500 }} value={address1} error={address1Error} helperText={address1Error ? "Enter a valid address" : ""}
                onChange={e => { setaddress1(e.target.value); setaddress1Error(false) }}
              />
              <TextField required id="standard-required" label="Address Line2" defaultValue="" style={{ width: 500 }} value={address2} error={address2Error} helperText={address2Error ? "Enter a valid address" : ""}
                onChange={e => { setaddress2(e.target.value); setaddress2Error(false) }}
              />

            </div>
            <br /><br />
            <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={handleClick}>Submit</Button>
          </form>
          <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Details submitted successfully !
            </Alert>
          </Snackbar>
          <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              Please fill all the mandatory fields.
            </Alert>
          </Snackbar>

          <Snackbar open={validationError} autoHideDuration={7000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              {custValidationMsg}
            </Alert>
          </Snackbar>
        </div>
      </div>
    </div>
  );
}
export default App;