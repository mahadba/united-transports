import React, { useState, useEffect } from 'react';
import "react-table-6/react-table.css";
import "./styles.css";
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
//import { withStyles, makeStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Snackbar from '@material-ui/core/Snackbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import * as myConstClass from './Constants.js';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment'
import Axios from 'axios';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';



const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(2),
    width: '25ch',
  },

}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

// const defaultCustomerProps = {
//   options: customerDetails,
//   getOptionLabel: (option) => option.customer
// };

function App() {
  const [ownVehicleID, setownVehicleID] = React.useState("");
  const [vehicleList, setvehicleList] = useState([{ radioValue: "Own Vehicle", vehicleOwnedBy: true, vehicleNumber: "", ownVehicleIDError: false, vehicleType: "", vehicleCompany: "", driverName: "", driverId: "", driverError: false, driverPhoneNumber: "", driverSalary: "", driverAdvance: "", driverAdvanceAmountError: false, thirdPartyID: "", thirdPartyIDError: false, thirdPartyCompany: "", thirdPartyCompanyError: false, thirdPartyVehicleType: "Other" }]);

  const [resetownVehicleID, setresetownVehicleID] = React.useState("ownVehicle");
  const [ownVehicleIDError, setownVehicleIDError] = React.useState(false);
  const [fromLocation, setfromLocation] = React.useState("");
  const [resetfromLocation, setresetfromLocation] = React.useState("fromLocation");
  const [fromLocationError, setfromLocationError] = React.useState(false);
  const [thirdPartyVehicleIDError, setthirdPartyVehicleIDError] = React.useState(false);
  const [customerAdvanceError, setcustomerAdvanceError] = React.useState(false);
  const [toLocation, settoLocation] = React.useState("");
  const [resettoLocation, setresettoLocation] = React.useState("toLocation");
  const [toLocationError, settoLocationError] = React.useState(false);
  const [driverAdvanceAmount, setdriverAdvanceAmount] = React.useState("");
  const [driverAdvanceAmountError, setdriverAdvanceAmountError] = React.useState(false);
  const [delivery, setDelivery] = React.useState("");
  const [deliveryError, setDeliveryError] = React.useState(false);
  const [customer, setcustomer] = React.useState("");
  const [resetcustomer, setresetcustomer] = React.useState("customer");
  const [customerError, setcustomerError] = React.useState(false);
  const [driver, setDriver] = React.useState("");
  const [resetDriver, setresetDriver] = React.useState("driver");
  const [driverError, setDriverError] = React.useState(false);
  const [thirdPartyID, setthirdPartyID] = React.useState("");
  const [resetthirdPartyID, setresetthirdPartyID] = React.useState("thirdParty");

  const [driverDetails, setdriverDetails] = React.useState("");
  const [vehicleDetails, setvehicleDetails] = React.useState("");
  const [locationDetails, setlocationDetails] = React.useState("");
  const [customerDetails, setcustomerDetails] = React.useState("");
  const [driverId, setdriverId] = React.useState("");
  const [customerId, setcustomerId] = React.useState("");
  const [locationId, setlocationId] = React.useState("");

  const [mylocationDetails, setmylocationDetails] = React.useState("");
  const [invoiceAmount, setinvoiceAmount] = React.useState("");
  const [customerAdvance, setcustomerAdvance] = React.useState("");
  const [infoMessage, setinfoMessage] = React.useState("");
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [infoopen, setinfoopen] = React.useState(false);
  const [pickupDate, setpickupDate] = React.useState(moment());
  const [errorMsg, seterrorMsg] = React.useState("");

  const CUSTOMER_URL = myConstClass.GET_CUSTOMER_DETAILS;
  const LOCATION_URL = myConstClass.GET_LOCATION_DETAILS;
  const VECHICLE_URL = myConstClass.GET_VEHICLES_DETAILS;
  const DRIVER_URL = myConstClass.GET_DRIVER_DETAILS;
  const GET_ACTIVE_CUSTOMER_DETAILS = myConstClass.GET_ACTIVE_CUSTOMER_DETAILS;

  const OPEN_NEW_BOOKING = myConstClass.OPEN_NEW_BOOKING;
  const LOC_DET_BY_CUST = myConstClass.GET_LOC_DET_BY_CUST_ID

  const [sourceLocation, setSourceLocation] = useState([]);
  const [destinationLocation, setDestinationLocation] = useState([]);


  useEffect(() => {
    loadCustomerData();
    loadLocationData();
    loadVehicleData();
    loadDriverData();
  }, []);

  const loadCustomerData = async () => {
    const response = await fetch(GET_ACTIVE_CUSTOMER_DETAILS);
    const data = await response.json();
    setcustomerDetails(data);
    console.log(data);
  }
  const loadLocationData = async () => {
    const response = await fetch(LOCATION_URL);
    const data = await response.json();
    setlocationDetails(data);
    console.log(data);
  }
  const loadVehicleData = async () => {
    const response = await fetch(VECHICLE_URL);
    const data = await response.json();
    setvehicleDetails(data);
    console.log(data);
  }
  const loadDriverData = async () => {
    const response = await fetch(DRIVER_URL);
    const data = await response.json();
    setdriverDetails(data);
    console.log(data);
  }

  // const handleRadioChange = (event,i) => {
  //   setradioValue(event.target.value);
  //   if (event.target.value === "Own Vehicle") {
  //     setshowOwnVehicle(true);

  //   }
  //   else {
  //     setshowOwnVehicle(false);
  //   }
  //   clearAll();
  // };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...vehicleList];
    if(value>0)
    list[index][e.target.id] = value;
    console.log(e.target.name + value);
    if (e.target.name === "radioValue") {
      list[index][name] = value;
      if (e.target.value === "Own Vehicle"){
        list[index]["vehicleOwnedBy"] = true;
        list[index]["vehicleType"] = "";
        list[index]["vehicleCompany"] = "";
        list[index]["invoiceAmount"]="";
        list[index]["driverPhoneNumber"]="";
        list[index]["driverSalary"] = "";
        list[index]["driverAdvance"] = "";
      }
      else
        list[index]["vehicleOwnedBy"] = false;
        list[index]["thirdPartyID"] = "";
        list[index]["thirdPartyCompany"]= "";
        list[index]["invoiceAmount"] = "";
    }
    else if (e.target.name = "thridPartyID") {

    }
    setvehicleList(list);
  };
  const handleOwnVehicleIDChange = (e, i) => {
    const { name, value } = e.target;
    const list = [...vehicleList];
    list[i][e.target.id] = value;
    var vn = value;
    var it = vehicleDetails.find(item => {
      return item.vehicleNumber === vn
    });

    console.log("vehicleType : " + list[i]["vehicleType"]);

    if (it) {
      console.log("it.vehicleType : " + it.vehicleType);
      list[i]["vehicleType"] = it.vehicleType;
      list[i]["vehicleCompany"] = it.vehicleCompany;
      list[i]["ownVehicleIDError"] = false;


      list[i]["driverName"] = "";
      list[i]["driverId"] = "";
      list[i]["driverError"] = false;
      list[i]["driverPhoneNumber"] = "";
      list[i]["driverSalary"] = "";
      list[i]["driverAdvance"] = "";
      list[i]["resetDriver"] = moment();

      var locDet = locationDetails.filter(item => {
        return item.customerName === customer
      });
      for (var j = 0; j < locDet.length; j++) {
        if ((locDet[j].sourceLocation === fromLocation) && (locDet[j].destinationLocation === toLocation.destinationLocation && locDet[j].vehicleType === list[i]["vehicleType"])) {
          console.log("------------customerInvoiceAmount : " + locDet[j].customerInvoiceAmount);
          list[i]["invoiceAmount"] = locDet[j].customerInvoiceAmount;

        }
      }


      setvehicleList(list);
    }
  };


  const handleDriverChange = (e, i) => {
    const { name, value } = e.target;
    const list = [...vehicleList];
    list[i][e.target.id] = value;
    var vn = value;
    var it = driverDetails.find(item => {
      return item.firstName === vn
    });


    if (it) {
      setdriverId(it.driverId);
      console.log(it.driverId);
      list[i]["driverPhoneNumber"] = it.phoneNumber;
      list[i]["driverError"] = false;
      list[i]["driverId"] = it.driverId;

      console.log("----------------------------------");
      console.log("type : " + list[i]["vehicleType"]);
      console.log("fromLocation : " + fromLocation);
      console.log("toLocation : " + toLocation.destinationLocation);
      console.log("customerId : " + customerId);
      console.log("----------------------------------");

      setvehicleList(list);
    }
    var it = locationDetails.filter(item => {
      return item.customerName === customer
    });

    for (var j = 0; j < it.length; j++) {
      if ((it[j].sourceLocation === fromLocation) && (it[j].destinationLocation === toLocation.destinationLocation && it[j].vehicleType === list[i]["vehicleType"])) {


        console.log("it[j].sourceLocation : " + it[j].sourceLocation);
        console.log("it[j].destinationLocation :" + it[j].destinationLocation);
        console.log("it[j].vehicleType : " + it[j].vehicleType);
        console.log("fromLocation : " + fromLocation);
        console.log("toLocation : " + toLocation.destinationLocation);
        console.log("list[i][vehicleType] : " + list[i]["vehicleType"]);

        console.log("list[i][driverSalary] : " + list[i]["driverSalary"]);
        console.log("list[j][driverSalary] : " + it[j].driverSalary);
        console.log("------------customerInvoiceAmount : " + it[j].customerInvoiceAmount);

        (list[i]["driverSalary"] = it[j].driverSalary);

      }
    }
  };
  const handleFromLocationChange = (event, newvalue) => {

    console.log("newvalue : " + newvalue);
    console.log("mylocationDetails : " + mylocationDetails);
    if (newvalue) {
      setfromLocation(newvalue);
    }

    if(mylocationDetails)
        {
          setfromLocation(newvalue);
          var vn = newvalue;
          setfromLocationError(false);
        var i = mylocationDetails.filter(item => {
          return item.sourceLocation === vn
        });
        console.log(i);
        setDestinationLocation(i);
      } 
    else {
      setinfoMessage("Please select customer!");
      setinfoopen(true);
      var vtime = moment().format("hhmmss");
      setresetfromLocation("resettoLocation" + vtime);
      settoLocation("");
    }
  }

  const handlepickupDate = (date) => {
    console.log("Date : " + date);

    setpickupDate(date);
  };

  const handleToLocationChange = (event, newvalue) => {
    console.log("newvalue : " + newvalue);

    if (fromLocation) {
      settoLocation(newvalue);
      var vn = customer;
      var i = locationDetails.filter(item => {
        return (item.customerName === vn)
      });

      for (var j = 0; j < i.length; j++) {
        if ((i[j].sourceLocation === fromLocation) && (i[j].destinationLocation === newvalue.destinationLocation)) {
          //   setinvoiceAmount(i[j].customerInvoiceAmount);
          setlocationId(i[j].locationId);
          console.log(i[j].locationId)
          setcustomerId(i[j].customerId);
          console.log(i[j].customerId);
        }
      }
      settoLocationError(false);
    }
    else {
      setinfoMessage("Please select source location before selecting destination!");
      setinfoopen(true);
      var vtime = moment().format("hhmmss");
      setresettoLocation("resettoLocation" + vtime);
      settoLocation("");
    }
  };
  const handledeliveryChange = (event) => {
    setDelivery(event.target.value);
    setDeliveryError(false);

  };
  const handleCustomerChange = (event, newvalue) => {
    setcustomer(newvalue.customerName);
    var vn = newvalue.customerName;
    
    setSourceLocation([]);
    setDestinationLocation([]);

    var i = locationDetails.filter(item => {
      return item.customerName === vn
    });

    var cust = customerDetails.find(item => {
      return item.customerName === vn
    })
    console.log(cust);
    if (cust) {

      console.log("cust.customerId : " + cust.customerId);

      loadLocData(cust.customerId);
      setcustomerId(cust.customerId);

    }
    setmylocationDetails(i);
    setcustomerError(false);
  };

  const loadLocData = async (newvalue) => {
    //console.log("customerId : " + customerId);
    console.log("newvalue : " + newvalue);
    console.log("customerId : " + LOC_DET_BY_CUST + newvalue);
    const response = await fetch(LOC_DET_BY_CUST + newvalue);
    const data = await response.json();
    setresetfromLocation(moment());
    setresettoLocation(moment());

    //setLocationDetails(data);
    setSourceLocation(data.sourceLocation);
    setDestinationLocation(data.destinationLocation);
    console.log(data);
  }
  function clearAll() {
    var vtime = moment().format("hhmmss");
    console.log(vtime);
    setresetownVehicleID("resetownVehicle" + vtime);
    setresetDriver("resetDriver" + vtime);

    setresetthirdPartyID("resetthirdParty" + vtime);

    setDelivery("");
    setresetcustomer("resetCustomer" + vtime);
    setcustomer("");
    setresetfromLocation("resetfromLocation" + vtime);
    setfromLocation("");
    setresettoLocation("resettoLocation" + vtime);
    settoLocation("");
    setinvoiceAmount("");
    setvehicleList([{ radioValue: "Own Vehicle", vehicleOwnedBy: true, vehicleNumber: "", ownVehicleIDError: false, vehicleType: "", vehicleCompany: "", driverName: "", driverId: "", driverError: false, driverPhoneNumber: "", driverSalary: "", driverAdvance: "", driverAdvanceAmountError: false, thirdPartyID: "", thirdPartyIDError: false, thirdPartyCompany: "", thirdPartyCompanyError: false, thirdPartyVehicleType: "Other" }]);

  }
  const handleClick = (e) => {
    var eflag = false;
    const list = [...vehicleList];
    for (var i = 0; i < list.length; i++) {
      console.log(i + "  " + list[i]["vehicleOwnedBy"]);
      if (vehicleList[i]["vehicleOwnedBy"]) {
        if (vehicleList[i]["vehicleNumber"] === "") {
          list[i]["ownVehicleIDError"] = true;
          eflag = true;
        }
        if (vehicleList[i]["driverName"] === "") {
          list[i]["driverError"] = true;
          eflag = true;
        }
        if (vehicleList[i]["driverAdvance"] === "") {
          list[i]["driverAdvanceAmountError"] = true;
          eflag = true;
        }
      }
      else {
        if (vehicleList[i]["thirdPartyID"] === "") {
          list[i]["thirdPartyIDError"] = true;
          console.log("id mama");
          eflag = true;
          list[i]["vehicleNumber"] = vehicleList[i]["thirdPartyID"];
        }
        if (vehicleList[i]["thirdPartyCompany"] === "") {
          list[i]["thirdPartyCompanyError"] = true;
          console.log("company   mama");
          list[i]["vehicleCompany"] = vehicleList[i]["thirdPartyCompany"];
          eflag = true;
        }
      }
    }
    setvehicleList(list);
    if (delivery === "") {
      setDeliveryError(true);
      eflag = true;
    }
    if (fromLocation === "") {
      setfromLocationError(true);
      eflag = true;
    }
    if (toLocation === "") {
      settoLocationError(true);
      eflag = true;
    }
    if (customer === "") {
      setcustomerError(true);
      eflag = true;
    }
    e.preventDefault();
    if (eflag === true) {
      setSuccessOpen(false);
      seterrorMsg("Please fill all mandatory fields!!")
      setErrorOpen(true);

    }
    else {
      setErrorOpen(false);

      const params = {
        "pickUpDate": pickupDate,
        "importOrExport": delivery,
        "customerId": customerId,
        "customerName": customer,
        "loadingPoint": fromLocation,
        "destination": toLocation.destinationLocation,
        "billAmount": invoiceAmount,
        "locationId": locationId,
        "customerAdvance": customerAdvance,
        "bookingOpenedBy": localStorage.getItem("userName"),
        "vehicleDriverList": vehicleList
      }



      Axios.post(OPEN_NEW_BOOKING, params)
        .then(response => {
          console.log("validated Axios");

          console.log("response.data.validationStatus : " + response.data.validationStatus);
          console.log("response.data.validationMessage : " + response.data.validationMessage);
          if (response.data.validationStatus) {
            setSuccessOpen(true);
            clearAll();
          } else {
            setErrorOpen(true);
          }
        })
        .catch(error => {
          console.log("Error Axios");
          console.log(error);
          seterrorMsg(error.message);
          setErrorOpen(true);

        });

    }
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    setErrorOpen(false);
    setinfoopen(false);
  };

  const handleAddClick = () => {
    setvehicleList([...vehicleList, { radioValue: "Own Vehicle", vehicleOwnedBy: true, vehicleNumber: "", ownVehicleIDError: false, vehicleType: "", vehicleCompany: "", driverName: "", driverId: "", driverError: false, driverPhoneNumber: "", driverSalary: "", driverAdvance: "", driverAdvanceAmountError: false, thirdPartyID: "", thirdPartyIDError: false, thirdPartyCompany: "", thirdPartyCompanyError: false, thirdPartyVehicleType: "Other" }]);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...vehicleList];
    list.splice(index, 1);
    setvehicleList(list);
  };

  const classes = useStyles();

  return (
    <div id="App" className="appContainer">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <h2>New Booking</h2>
        <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }}  >
          <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                //disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Pickup Date"
                value={pickupDate}
                autoOk
                onChange={handlepickupDate}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <FormControl className={classes.formControl} error={deliveryError}>
              <InputLabel id="select company label">Delivery Type</InputLabel>
              <Select
                className="mdb-select"
                labelId="select delivery"
                id="select delivery"
                label="Delivery"
                value={delivery}
                onChange={handledeliveryChange}
              >
                <MenuItem value={"EXPORT"}>EXPORT</MenuItem>
                <MenuItem value={"IMPORT"}>IMPORT</MenuItem>
              </Select>
              <FormHelperText>{deliveryError ? "Select a delivery type" : ""}</FormHelperText>
            </FormControl>
            <Autocomplete
              options={customerDetails}
              getOptionLabel={option => option.customerName}
              key={resetcustomer}
              id="customer"
              clearOnEscape
              Select={customer}
              onChange={handleCustomerChange}
              renderInput={(params) => <TextField {...params} label="Customer" margin="normal" error={customerError} helperText={customerError ? "Select a customer" : ""} />}
            />
            <Autocomplete
              // options={mylocationDetails ? mylocationDetails : locationDetails}
              //  getOptionLabel={option => option.sourceLocation}
              options={sourceLocation.map((option) => option)}
              key={resetfromLocation}
              id="fromLocation"
              clearOnEscape
              Select={fromLocation}
              onChange={handleFromLocationChange}
              renderInput={(params) => <TextField {...params} label="From Location" margin="normal" error={fromLocationError} helperText={fromLocationError ? "Select a source location" : ""} />}
            />
            <Autocomplete
              //options={mylocationDetails ? mylocationDetails : locationDetails}
              getOptionLabel={option => option.destinationLocation}
              options={destinationLocation.map((option) => option)}
              key={resettoLocation}
              id="toLocation"
              clearOnEscape
              Select={toLocation}
              onChange={handleToLocationChange}
              renderInput={(params) => <TextField {...params} label="To Location" margin="normal" error={toLocationError} helperText={toLocationError ? "Select a destination location" : ""} />}
            />



          </div>
          <br />
          {vehicleList.map((x, i) => {
            return (
              <div >
                <div align="left" style={{ marginLeft: "4em", display: "flex", flexWrap: "wrap" }} >
                  <RadioGroup aria-label="quiz" name="radioValue" id="raidoValue" value={x.radioValue} onChange={e => handleInputChange(e, i)} row>
                    <FormControlLabel value="Own Vehicle" control={<Radio color="default" />} label="Own" />
                    <FormControlLabel value="Third Party Vehicle" control={<Radio color="default" />} label="Outside" />
                  </RadioGroup>
                </div>

                {x.vehicleOwnedBy ? (
                  <div align="left" style={{ marginLeft: "4em", display: 'flex', flexWrap: "wrap" }}>
                    <Autocomplete
                      options={vehicleDetails}
                      getOptionLabel={option => option.vehicleNumber}
                      key={resetownVehicleID}
                      id="vehicleNumber"
                      name="vehicleNumber"
                      clearOnEscape
                      Select={x.vehicleNumber}
                      onSelect={e => handleOwnVehicleIDChange(e, i)}
                      renderInput={(params) => <TextField {...params} style={{ width: 180 }} label="Vehicle ID" margin="normal" error={x.ownVehicleIDError} helperText={x.ownVehicleIDError ? "Select a Vehicle ID" : ""} />}
                    />
                    <TextField
                      id="ownVehicleType"
                      label="Type"
                      value={x.vehicleType}
                      name="Type"
                      InputProps={{ readOnly: true, }}
                      style={{ width: 120 }}
                    />
                    <TextField
                      id="vehicleCompany"
                      label="vehicleCompany"
                      name="vehicleCompany"
                      value={x.vehicleCompany}
                      InputProps={{ readOnly: true, }}
                    />

                    <TextField
                      id="Invoice Amount"
                      label="Invoice Amount"
                      value={x.invoiceAmount||""}
                      name="invoiceAmount"

                      InputProps={{ readOnly: true, }}
                      style={{ width: 120 }}
                    />

                    <Autocomplete
                      options={driverDetails}
                      getOptionLabel={option => option.firstName}
                      key={x.resetDriver}
                      id="driverName"
                      name="driverName"
                      clearOnEscape
                      Select={x.driverName}
                      onSelect={e => handleDriverChange(e, i)}
                      renderInput={(params) => <TextField {...params} label="Driver" margin="normal" error={x.driverError} helperText={x.driverError ? "Select a driver" : ""} />}
                    />
                    <TextField
                      id="driverPhoneNumber"
                      label="Phone Number"
                      name="driverPhoneNumber"
                      value={x.driverPhoneNumber}
                      style={{ width: 120 }}
                      InputProps={{ readOnly: true, }}
                    />

                    <TextField
                      id="driverSalary"
                      label="driverSalary"
                      name="driverSalary"
                      value={x.driverSalary}
                      style={{ width: 100 }}
                      InputProps={{ readOnly: true, }}
                    />
                    <TextField
                      id="driverAdvance"
                      label="Advance"
                      name="driverAdvance"
                      style={{ width: 100 }}
					  InputProps = {{ inputProps: { max: 10 , min: 0} }}
                      type="number" error={x.driverAdvanceAmountError} helperText={x.driverAdvanceAmountError ? "Enter a valid advance amount" : ""}
                      value={x.driverAdvance}
                      onChange={e => handleInputChange(e, i)}
                    />



                  </div>) : (

                  <div align="left" style={{ marginLeft: "4em", display: "flex", flexWrap: "wrap" }}>
                    <TextField
                      id="thirdPartyID"
                      label="Outside Vehicle Number"
                      name="thirdPartyID"
                      value={x.thirdPartyID}
                      onChange={e => handleInputChange(e, i)}
                      InputProps={{ readOnly: false, }}
                      error={x.thirdPartyIDError} helperText={x.thirdPartyIDError ? "Enter a valid vehicle Number" : ""}
                    />
                    <TextField
                      id="ThridPartyVehicleType"
                      label="Type"
                      name="thirdPartyVehicleType"
                      value={x.thirdPartyVehicleType}
                      onChange={e => handleInputChange(e, i)}
                      InputProps={{ readOnly: true, }}

                    />
                    <TextField
                      id="thirdPartyCompany"
                      label="company"
                      name="thirdPartyCompany"
                      value={x.thirdPartyCompany}
                      onChange={e => handleInputChange(e, i)}
                      InputProps={{ readOnly: false, }}
                      error={x.thirdPartyCompanyError} helperText={x.thirdPartyCompanyError ? "Enter a valid advance amount" : ""}
                    />
                    <TextField
                      id="invoiceAmount"
                      label="Invoice Amount"
                      value={x.invoiceAmount}
                      name="invoiceAmount"
                      type = "number"
                      onChange={e=>handleInputChange(e,i)}
                      style={{ width: 120 }}
                    />
                  </div>)}
                <br />
                <div align="left" style={{ marginLeft: "4em", display: "flex", flexWrap: "wrap" }}>
                  {vehicleList.length - 1 === i && <Button variant="contained" color="primary" allign="center" style={{ height: 30, fontSize: "90%", opacity: "1" }} onClick={handleAddClick}>Add</Button>}
                  <br /><br /> &nbsp;&nbsp;
                  {vehicleList.length !== 1 && <Button variant="contained" color="primary" allign="center" style={{ height: 30, fontSize: "90%", opacity: "1" }} onClick={() => handleRemoveClick(i)}>Delete</Button>}
                  <br />
                </div>
              </div>
            );
          })}
          <br /><br />
          <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={handleClick}>Submit</Button>
        </form>
      </div>
      <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Booking created successfully !
        </Alert>
      </Snackbar>
      <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errorMsg}
        </Alert>
      </Snackbar>
      <Snackbar open={infoopen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="info">
          {infoMessage}
        </Alert>
      </Snackbar>
    </div>
  );
}
export default App;