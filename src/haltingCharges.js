import React, { useState, useEffect } from 'react';
import "./styles.css";
import SideBar from "./sidebar";
import "react-table-6/react-table.css";
import ReactTable from 'react-table-6';
import { makeStyles } from '@material-ui/core/styles';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import moment from 'moment';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import * as myConstClass from './Constants.js';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(2),
    width: '25ch',
  },

}));



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

function App() {
	let history = useHistory();
  const classes = useStyles();
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [driver, setdriver] = React.useState("");
  const [driverId, setDriverId] = React.useState("");
  const [resetDriver, setresetDriver] = React.useState("");
  const [driverError, setdriverError] = React.useState(false);
  const [type, settype] = React.useState("");
  const [typeError, settypeError] = React.useState(false);
  const [fromDate, setfromDate] = React.useState(moment());
  const [toDate, settoDate] = React.useState(moment());
  const [amount, setamount] = React.useState("");
  const [comment, setcomment] = React.useState("");
  const [amountError, setamountError] = React.useState(false);
  const [phoneNumber, setphoneNumber] = React.useState("");
  const [successMsg, setsuccessMsg] = React.useState("");
  const [amountType, setamountType] = React.useState("CREDIT");
  const [deleteOpen, setdeleteOpen] = React.useState('');
  const [currentID, setcurrentID] = React.useState('');
  const [editMode, seteditMode] = React.useState(false);
  // const [driverDetails, setdriverDetails] = React.useState('');
  const [haltingChargeDetails, setHaltingChargeDetails] = React.useState([]);
  const [driverDetails, setDriverDetails] = React.useState([]);
  const [custValidationStatus, setCustValidationStatus] = React.useState(false);
  const [custValidationMsg, setCustValidationMsg] = React.useState("");
  const [validationError, setValidationError] = React.useState(false);
  const [editOpen, setEditOpen] = React.useState(false);

  const GET_HALTING_DETAILS = myConstClass.GET_HALTING_DETAILS;
  const GET_DRIVER_DETAILS = myConstClass.GET_DRIVER_DETAILS;
  const ADD_HALTING_DETAILS = myConstClass.ADD_HALTING_DETAILS;
  const DELETE_HALTING_DETAILS = myConstClass.DELETE_HALTING_DETAILS;
  const UPDATE_HALTING_DETAILS = myConstClass.UPDATE_HALTING_DETAILS;



  useEffect(() => {
	 loadDataForAdmin();
    
  }, []);


const loadDataForAdmin = async () => {
		
	if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				loadHaltingData();
				loadDriverData();
	  } else {
		  history.push('/home');
	  }
	  
	}
	
  const loadHaltingData = async () => {
    // setValidationError(false);
    const response = await fetch(GET_HALTING_DETAILS);
    const data = await response.json();
    setHaltingChargeDetails(data);


    //  console.log(data);
  }

  const loadDriverData = async () => {
    const driverResponse = await fetch(GET_DRIVER_DETAILS);
    const driverData = await driverResponse.json();
    setDriverDetails(driverData);
    //  console.log(data);
  }


  const defaultDriverProps = {
    options: driverDetails,
    getOptionLabel: (option) => option.firstName
  };

  const handleEdit = (event) => {
    event.preventDefault();
    setSuccessOpen(false);
    setEditOpen(true);
    setDialogOpen(true);


    if (event.target.id === "") {
      setcurrentID(event.target.parentNode.id);
      var vn = event.target.parentNode.id;
    }
    else {
      setcurrentID(event.target.id);
      var vn = event.target.id;
    }

    console.log("Vn Value : " + vn);


    var i = haltingChargeDetails.find(item => {
      return item.haltingId === vn
    })

    if (i) {
      setdriver(i.driverName);
      setphoneNumber(i.driverPhoneNumber);
      setfromDate(moment(i.haltingChargedDate, "YYYY-MM-DD").format("MM/DD/YYYY"));
      //  settoDate(moment(i.toDate, "DD/MM/YYYY"));
      setamount(i.haltingCharges);
      settype(i.typeOfHalt);
      setamountType(i.creditOrDebit);
      setcomment(i.haltingComments);
    }
  };
  const handleDialogClose = () => {
    setDialogOpen(false);
    setdriver("");
    setphoneNumber("");
    settype("");
    setfromDate(moment());
    settoDate(moment());
    setamount("");
    setamountType("CREDIT");
    setcomment("");
    setcurrentID("");
    setdriverError(false);
    settypeError(false);
    setamountError(false);
    setEditOpen(false);
  };
  const addHaltingCharges = (event) => {
    setEditOpen(false);
    setSuccessOpen(false);
    event.preventDefault();
    setDialogOpen(true);
  };
  const onDriverChange = (event, newvalue) => {

    //		console.log("hjhkjhj : "+newvalue);
    //  console.log("driverId : "+newvalue.driverId);
    if (newvalue) {
      console.log("driverId : " + newvalue.driverId);
      setdriver(newvalue.firstName);
      setDriverId(newvalue.driverId);
      var vn = newvalue.driverId;
      setdriverError(false);
      var i = driverDetails.find(item => {
        return item.driverId === vn
      })
      if (i) {
        setphoneNumber(i.phoneNumber);
      }
    }
  };
  const handleFromDate = (date) => {
    setfromDate(date);
  };
  const handleToDate = (date) => {
    settoDate(date);
  };
  const handleTypeChange = (event) => {
    settype(event.target.value);
    settypeError(false);
  };
  const handleAmountTypeChange = (event) => {
    setamountType(event.target.value);

  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    setErrorOpen(false);
  };
  const handleDeleteClose = () => {
    setdeleteOpen(false);
    setcurrentID("");
  };
  const handleDelete = (event) => {


    if (event.target.id === "") {
      setcurrentID(event.target.parentNode.id);
    }
    else {
      setcurrentID(event.target.id);
    }


    setdeleteOpen(true);
  };
  const handleDeleteConfirm = (event) => {

    console.log("delete : " + currentID);

    const params = {
      "haltingId": currentID
    }

    Axios.post(DELETE_HALTING_DETAILS, params)
      .then(response => {
        console.log("validated Axios");
        setCustValidationStatus(response.data.validationStatus);
        setCustValidationMsg(response.data.validationMessage);


        if (response.data.validationStatus) {

          loadHaltingData();

          setdeleteOpen(false);
          setsuccessMsg("Entry deleted successfully!");
          setSuccessOpen(true);
        } else {
          setdeleteOpen(true);
          //setsuccessMsg("Entry deleted successfully!");
          setSuccessOpen(false);
        }

      })
      .catch(error => {
        console.log("Error Axios");
        console.log(error);
        setdeleteOpen(true);
        //setsuccessMsg("Entry deleted successfully!");
        setSuccessOpen(false);
      });




  };



  const handleUpdate = (event) => {

    var eflag = false;
    event.preventDefault();
    if (driver === "") {
      eflag = true;
      setdriverError(true);
    }
    if (type === "") {
      eflag = true;
      settypeError(true);
    }
    if (amount === "") {
      eflag = true;
      setamountError(true);
    }
    if (eflag) {
      setErrorOpen(true);
      setSuccessOpen(false);
    }
    else {

      var i = haltingChargeDetails.find(item => {
        return item.haltingId === currentID
      })

      var driID = ""

      if (i) {
        //setDriverId();
        driID = i.driverId;
      }

      const params = {
        "haltingId": currentID,
        "driverId": driID,
        "driverName": driver,
        "driverPhoneNumber": phoneNumber,
        "haltingCharges": amount,
        "haltingComments": comment,
        "typeOfHalt": type,
        "haltDate": fromDate,
        "creditOrDebit": amountType
      }

      Axios.post(UPDATE_HALTING_DETAILS, params)
        .then(response => {
          console.log("validated Axios");
          setCustValidationStatus(response.data.validationStatus);
          setCustValidationMsg(response.data.validationMessage);


          if (response.data.validationStatus) {

            loadHaltingData();

            setErrorOpen(false);
            setsuccessMsg("Halting charges added successfully!");
            setSuccessOpen(true);
            handleDialogClose();
          } else {
            setErrorOpen(false);
            setSuccessOpen(false);
            setValidationError(true);
          }


        })
        .catch(error => {
          console.log("Error Axios");
          console.log(error);

        });



    }
  }

  const handleAdd = (event) => {
    var eflag = false;
    event.preventDefault();
    if (driver === "") {
      eflag = true;
      setdriverError(true);
    }
    if (type === "") {
      eflag = true;
      settypeError(true);
    }
    if (amount === "") {
      eflag = true;
      setamountError(true);
    }
    if (eflag) {
      setErrorOpen(true);
      setSuccessOpen(false);
    }
    else {



      const params = {
        "driverId": driverId,
        "driverName": driver,
        "driverPhoneNumber": phoneNumber,
        "haltingCharges": amount,
        "haltingComments": comment,
        "typeOfHalt": type,
        "haltDate": fromDate,
        "creditOrDebit": amountType
      }

      Axios.post(ADD_HALTING_DETAILS, params)
        .then(response => {
          console.log("validated Axios");
          setCustValidationStatus(response.data.validationStatus);
          setCustValidationMsg(response.data.validationMessage);


          if (response.data.validationStatus) {

            loadHaltingData();


            setErrorOpen(false);
            setsuccessMsg("Halting charges added successfully!");
            setSuccessOpen(true);
            handleDialogClose();
          } else {
            setErrorOpen(false);
            setSuccessOpen(false);
            setValidationError(true);
          }


        })
        .catch(error => {
          console.log("Error Axios");
          console.log(error);

        });



    }
  };
  const columns = [

    {
      Header: 'Name',
      accessor: 'driverName',
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },  // String-based value accessors!

    {
      Header: 'Phone Number',
      accessor: 'driverPhoneNumber',
      //width:'10%',
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },

    {
      id: "Date",
      Header: "Date", // Custom header components!
      //width:'30%',
      accessor: "haltingChargedDate",
      Filter: ({ filter, onChange }) => (
        <input width="48"
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Credit/Debit",
      accessor: "creditOrDebit"

    },
    {
      Header: 'Type',
      accessor: "typeOfHalt",
      //width:"10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Comments",
      accessor: "haltingComments"
    },
    {
      Header: "Amount", // Custom header components!
      accessor: "haltingCharges",
      //width:"30%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      accessor: 'id',
      width: 78,
      //  Cell: props => (<span><Edit id={props.value} style={{ color: "#000033", cursor: "pointer" }} onClick={handleEdit} onMouseOver={handleEditEnter} onMouseOut={handleLeave} />&nbsp;&nbsp;&nbsp;&nbsp;<Delete id={props.value} style={{ color: "#000033", cursor: "pointer" }} onClick={handleDelete} onMouseOver={handleEnter} onMouseOut={handleLeave} /></span>),
      //  Filter: ({ filter, onChange }) => (null)


      Cell: ({ row, original }) => (<span><Edit id={original.haltingId} style={{ color: "#000033", cursor: "pointer" }} onClick={handleEdit} onMouseOver={handleEditEnter} onMouseOut={handleLeave} />&nbsp;&nbsp;&nbsp;&nbsp;
        <Delete id={original.haltingId} style={{ color: "#000033", cursor: "pointer" }} onClick={handleDelete} onMouseOver={handleEnter} onMouseOut={handleLeave} /></span>),
      Filter: ({ filter, onChange }) => (null)
    }
  ];

  const handleEnter = (e) => {
    e.target.style.color = "red";
  }
  const handleEditEnter = (e) => {
    e.target.style.color = "green";
  }
  const handleLeave = (e) => {
    e.target.style.color = "#000033";
  }
  return (
    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <h2>Halting Charges</h2>
        <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={addHaltingCharges}>Add Halting Charges</Button>
        <br /><br />
        <form className={classes.root} noValidate autoComplete="new-password" style={{ display: 'block' }}  >
          <ReactTable
            data={haltingChargeDetails}
            columns={columns}
            minRows={1}
            defaultPageSize={10}
            filterable
            filterOptions={(options, state) => options}
            autoComplete="none"
            sorted={[
              {
                id: "id",
                asc: true
              }
            ]}
            defaultFilterMethod={(filter, row) =>
              (String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())}
          />
        </form>
      </div>
      <Dialog
        open={dialogOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleDialogClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dial0g-slide-description"
        maxWidth={"xl"}
        disableBackdropClick
      >
        {editOpen ?
          <DialogTitle style={{ textAlign: "center" }} id="alert-dialog-slide-title">{"Edit Halting Charges"}</DialogTitle>
          :
          <DialogTitle style={{ textAlign: "center" }} id="alert-dialog-slide-title">{"Add Halting Charges"}</DialogTitle>
        }
        <DialogContent>
          <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
            <div align="left" style={{ marginLeft: "", display: 'flex', flexWrap: "wrap" }}>
              <Autocomplete
                options={driverDetails}
                getOptionSelected={(option, value) => option.firstName === value.firstName}
                getOptionLabel={option => option.firstName}
                key={resetDriver}
                id="driver"
                clearOnEscape
                disabled={editOpen}
                value={{ firstName: driver || "" }}
                onChange={onDriverChange}
                renderInput={(params) => <TextField {...params} label="Driver" margin="normal" error={driverError} helperText={driverError ? "Select a driver" : ""} />}
              />
              <TextField
                id="phoneNumber"
                label="Phone Number"
                value={phoneNumber}
                InputProps={{ readOnly: true, }}
              />
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  //disableToolbar
                  variant="inline"
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Date"
                  value={fromDate}
                  autoOk
                  onChange={handleFromDate}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
              </MuiPickersUtilsProvider>
              <FormControl className={classes.formControl} error={typeError}  >
                <InputLabel id="select type label">Credit/Debit to driver</InputLabel>
                <Select
                  className="mdb-select"
                  labelId="type"
                  id="Credit/Debit to driver"
                  label="Type"
                  value={amountType}
                  onChange={handleAmountTypeChange}
                >
                  <MenuItem value={"CREDIT"}>CREDIT</MenuItem>
                  <MenuItem value={"DEBIT"}>DEBIT</MenuItem>
                </Select>
                <FormHelperText>{typeError ? "Select valid halting charge type" : ""}</FormHelperText>
              </FormControl>

            </div>
            <div align="left" style={{ marginLeft: "", display: 'flex', flexWrap: "wrap" }}>
              <FormControl className={classes.formControl} error={typeError}  >
                <InputLabel id="select type label">Type</InputLabel>
                <Select
                  className="mdb-select"
                  labelId="type"
                  id="select type"
                  label="Type"
                  value={type}
                  onChange={handleTypeChange}
                >
                  <MenuItem value={"MT"}>MT HALT</MenuItem>
                  <MenuItem value={"LOAD"}>LOAD HALT</MenuItem>
                  <MenuItem value={"SHED"}>SHED HALT</MenuItem>
                  <MenuItem value={"OTHER"}>OTHER</MenuItem>
                </Select>
                <FormHelperText>{typeError ? "Select valid halting charge type" : ""}</FormHelperText>
              </FormControl>
              <TextField
                id="standard-number"
                label="Comments"
                value={comment}
                error={amountError} helperText={amountError ? "Enter a valid comments" : ""}
                onChange={e => { setcomment(e.target.value); setamountError(false); }}
              />
              <TextField
                id="standard-number"
                label="Amount"
                value={amount ||""}
                type="number" error={amountError} helperText={amountError ? "Enter a valid amount" : ""}
                onChange={e => { console.log(e.target.value); if(e.target.value >= 0 ) setamount(e.target.value); setamountError(false); }}
              />
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          {editOpen ?
            <Button onClick={handleUpdate} color="primary">
              Update
            </Button> :
            <Button onClick={handleAdd} color="primary">
              Add
            </Button>
          }

          <Button onClick={handleDialogClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={deleteOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleDeleteClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dial0g-slide-description"
        maxWidth={"xl"}
        disableBackdropClick>
        <DialogTitle style={{ textAlign: "center" }} id="alert-dialog-slide-title">{"DELETE"}
          <DialogContent>
            Are you sure you want to delete this entry?
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDeleteConfirm} color="primary">
              Yes
            </Button>
            <Button onClick={handleDeleteClose} color="primary">
              No
            </Button>
          </DialogActions>
        </DialogTitle>
      </Dialog>
      <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          {successMsg}
        </Alert>
      </Snackbar>
      <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Please fill all the mandatory fields.
        </Alert>
      </Snackbar>
    </div>
  );
}

export default App;



