import React, { Component } from 'react';
import 'react-dropdown/style.css';
import Axios from 'axios';
import ReactTable from 'react-table-6'
import 'react-table-6/react-table.css'
import * as myConstClass from './Constants.js';
import ExportToExcel from './ExportToExcel';

class ViewVehicleDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
			 category: 'vehicleInsurance',
			dataColumn: [],
			vehicleDetails:[],
			banner1Css: { color: "#FFF", backgroundColor: "green" },
			banner2Css: { color: "#000", backgroundColor: "grey", fontFamily: "arial" },
			banner3Css: { color: "#FFF", backgroundColor: "red", fontSize: 20 },
			
        }
		
		this.handleChange = this.handleChange.bind(this);

    }
	
	componentDidMount() {    
        const url = myConstClass.GET_VEHICLES_DETAILS ;
        console.log("url : " + url);
        Axios.get(url)
            .then(vehicleList => {
                console.log("referalList : ", vehicleList);
                console.log("referalList data : ", vehicleList.data);
                this.setState({
                    vehicleDetails: vehicleList.data
                })

            })
            .catch(error => {
                console.log(error);
            });
    }
  
  handleChange(event) {
    this.setState({category: event.target.value});
  }

    render() {
				
					var data = myConstClass.vehicleInsurance ;
					
				 if("vehicleInsurance"===this.state.category){
				 data = myConstClass.vehicleInsurance ;
				 } else if("vehicleRoadTax"===this.state.category){
					 data = myConstClass.vehicleRoadTax ;
				 }else if("vehicleNationalPermitValidity"===this.state.category){
					 data = myConstClass.vehicleNationalPermitValidity ;
				 }else if("vehicleTNPermitValidity"===this.state.category){
					 data = myConstClass.vehicleTNPermitValidity ;
				 }else if("allDetails"===this.state.category){
					 data = myConstClass.allDetails ;
				 }

 

       
        return (
            
                  
        <div>
			<div className='detailsDropdowndiv'>
			  <strong>Select Category : </strong>
				  <select value={this.state.category} onChange={this.handleChange} className='detailsDropdown' style={{backgroundColor:"#8A7F80"}}>
					<option  className='myMenuClassName' value="vehicleInsurance">Insurance</option>
					<option className='myMenuClassName' value="vehicleRoadTax">Road Tax</option>
					<option className='myMenuClassName' value="vehicleNationalPermitValidity">National Permit</option>
					<option className='myMenuClassName' value="vehicleTNPermitValidity">TamilNadu Permit</option>
					<option className='myMenuClassName' value="allDetails">All Details</option>
				 
				  </select>
			</div>
		  
							   <ReactTable
										data={this.state.vehicleDetails}
										columns={data}
										defaultPageSize={10}
										filterable
										defaultFilterMethod={(filter, row) =>
											(String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())}
								>
					

										{(state,filteredData, instance) => {
												this.reactTable = state.pageRows.map(post => {return post._original});
													return (
														<div> 
															{filteredData()}
															<ExportToExcel posts={this.reactTable}/>
														</div>
													)	
										} }
				  
								</ReactTable>	
			  </div>
        )

    }
}

export default ViewVehicleDetails;



