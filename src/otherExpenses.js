import React, { useEffect } from 'react';
import axios from 'axios';
import "./styles.css";
import SideBar from "./sidebar";
//import { haltingChargeDetails, vehicleDetails, otherExpenses } from "./Constants";
import "react-table-6/react-table.css";
import ReactTable from 'react-table-6';
import { makeStyles } from '@material-ui/core/styles';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import moment from 'moment';
import * as myConstClass from './Constants.js';
import { useHistory } from 'react-router-dom';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { setDate } from 'date-fns';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(2),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(2),
    width: '25ch',
  },

}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

function App() {
	let history = useHistory();
  const classes = useStyles();
  const [successopen, setSuccessOpen] = React.useState(false);
  const [erroropen, setErrorOpen] = React.useState(false);
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [vehicleNumber, setvehicleNumber] = React.useState("");
  const [resetVehicleID, setresetvehicleID] = React.useState("");
  const [vehicleIDError, setvehicleIDError] = React.useState(false);
  const [type, settype] = React.useState("");
  const [expenseDate, setexpenseDate] = React.useState(moment());
  const [amount, setamount] = React.useState("");
  const [amountError, setamountError] = React.useState(false);
  const [expenseType, setexpenseType] = React.useState("");
  const [expenseTypeError, setexpenseTypeError] = React.useState(false);
  const [comment, setcomment] = React.useState("");
  const [successMsg, setsuccessMsg] = React.useState("");
  const [errorMsg, seterrorMsg] = React.useState("");
  const [deleteOpen, setdeleteOpen] = React.useState('');
  const [vehicleDetails, setvehicleDetails] = React.useState("");
  const [otherExpenses, setotherExpenses] = React.useState("");
  const [currentID, setcurrentID] = React.useState("");
  const SAVE_EXPENSE_URL = myConstClass.SAVE_OTHEREXPENSE_DETAILS;
  const UPDATE_EXPENSE_URL = myConstClass.UPDATE_OTHEREXPENSE_DETAILS;
  const GET_EXPENSE_URL = myConstClass.GET_OTHEREXPENSE_DETAILS;
  const GET_VEHICLES_DETAILS= myConstClass.GET_VEHICLES_DETAILS;
  const DELETE_EXPENSE_URL = myConstClass.DELETE_OTHEREXPENSE_DETAILS;

  const [addExpense, setaddExpense] = React.useState({
    vehicleNumber: '',
    vehicleType: '',
    expenseDate: '',
    expenseType: '',
    expenseAmount: '',
    expenseComments: ''
  })
  useEffect(() => {
	  
	  loadDataForAdmin();
	  
    
  },[]);
  
  const loadDataForAdmin = async () => {
		
	if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				loadVehicleData();
				loadExpenseData();
	  } else {
		  history.push('/home');
	  }
	  
	}
	
  const loadVehicleData = async () => {
    const response = await fetch(GET_VEHICLES_DETAILS);
    const data = await response.json();	
    setvehicleDetails(data);
    console.log(data);
  }
  const loadExpenseData = async () => {
    const response = await fetch(GET_EXPENSE_URL);
    const data = await response.json();	
    setotherExpenses(data);
    console.log(data);
  }


  const handleEdit = (e) => {
    setSuccessOpen(false);
    e.preventDefault();
    setDialogOpen(true);
    if(e.target.id==="")
    {
    setcurrentID(e.target.parentNode.id);
    var vn = e.target.parentNode.id;
    }
    else
    {
    setcurrentID(e.target.id);
    var vn = e.target.id;
    }
    var i = otherExpenses.find(item => {
      return item.expenseId === vn
    })
    if (i) {
      setcurrentID(i.expenseId);
      setvehicleNumber(i.vehicleNumber);
      settype(i.vehicleType);
      setexpenseDate(moment(i.expenseDate, "DD/MM/YYYY"));
      setamount(i.expenseAmount);
      setexpenseType(i.expenseType);
      setcomment(i.expenseComments);
    }
  };
  const handleDialogClose = () => {
    setDialogOpen(false);
    setexpenseType("");
    settype("");
    setexpenseDate(moment());
    setcomment("");
    setamount("");
    setvehicleNumber("");
    setvehicleIDError(false);
    setexpenseTypeError(false);
    setamountError(false);
    setcurrentID("");
  };
  const addHaltingCharges = (event) => {
    setSuccessOpen(false);
    event.preventDefault();
    setDialogOpen(true);
  };
  const onVehicleChange = (event, newvalue) => {
    setvehicleNumber(newvalue.vehicleNumber);
    var vn = newvalue.vehicleNumber;
    setvehicleIDError(false);
    var i = vehicleDetails.find(item => {
      return item.vehicleNumber === vn
    })
    if (i) {
      settype(i.vehicleType);
    }
  };
  const handleExpenseDate = (date) => {
    setexpenseDate(date);    
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    setErrorOpen(false);
  };

  const handleAdd = (event) => {
    var eflag = false;
    event.preventDefault();
    if (vehicleNumber === "") {
      eflag = true;
      setvehicleIDError(true);
    }
    if (expenseType === "") {
      eflag = true;
      setexpenseTypeError(true);
    }
    if (amount === "") {
      eflag = true;
      setamountError(true);
    }
    if (eflag) {
      seterrorMsg("Please fill all mandatory fields!");
      setErrorOpen(true);
      setSuccessOpen(false);
    }
    else {
      setErrorOpen(false);
      var vurl;
      if(currentID===""){
        vurl = SAVE_EXPENSE_URL;
      }
      else{
        vurl = UPDATE_EXPENSE_URL;
      }
      //setaddExpense({vehicleNumber:vehicleNumber,expenseType:expenseType,expenseAmount:amount,expenseComments:comment,expenseDate:expenseDate,vehicleType:type});
      axios.post(vurl, {
        "vehicleNumber": vehicleNumber,
        "vehicleType": type,
        "expenseDate": moment(expenseDate).format("DD-MM-YYYY"),
        "expenseType": expenseType,
        "expenseAmount": amount,
        "expenseComments": comment,
        "expenseId":currentID
      })
        .then(function (response) {
          console.log(response.data);
          setsuccessMsg("Expenses submitted successfully!");
          setSuccessOpen(true);
          loadExpenseData();
        }).catch(function (error) {
          console.log(error);
          seterrorMsg(error);
          setErrorOpen(true);
        })
      handleDialogClose();
    }
  };
  const handleDeleteClose = () => {
    setdeleteOpen(false);
  };
  const handleDelete = (e) => {
    if(e.target.id==="")
    setcurrentID(e.target.parentNode.id);
    else
    setcurrentID(e.target.id);
    setdeleteOpen(true);
  };
  const handleDeleteConfirm = () => {
    setdeleteOpen(false);
    console.log(currentID);
    axios.post(DELETE_EXPENSE_URL,
      {
        "expenseId": currentID
      })
      .then(function (response) {
        console.log(response.data);
       loadExpenseData();
      }).catch(function (error) {
        console.log(error);
        seterrorMsg(error.toString());
        setErrorOpen(true);
      })

  };
  const columns = [
       {
      Header: 'Vehicle Number',
      accessor: 'vehicleNumber',
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },  // String-based value accessors!

    {
      Header: 'Type',
      accessor: 'vehicleType',
      //width:'10%',
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: 'Date',
      accessor: 'expenseDate',
      //width:'10%',
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: 'Expense Type',
      accessor: "expenseType",
      //width:"10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      id: "amount",
      Header: "Amount", // Custom header components!
      //width:'30%',
      accessor: "expenseAmount",
      Filter: ({ filter, onChange }) => (
        <input width="48"
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Comment", // Custom header components!
      accessor: "expenseComments",
      //width:"10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          value={filter ? filter.value : ''} />)
    },
    {
      accessor: 'expenseId',
      width:100,
      Cell: props => (<span><Edit id={props.value} style={{ color: "#000033", cursor: "pointer" }} onClick={handleEdit} onMouseOver={handleEditEnter} onMouseOut={handleLeave} />&nbsp;&nbsp;&nbsp;&nbsp; <Delete id={props.value} style={{ color: "#000033", cursor: "pointer" }} onClick={handleDelete} onMouseOver={handleEnter} onMouseOut={handleLeave} /></span>),
      Filter: ({ filter, onChange }) => (null)
    }
  ];

  const handleEnter = (e) => {
    e.target.style.color = "red";
  }

  const handleEditEnter = (e) => {
    e.target.style.color = "green";
  }
  const handleLeave = (e) => {
    e.target.style.color = "#000033";
  }
  return (
    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <h2>Other Vehicle Expenses</h2>
        <Button variant="contained" color="primary" allign="center" style={{ fontSize: "90%", opacity: "1" }} onClick={addHaltingCharges}>Add Other Expenses</Button>
        <br /><br />

        <form className={classes.root} noValidate autoComplete="new-password" style={{ display: 'block' }}  >
          {otherExpenses ? (
            <ReactTable
              data={otherExpenses}
              columns={columns}
              defaultPageSize={10}
              filterable
              filterOptions={(options, state) => options}
              autoComplete="none"
              sorted={[
                {
                  id: "id",
                  asc: true
                }
              ]}
              defaultFilterMethod={(filter, row) =>
                (String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())}
            />) : null}
        </form>
      </div>
      <Dialog
        open={dialogOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleDialogClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dial0g-slide-description"
        maxWidth={"xl"}
        disableBackdropClick
      >
        <DialogTitle id="alert-dialog-slide-title" style={{ textAlign: "center" }}>Add Other Expenses
        <DialogContent>
            {vehicleDetails ? (
              <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >

                <div align="left" style={{ marginLeft: "", display: 'flex', flexWrap: "wrap" }}>
                  <Autocomplete
                    options={vehicleDetails}
                    getOptionLabel={(option) =>
                      option.vehicleNumber !== undefined ? option.vehicleNumber.toString() : ""
                    }
                    getOptionSelected={(option, value) => option.vehicleNumber === value.vehicleNumber}
                    id="vehicleID"
                    clearOnEscape
                    value={{ vehicleNumber }}
                    onChange={onVehicleChange}
                    renderInput={(params) => <TextField {...params} label="Vehicle Number" margin="normal" error={vehicleIDError} helperText={vehicleIDError ? "Select a vehicle number" : ""} />}
                  />
                  <TextField
                    id="type"
                    label="Type"
                    value={type}
                    InputProps={{ readOnly: true, }}
                  />
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      //disableToolbar
                      variant="inline"
                      format="dd/MM/yyyy"
                      margin="normal"
                      id="date-picker-inline"
                      label="Date"
                      value={expenseDate}
                      autoOk
                      onChange={handleExpenseDate}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </MuiPickersUtilsProvider>
                </div>
                <div align="left" style={{ marginLeft: "", display: 'flex', flexWrap: "wrap" }}>

                  <TextField
                    id="standard-number"
                    label="Expense Type"
                    value={expenseType}
                    error={expenseTypeError} helperText={expenseTypeError ? "Enter a valid permit amount" : ""}
                    onChange={e => { setexpenseType(e.target.value); setexpenseTypeError(false); }}
                  />
                  <TextField
                    id="standard-number"
                    label="Amount"
                    value={amount}
                    type="number" error={amountError} helperText={amountError ? "Enter a valid permit amount" : ""}
                    onChange={e => { setamount(e.target.value); setamountError(false); }}
                  />
                  <TextField
                    id="standard-number"
                    label="Comment"
                    value={comment}
                    onChange={e => { setcomment(e.target.value); }}
                  />
                </div>

              </form>
            ) : null}
          </DialogContent>
          <DialogActions>
            <Button onClick={handleAdd} color="primary">
              Add
          </Button>
            <Button onClick={handleDialogClose} color="primary">
              Cancel
          </Button>
          </DialogActions>
        </DialogTitle>
      </Dialog>
      <Dialog
        open={deleteOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleDeleteClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dial0g-slide-description"
        maxWidth={"xl"}
        disableBackdropClick>
        <DialogTitle id="alert-dialog-slide-title" style={{ textAlign: "center" }}>DELETE</DialogTitle>
        <DialogContent>
          Are you sure you want to delete this entry?
            </DialogContent>
        <DialogActions>
          <Button onClick={handleDeleteConfirm} color="primary">
            Yes
          </Button>
          <Button onClick={handleDeleteClose} color="primary">
            No
          </Button>
        </DialogActions>

      </Dialog>
      <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          {successMsg}
        </Alert>
      </Snackbar>
      <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
                    {errorMsg}
        </Alert>
      </Snackbar>
    </div>
  );
}

export default App;



