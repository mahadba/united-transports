import React, { useState ,useEffect } from 'react';
import "./styles.css";
import SideBar from "./sidebar";
import "react-table-6/react-table.css";
import ReactTable from 'react-table-6';
import { makeStyles } from '@material-ui/core/styles';
import * as myConstClass from './Constants.js';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& .MuiTextField-root': {
      margin: theme.spacing(3),
      width: '25ch',

    },
  },
  formControl: {
    margin: theme.spacing(3),
    width: '25ch',
  },

}));

function App() {
	let history = useHistory();
  const classes = useStyles();
   const API_URL = myConstClass.GET_CUSTOMER_DETAILS;
   const [customerDetails, setCustomerDetails] = useState([]);
  
  useEffect(() => {
    loadData();
  },[]);
  
  const loadData = async () => {
	  
	  if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				 const response = await fetch(API_URL);
				const data = await response.json();
				
				setCustomerDetails(data);
				console.log(data);
	  } else {
		  history.push('/home');
	  }
	  
	  
   
  }
  
  const columns = [
    {
      Header: 'Company',
      accessor: 'customerName',
      width: "30%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          style={{ width: "90%" }}
          value={filter ? filter.value : ''} />)
    },
    {
      Header: 'Contact',
      accessor: 'customerPOC',
      width: "30%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          style={{ width: "90%" }}
          value={filter ? filter.value : ''} />)
    },
    {
      Header: 'Phone',
      accessor: "customerContact",
      width: "20%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          style={{ width: "90%" }}
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "Email", // Custom header components!
      accessor: "customeremail",
      width: "10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          style={{ width: "90%" }}
          value={filter ? filter.value : ''} />)
    },
    {
      id:"address",
      Header: "Address", // Custom header components!
      accessor: d => `${d.customerAddress1}, ${d.customerAddress2}`,
      width: "20%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          style={{ width: "90%" }}
          value={filter ? filter.value : ''} />)
    },
    {
      Header: "GST", // Custom header components!
      accessor: "customerGST",
      width: "10%",
      Filter: ({ filter, onChange }) => (
        <input
          onChange={event => onChange(event.target.value)}
          autoComplete="none"
          style={{ width: "90%" }}
          value={filter ? filter.value : ''} />)
    },
    {
      id: "status",
      Header: "Status", // Custom header components!
      accessor: "customerStatus",
      width: "10%",
      Filter: ({ filter, onChange }) => (
        <select
          onChange={event => onChange(event.target.value)}
          style={{ width: "90%", textIndent: "50px" }}
          value={filter ? filter.value : ""}
        >
          <option value="">All</option>
          <option value="Active">Active</option>
          <option value="Inactive">Inactive</option>
        </select>
      )
    }
  ];
  return (
    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
      <div id="page-wrap" style={{ backgroundImage: "url(/hexa3.jpg)", backgroundSize: "cover", widht: "100%", height: "100%" }}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div>
        <h2>Customer Details</h2>
        <form className={classes.root} noValidate autoComplete="new-password" style={{ display: 'block' }}  >
          <ReactTable
            data={customerDetails}
            columns={columns}
            defaultPageSize={10}
            minRows={1}
            filterable
            filterOptions={(options, state) => options}
            autoComplete="none"
            defaultFiltered={[
              {
                id: "status",
                value: "Active"
              }
            ]}
            defaultFilterMethod={(filter, row, column) => {
              console.log(column);
              if (column.id === "status")
                return (String(row[filter.id])).toLowerCase().startsWith((filter.value).toLowerCase())
              else
                return (String(row[filter.id])).toLowerCase().includes((filter.value).toLowerCase())
            }
            }
          />
        </form>
      </div>
    </div>

  );
}

export default App;



