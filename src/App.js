import React from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import VehicleDetails from './vehicleDetails';
import DriverDetails from './driverDetails';
import CustomerDetails from './customerDetails';
import VehicleAddEdit from './vehicleAddEdit';
import driverAddEdit from './driverAddEdit';
import Booking from './booking';
import newBooking from './newBooking';
import openBookings from './openBookings';
import paySalary from './paySalary';
import customerAddEdit from './customerAddEdit';
import customerAddOnly from './customerAddOnly'
import driverAddOnly from './driverAddOnly'
import locationDetails from './locationDetails'
import thirdPartyVehicleDetails from './thirdPartyVehicleDetails'
import thirdPartyAddEdit from './thirdPartyAddEdit'
import locationAddEdit from './locationAddEdit'
import haltingCharges from './haltingCharges'
import otherExpenses from './otherExpenses'
import report from './report'
import contacts from './contacts'
import appAccess from './appAccess'

function App() {
  return (
    <Router>
      <div className="App">
	  

        <Route path='/' exact component={Home} />
        <Route path='/home' component={Home} />
		
		<Route path='/openBookings' component={openBookings} /> 
		<Route path='/booking' component={Booking} />          
        <Route path='/newBooking' component={newBooking} />  
		<Route path='/contacts' component={contacts}/>
		 
		 
		 
        <Route path='/vehicleDetails' component={VehicleDetails} />
        <Route path='/vehicleAddEdit' component={VehicleAddEdit} />
        <Route path='/driverDetails' component={DriverDetails} />
		<Route path='/customerDetails' component={CustomerDetails} />
        <Route path='/driverAddEdit' component={driverAddEdit} /> 
		
		<Route path='/driverAddOnly' component={driverAddOnly} /> 
        <Route path='/paySalary' component={paySalary} /> 
        <Route path='/customerAddEdit' component={customerAddEdit}/>
		<Route path='/customerAddOnly' component={customerAddOnly}/>
		
        <Route path='/locationDetails' component={locationDetails}/>
        <Route path='/thirdPartyVehicleDetails' component={thirdPartyVehicleDetails}/>
        <Route path='/locationAddEdit' component={locationAddEdit}/>
        <Route path='/thirdPartyVehicleAddEdit' component={thirdPartyAddEdit}/>
        <Route path='/haltingCharges' component={haltingCharges}/>
        <Route path='/otherExpenses' component={otherExpenses}/>
        <Route path='/reports' component={report}/>
       
		<Route path='/appAccess' component={appAccess}/>
		
      </div>
    </Router> 
  );
}

export default App;



