import React, { Component } from 'react';
import Banner from 'react-js-banner';
import 'react-dropdown/style.css';
import ReactTable from 'react-table-6'
import 'react-table-6/react-table.css'
import * as myConstClass from './Constants.js';
import ExportToExcel from './ExportToExcel';
import Axios from 'axios';

class aboutToExpire extends Component {

    constructor(props) {
        super(props)
        this.state = {
			category: 'vehicleInsurance',
			dataColumn: [],
			aboutToExpire: [],
			vehicleExpDtlList: {},
			banner1Css: { color: "#FFF", backgroundColor: "green" },
			banner2Css: { color: "#000", backgroundColor: "#028090", fontFamily: "arial" },
			banner3Css: { color: "#FFF", backgroundColor: "red", fontSize: 20 }
        }
		
		this.handleChange = this.handleChange.bind(this);

    }
	
  
  
  componentDidMount() {    
        const url = myConstClass.ABOUT_TO_EXPIRE_DETAILS ;
        console.log("url : " + url);
        Axios.get(url)
            .then(vehicleList => {
                console.log("referalList : ", vehicleList);
                console.log("referalList data : ", vehicleList.data);
                this.setState({
                    vehicleExpDtlList: vehicleList.data,
					aboutToExpire: vehicleList.data.insurance
                })

            })
            .catch(error => {
                console.log(error);
            });
    }
	
  handleChange(event) {
			console.log(event.target.value);
			this.setState({
				category: event.target.value,
				aboutToExpire : this.state.vehicleExpDtlList.insurance
			});
			
			if("vehicleInsurance" === event.target.value){
				this.setState({
					aboutToExpire : this.state.vehicleExpDtlList.insurance
				});
			 } else if("vehicleRoadTax" === event.target.value){
				this.setState({
					aboutToExpire : this.state.vehicleExpDtlList.roadTax
				});
			 }else if("vehicleNationalPermitValidity" === event.target.value){
				this.setState({
					aboutToExpire : this.state.vehicleExpDtlList.nationalPermit
				});
			 }else if("vehicleTNPermitValidity" === event.target.value){
				this.setState({
					aboutToExpire : this.state.vehicleExpDtlList.tamilNaduPermit
				});
			 } else if("vehicleFCStatus" === event.target.value){
				this.setState({
					aboutToExpire : this.state.vehicleExpDtlList.fc
				});
			 }
	
	
  }

    render() {
			var data = myConstClass.vehicleInsurance ;
				
			if("vehicleInsurance"===this.state.category){
					data = myConstClass.vehicleInsurance ;
			 } else if("vehicleRoadTax"===this.state.category){
					data = myConstClass.vehicleRoadTax ;
			 }else if("vehicleNationalPermitValidity"===this.state.category){
					data = myConstClass.vehicleNationalPermitValidity ;
			 }else if("vehicleTNPermitValidity"===this.state.category){
					data = myConstClass.vehicleTNPermitValidity ;
			 }else if("vehicleFCStatus"===this.state.category){
					data = myConstClass.vehicleFCValidity ;
			 }

       
        return (
            
                  
              <div>
			
			  <Banner title="We are displaying details which are about to expire in 20 days" css={this.state.banner2Css} />
			<div className='detailsDropdowndiv'>
			  <strong>Select Category : </strong>
          <select value={this.state.category} onChange={this.handleChange} className='detailsDropdown' style={{backgroundColor:"#8A7F80"}}>
            <option  className='myMenuClassName' value="vehicleInsurance">Insurance</option>
            <option className='myMenuClassName' value="vehicleRoadTax">Road Tax</option>
            <option className='myMenuClassName' value="vehicleNationalPermitValidity">National Permit</option>
            <option className='myMenuClassName' value="vehicleTNPermitValidity">TamilNadu Permit</option>
			<option className='myMenuClassName' value="vehicleFCStatus">FC Status</option>
          </select>
		  </div>
			
			<ReactTable
				data={this.state.aboutToExpire}
				columns={data}
				defaultPageSize={10}
			  >
			  
			  {(state,filteredData, instance) => {
				this.reactTable = state.pageRows.map(post => {return post._original});
							return (
								<div> 
									{filteredData()}
									<ExportToExcel posts={this.reactTable}/>
								</div>
							)	
			  } }
			  
			  </ReactTable>
		
			  </div>
        )

    }
}

export default aboutToExpire;


