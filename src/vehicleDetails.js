import React, { Component } from 'react';
import "./styles.css";
import SideBar from "./sidebar";
import ViewVehicleDetails from "./ViewVehicleDetails";
import AboutToExpire from "./aboutToExpire";
import ExpiredVehicleDetails from "./expiredVehicleDetails";




import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';


class vehicleDetails extends Component {
	
	 componentDidMount() {


       if("booking"=== localStorage.getItem('accessType')) {
			//history.push('/newBooking');
			 this.props.history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
				
	  } else {
		 // history.push('/home');
		   this.props.history.push('/home');
	  }
	  
	
    }
 
	render() {
		
		return (
    <div id="App">
    <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />

    <div id="page-wrap" style={{backgroundImage: "url(/hexa3.jpg)", backgroundSize:"cover", widht:"100%", height:"100%"}}>
	<div className="appHeaderClass"> UNITED TRANSPORT</div>      
      <h2>Vehicle Details</h2>
     <Accordion className='accordionTest' allowZeroExpanded preExpanded={['expire']}>
	    <AccordionItem uuid="expire" className='accordion__item_test'>
                <AccordionItemHeading className="accordion__Heading_test">
                    <AccordionItemButton className="accordion__button_test"> 
                       About to Expire
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel className="accordion__panel_test">
                    <AboutToExpire/>
                </AccordionItemPanel>
            </AccordionItem>
			 <AccordionItem className='accordion__item_test'>
                <AccordionItemHeading className="accordion__Heading_test">
                    <AccordionItemButton className="accordion__button_test">
                       View Vehicle Details
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel className="accordion__panel_test">
                    <ViewVehicleDetails/>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem className='accordion__item_test'>
                <AccordionItemHeading className="accordion__Heading_test">
                    <AccordionItemButton className="accordion__button_test">
                       Expired Details
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel className="accordion__panel_test">
                    <ExpiredVehicleDetails/>
                </AccordionItemPanel>
            </AccordionItem>
           
            </Accordion>
      
    </div>
  </div>
  )
		
		
	}
}

export default vehicleDetails;





