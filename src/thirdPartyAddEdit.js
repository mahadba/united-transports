import React, {  useState , useEffect } from 'react';
import "react-table-6/react-table.css";
import "./styles.css";
import SideBar from "./sidebar";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Snackbar from '@material-ui/core/Snackbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { vehicleDetails, driverDetails, locationDetails, thridPartyVehicleDetails } from './Constants';
import Button from '@material-ui/core/Button';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      '& .MuiTextField-root': {
        margin: theme.spacing(3),
        width: '25ch',
        
      },
    },
    formControl: {
      margin: theme.spacing(3),
      width: '25ch',
    },
    
  }));
  
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  
  function App() {
	  let history = useHistory();
    const classes = useStyles();
    const [vehicleID, setvehicleID] = React.useState("");
    const [resetVehicleID, setresetVehicleID] = React.useState("resetVehicleID");
    const [vehicleIDError, setvehicleIDError] = React.useState(false);    
    const [type, settype] = React.useState("");
    const [typeError, settypeError] = React.useState(false);
    const [company, setcompany] = React.useState("");
    const [companyError, setcompanyError] = React.useState(false);
    const [amount, setamount] = React.useState("");
    const [amountError, setamountError] = React.useState(false);
    const [advance, setadvance] = React.useState("");
    const [advanceError, setadvanceError] = React.useState(false);
    const [balance, setbalance] = React.useState("");
    const [balanceError, setbalanceError] = React.useState(false);
    const [successopen, setSuccessOpen] = React.useState(false);
    const [erroropen, setErrorOpen] = React.useState(false);
   
    useEffect(() => {
		loadData();
	},[]);
  
  const loadData = async () => {
	  
	  
	    if("booking"=== localStorage.getItem('accessType')) {
			history.push('/newBooking');
	  } else if("admin" === localStorage.getItem('accessType')) {
			
	  } else {
		  history.push('/home');
	  }
	  
	  
    
  }
  
    const handleVehicleIDSelect =(event, newvalue) =>{
        setvehicleID(newvalue);
        setvehicleIDError(false);
        var vn = newvalue;
        var i = thridPartyVehicleDetails.find(item=> {
          return item.vehicleNumber ===vn
        })
        if(i)
        {           
            settype(i.vehicleType);
            setcompany(i.company);
            setadvance(i.paid);
            setamount(i.amount);
            setbalance(i.balance);
            setcompanyError(false);
            settypeError(false);
            setadvanceError(false);
            setamountError(false);
        }
    };
    const handleTypeChange= (e) => { 
        settype(e.target.value);
        settypeError(false);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }  
        setSuccessOpen(false);  
        setErrorOpen(false);  
      };
    const handleClick= (e) => { 
      console.log(vehicleID);
      var eflag=false;
      if(vehicleID=="" || vehicleID == null){
          setvehicleIDError(true);
          eflag=true;
      }
      if(company==""){
          setcompanyError(true);
          eflag=true;
      }
      if(type==""){
          settypeError(true);
          eflag=true;
      }
      if(amount==""){
        setamountError(true);
        eflag=true;
      }
      if(advance==""){
        setadvanceError(true);
        eflag=true;
      }
    if(eflag){
        setErrorOpen(true);
        setSuccessOpen(false);
    }
    else  
    {   setErrorOpen(false);
        setSuccessOpen(true);
        var vt=moment().format("hhmmss");
        setresetVehicleID(vt);
        setcompany("");        
        settype("");
        setamount("");
        setadvance("");
        setbalance("");
    }
    };
    const handleVehicleIDBlur = (event) => {
        setvehicleID(event.target.value);
      };
    const handleVehicleIDChange = (event) => {
        setvehicleIDError(false);
      };
    const companychange =(event) =>{
        setcompany(event.target.value);
        setcompanyError(false);
    };
    const advanceChange =(event) =>{
      if(event.target.value>0)
        setadvance(event.target.value);
        setadvanceError(false);
        
    };
    const amountChange =(event) =>{
      if(event.target.value>0)
        setamount(event.target.value);
        setamountError(false);
        
    };
    const balanceUpdate=(event)=>{
        if((amount!="")&&(advance!=""))
        {
            var tot = parseInt(amount)-parseInt(advance);
            setbalance(tot);
        }
      };
    return (   
      
        <div id="App">
        <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"}/> 
        <div id="page-wrap" style={{backgroundImage: "url(/hexa3.jpg)", backgroundSize:"cover", widht:"100%", height:"100%"}}>
        <div className="appHeaderClass"> UNITED TRANSPORT</div> 
        <div>
        <h2>Add/Edit Third Party Vehicle Details</h2>
        <br/> <br/> 
        <form className={classes.root} noValidate autoComplete="off" style={{ display: 'block' }} >
        <div align="left" style={{marginLeft:"4em",display: 'flex', flexWrap:"wrap"}}>         
        <Autocomplete
            id="vehicleID"
            freeSolo  
            options={thridPartyVehicleDetails.map((option) => option.vehicleNumber)}
            key={resetVehicleID}
            value={vehicleID} 
            onChange={handleVehicleIDSelect}                                  
            renderInput={(params) => <TextField {...params} label="Vehicle ID" margin="normal" error={vehicleIDError} helperText={vehicleIDError ? "Vehicle ID is required" : ""}/>}
        />    
            <FormControl className={classes.formControl} error={typeError}>        
            <InputLabel id="select company label">Vehicle Type</InputLabel>
            <Select
            className="mdb-select"
            labelId="select type"
            id="select type"
            label="Vehicle Type"          
            value={type}           
            onChange={handleTypeChange}          
            >
          <MenuItem value={"20'"}>20'</MenuItem>
          <MenuItem value={"40'"}>40'</MenuItem>        
          </Select>
          <FormHelperText>{typeError ? "Select a vehicle type": ""}</FormHelperText>
          </FormControl>
          <TextField
          id="company"
          label="Company"            
          value={company}
          onChange={companychange}
          error={companyError} helperText={companyError ? "Enter the company name" : ""} 
            />           
        </div>
        <br/>
        <div align="left" style={{marginLeft:"4em",display: 'flex', flexWrap:"wrap"}}>  
        <TextField
          id="amount"
          label="Amount"            
          value={amount}
          type="number"
          onChange={amountChange}
          onBlur={balanceUpdate}
          error={amountError} helperText={amountError ? "Enter the amount charged for the vehicle" : ""} 
            />           
            <TextField
          id="advance"
          label="Paid"            
          value={advance}
          type="number"
          onChange={advanceChange}
          onBlur={balanceUpdate}
          error={advanceError} helperText={advanceError ? "Enter the amount paid" : ""} 
            />           
            <TextField
          id="balance"
          label="Balance"            
          value={balance}          
          type="number"     
          InputProps={{readOnly: true,}}  />                  
        </div>
        <br/><br/>
        <Button variant="contained" color="primary" allign="center" style={{fontSize:"90%", opacity:"1"}} onClick={handleClick}>Submit</Button>
        <br/>
        </form>
        <Snackbar open={successopen} autoHideDuration={7000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
            Details submitted successfully !
        </Alert>
        </Snackbar>
        <Snackbar open={erroropen} autoHideDuration={7000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Please fill all the mandatory fields.
        </Alert>
        </Snackbar>
        </div>
        </div>
        </div>
    );
  }
  export default App;