import React, { Component } from 'react';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class ExportToExcel extends Component{

    render(){
        return(
            <div style={{marginRight: '25px'}}>
                <ReactHTMLTableToExcel
                    id="test-table-xls-button"
                    className="export"
                    table="table-to-xls"
                    filename="filtredData"
                    sheet="tablexls"
                    buttonText="Export"/>
                <table hidden={true} id="table-to-xls">
                    <thead>
                    <tr>
                        <th>Vehicle No</th>
                        <th>Vehicle Model</th>
                        <th>Vehicle Type </th>
                        <th>Insurance Validity</th>
						<th>Company</th>
						<th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.posts.map(post => {
                            return(

                                <tr key={post.vehicleNumber}>
								<td>{post.vehicleNumber }</td>
                                    <td>{post.vehicleModel }</td>
                                    <td>{post.vehicleType }</td>
                                    <td>{post.insuranceValidity }</td>
                                    <td>{post.company }</td>
									 <td>{post.status }</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>

            </div>
        )
    }
}
export default ExportToExcel;